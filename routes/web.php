<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@viewLanding");

//---------//
/* LOGIN */
//--------//
Route::get('/login', 'LoginController@viewLogin')->name('login');
Route::post('/login', 'LoginController@login');

Route::get('/reset', 'LoginController@viewPasswordReset');
Route::post('/reset', 'LoginController@passwordReset');

Route::get('/register', 'RegisterController@viewRegister');
Route::post('/register', 'RegisterController@register');

Route::post('/webhook', 'WebHookController@prueba');
//-----------//
/* END LOGIN*/
//-----------//

Route::group(['middleware' => 'auth'], function () {
	Route::get('/logout', 'LoginController@logout');
	Route::get('/logOptica', 'LoginController@logOptica');

	Route::get('/checkSuscripcion', 'SuscripcionController@checkSuscripcion');

	Route::get('/estadisticas', "IndexController@index");
	Route::get('/preguntas', "IndexController@showFaq");

	Route::group(['prefix' => 'cliente'], function () {
		Route::get('/', "UsuarioController@getCliente");

		Route::get('/{id}/receta/{producto_id}', "UsuarioController@showCliente");
		
		Route::get('/{id}/receta/{producto_id}/download', "RecetaController@downloadReceta");

		Route::post('/{id}/edit', "UsuarioController@editCreate");
		
		Route::post('/{id}/receta/{producto_id}/edit_receta', "RecetaController@update");
		
		Route::post('/save_mail', "UsuarioController@saveMail");
		Route::post('/send_mail', "UsuarioController@sendMail");
	});

	Route::post('/create_producto', "RecetaController@create");

	Route::get('/recetas', "RecetaController@getRecetas");
	Route::post('/recetas/{producto_id}/updateEstado', "RecetaController@updateEstado");

	Route::group(['prefix' => 'estadisticas'], function () {
		Route::get('/getRecetasByEstados', "EstadisticasController@getRecetasByEstados");
		Route::get('/getRecetasByMedico', "EstadisticasController@getRecetasByMedico");
		Route::get('/getInfoBoxes', "EstadisticasController@getInfoBoxes");
	});

	Route::group(['prefix' => 'administracion'], function () {
		Route::get('', "AdministracionController@show");
		Route::get('/save_profesional', "AdministracionController@saveProfesional");
		Route::post('/change_password', "AdministracionController@changePassword");

		Route::get('/medicos', "AdministracionController@showMedicos");
		Route::post('/medicos/save_medico', "AdministracionController@saveMedico");

		Route::get('/opticas', "AdministracionController@showOpticas");
		Route::post('/opticas/save_optica', "AdministracionController@saveOptica");
		
		Route::get('/usuarios', "AdministracionController@showUsuarios");
		Route::post('/usuarios/save_usuario', "AdministracionController@saveUsuario");
	});
	
	Route::group(['prefix' => 'suscripcion'], function () {
		Route::get('', "SuscripcionController@show");
		Route::post('asociarTarjeta', "SuscripcionController@asociarTarjeta");
		Route::post('deleteTarjeta', "SuscripcionController@deleteTarjeta");
		Route::post('updateDefaultTarjeta', "SuscripcionController@updateDefaultTarjeta");
		Route::post('editSuscripcion', "SuscripcionController@editSuscripcion");
	});

	Route::group(['prefix' => 'superadmin'], function () {
		Route::get('/', "SuperAdminController@show");
		Route::get('/notificaciones', "SuperAdminController@showNotificaciones");
		Route::get('/opticas', "SuperAdminController@showOpticas");
		Route::get('/planes', "SuperAdminController@showPlanes");
		Route::get('/getAdministradores', "SuperAdminController@getAdministradores");
		Route::get('/getAdministradoresByPago', "SuperAdminController@getAdministradoresByPago");
		Route::put('/planes', "SuperAdminController@savePlanes");
	});

	Route::get('/pruebaPdf', "PruebaController@pruebaPdf");

});
