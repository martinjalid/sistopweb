<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class LentePrueba extends Authenticatable{
    use Notifiable;

	protected $table = 'lente_prueba';
	public $timestamps = false;

	protected $fillable = [
		'receta_lente_id',
		'od_esferico',
		'od_eje',
		'od_cilindrico',
		'oi_esferico',
		'oi_eje',
		'oi_cilindrico',
		'oi_agudeza',
		'od_agudeza',
		'ao_agudeza',
	];
	public function getOdEsfericoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOdCilindricoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOiEsfericoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOiCilindricoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOiEjeAttribute($val){
		return is_null($val) ? $val : $val.'°';
	}

	public function getOdEjeAttribute($val){
		return is_null($val) ? $val : $val.'°';
	}
}