<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdministradorCobranza extends Model{

	protected $table = 'administrador_cobranza';

	protected $fillable = [
		'administrador_id',
		'estado_id',
		'fecha_cuota',
		'fecha_pago',
		'monto',
		'plan_id',
	];

	public $timestamps = false;
}