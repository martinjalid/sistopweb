<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class RecetaLente extends Authenticatable{
    use Notifiable;

	protected $table = 'receta_lente';
	public $timestamps = false;

	protected $fillable = [
		'od_esferico',
		'oi_esferico',
		'od_eje',
		'oi_eje',
		'od_cilindrico',
		'oi_cilindrico',
		'queratometria_od',
		'queratometria_oi',
		'tipo_lente',
		'diametro',
		'radio',
		'color',
		'producto_id',
	];

    public function prueba(){
    	return $this->hasOne('App\LentePrueba', 'receta_lente_id', 'id');
    }

    public function oftalmologo(){
    	return $this->hasOne('App\LenteOftalmologo', 'receta_lente_id', 'id');
    }
    
    public function getOdEsfericoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOdCilindricoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOiEsfericoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOiCilindricoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOiEjeAttribute($val){
		return is_null($val) ? $val : $val.'°';
	}

	public function getOdEjeAttribute($val){
		return is_null($val) ? $val : $val.'°';
	}
};