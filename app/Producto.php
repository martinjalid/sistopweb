<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Producto extends Model{

	protected $table = 'producto';

    protected $fillable = [
        'profesional_id',
        'observacion',
        'usuario_id',
        'tipo_producto_id',
        'optica_id',
        'estado_id',
        'activo',
    ]; 

    public function usuario(){
        return $this->belongsTo('App\Usuario', 'usuario_id', 'id');
    }

    public function estado(){
        return $this->belongsTo('App\Estado', 'estado_id', 'id')->first()->nombre;
    }

    public function receta(){
        return $this->hasOne('App\Receta', 'producto_id', 'id');
    }

    public function recetaLente(){
        return $this->hasOne('App\RecetaLente', 'producto_id', 'id');
    }

    public function tipoProducto(){
        return $this->hasOne('App\TipoProducto', 'id', 'tipo_producto_id');
    }

    public function medico(){
        return $this->hasOne('App\Profesional', 'id', 'profesional_id')->first();
    }

    public function getCreatedAtAttribute($value){
    	$dt = Carbon::parse($value);
        return $dt;
    }

}