<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Plan extends Authenticatable{

	protected $table = 'plan';

	protected $fillable = [ 'max_opticas', 'max_usuarios', 'monto', 'id_mp' ];

	public $timestamps = false;

}