<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Administrador extends Authenticatable{
	use Notifiable;
	
	protected $table = 'administrador';

	protected $hidden = [
        'password', 'remember_token',
    ];

    protected $fillable = [
        'mail',
        'password',
        'activo',
        'perfil_id',
        'plan_id',
        'free_trial_expires_at',
        'suscripcion_mp',
    ];

    public function perfil(){
        return $this->hasOne('App\Perfil', 'id', 'perfil_id')->first()->nombre;
    }

    public function opticas(){
        return $this->belongsToMany('App\Optica', 'administrador_optica', 'administrador_id', 'optica_id')->where('activo', 1)->get();
    }

    public function getOpticasIds(){
        $array = [];
        foreach ($this->opticas() as $optica) {
            array_push($array, $optica->id);
        }
        return $array;
    }

    public function plan(){
        return $this->hasOne('App\Plan', 'id', 'plan_id');
    }

    public function getMedicosIds(){
        $array = [];
        foreach ($this->opticas() as $optica) {
            foreach ($optica->profesionales as $prof) {
                array_push($array, $prof->id);
            }
        }
        return $array;
    }

    public function getAdministradorGral(){
        if ( $this->perfil() != 'Administrador' ) {
            $optica = $this->opticas()->first();
            return $optica->usuarios()->where('perfil_id', 1)->first();
        }else{
            return $this;
        }
    }

    public function estaSuscrito(){
        if ( $this->perfil() != 'Administrador' ) {
            $admin = $this->getAdministradorGral();
            return $admin->suscripcion_mp == '' ? false : true;            
        }else{ 
            return $this->suscripcion_mp == '' ? false : true;
        }
    }
}