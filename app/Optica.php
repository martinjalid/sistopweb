<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Optica extends Model{
	protected $table = 'optica';

    protected $fillable = [
        'nombre',
        'activo'
    ];

    public function getClientes(){
    	return $this->hasMany('App\Usuario', 'optica_id', 'id');
    }

    public function usuarios(){
        return $this->belongsToMany('App\Administrador', 'administrador_optica', 'optica_id', 'administrador_id');
    }

    public function productos(){
        return $this->hasMany('App\Producto', 'optica_id', 'id')->where('activo', 1);
    }

    public function profesionales(){
    	return $this->hasMany('App\Profesional', 'optica_id', 'id');
    }

    public function getMedicosIds(){
        $array = [];
        foreach ($this->profesionales as $prof) {
            array_push($array, $prof->id);
        }
        return $array;
    }

    public function puedeCrearUsuario(){
        $admin = $this->usuarios()->where('perfil_id', '1')->first();
        return $this->usuarios()->count() < $admin->plan()->first()->max_usuarios;
    }

    public function getCreatedAtAttribute($value){
        $dt = Carbon::parse($value);
        return $dt;
    }

}