<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Anteojo extends Authenticatable{
    use Notifiable;

	protected $table = 'anteojo';

	protected $fillable = [
		'tipo',
		'distancia',
		'od_esferico',
		'oi_esferico',
		'od_eje',
		'oi_eje',
		'od_cilindrico',
		'oi_cilindrico',
		'armazon',
		'valor_lente',
		'valor_armazon',
		'tratamiento_color',
		'color_id',
		'tratamiento_id',
		'material_lente_id',
		'receta_id',
		'origen_lente',
	];

	public $timestamps = false;

	public function material(){
        return $this->hasOne('App\MaterialLente', 'id', 'material_lente_id');
    }

    public function tratamiento(){
        return $this->hasOne('App\Tratamiento', 'id', 'tratamiento_id');
    }

    public function color(){
        return $this->hasOne('App\Color', 'id', 'color_id');
    }

	public function getOdEsfericoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOdCilindricoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOiEsfericoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOiCilindricoAttribute($val){
		return $val > 0 ? '+'.$val : $val;
	}

	public function getOiEjeAttribute($val){
		return is_null($val) ? $val : $val.'°';
	}

	public function getOdEjeAttribute($val){
		return is_null($val) ? $val : $val.'°';
	}


}