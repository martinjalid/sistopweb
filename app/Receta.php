<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Receta extends Model{
	protected $table = 'receta';
	public $timestamps = false;

	protected $fillable = [
		'distancia_izq',
		'distancia_der',
		'altura',
		'adicion',
		'administrador_id',
		'observacion',
		'detalle_lente',
		'usuario_id',
		'profesional_id',
		'tipo_lente_id',
		'activo',
		'producto_id'
	];

	public function anteojos(){
		return $this->hasMany('App\Anteojo', 'receta_id', 'id')->get();
	}

	public function tipo_lente(){
        return $this->hasOne('App\TipoLente', 'id', 'tipo_lente_id');
    }

}