<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdministradorOptica extends Authenticatable{
	use Notifiable;
	
	protected $table = 'administrador_optica';

	public $timestamps = false;
	
    protected $fillable = [
        'administrador_id',
        'optica_id',
    ];
}
