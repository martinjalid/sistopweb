<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoNotificacion extends Model{

	protected $table = 'tipo_notificacion';

	public $timestamps = false;
}