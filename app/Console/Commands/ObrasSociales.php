<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ObrasSociales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'obras:sociales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $csv = file_get_contents( public_path('csv/obras sociales.csv')  );
        $csv = explode('""', $csv);
        dd( $csv );
    }
}
