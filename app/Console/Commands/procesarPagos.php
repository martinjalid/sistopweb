<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Administrador;
use App\AdministradorCobranza;
use App\Plan;
use Carbon\Carbon;
require_once base_path('vendor/mercadopago/sdk/lib/mercadopago.php');

class procesarPagos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:pagos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        try {
            $administradores = Administrador::join('perfil', 'perfil_id', 'perfil.id')->where('perfil.nombre', 'Administrador')->get();
            $hoy = Carbon::now()->toDateString();
            
            foreach ($administradores as $administrador ) {
                $cobranza = AdministradorCobranza::join('estado', 'estado.id', 'estado_id')->where('administrador_id', $administrador->id)->where('nombre', 'Pendiente')->where('tipo', 'Cobranza')->first();
                
                $plan = Plan::find( $administrador->plan_id );

                if ( !is_null( $cobranza ) ) {

                    if ( $hoy >= $cobranza->fecha_cuota ) {

                        if ( is_null( $cobranza->plan_id ) ) {
                            $cobranza->update([
                                'plan_id'   => $plan->id,
                                'monto'     => $plan->monto,
                            ]);
                        }

                        $mp = new \MP ( config('services.mercadopago.access_token'));
                        
                        $mail = $administrador->id == 1 ? 'test@test.com' : $administrador->mail;
                        // BUSCO EL CLIENTE EN MP
                        $customer = $mp->get("/v1/customers/search", array( "email" => $mail ));

                        $customer_id = $customer["response"]["results"][0]['id'];
                        $default_card = $customer["response"]["results"][0]['default_card'];
                        
                        // GENERO EL CARD TOKEN PARA EL PAGO
                        $card_token = $mp->post ("/v1/card_tokens", array("card_id" => $default_card));
                        $token = $card_token['response']['id'];

                        //REALIZO EL PAGO
                        $payment_data = array(
                                    "transaction_amount" => $cobranza->monto,
                                    "token" => $token,
                                    "description" => "SistOpWeb",
                                    "installments" => 1,
                                    "payer" => array ( "id" => $customer_id) 
                                );

                        $payment = $mp->post("/v1/payments", $payment_data); 
                        dd( $payment );

                    }else
                        continue;    
                
                }else
                    continue;
                
            }

        } catch (Exception $e) {
            error_log( 'Error: '.$e->getMessage().' - Linea: '.$e->getLine() );
        }
    }
}
