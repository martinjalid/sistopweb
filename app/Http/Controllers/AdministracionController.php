<?php  


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\Request;
use App\Administrador;
use App\AdministradorOptica;
use App\Profesional;
use App\Perfil;
use App\Optica;
use App\Notificar;
use App\TipoNotificacion;
use Exception;
use Validator;
use Mail;
use App\Mail\NewUsuario;
use Hash;

class AdministracionController extends Controller {

	public function show(){
		try {

			$admin = Auth::user();
			
			$optica = Optica::find( session('optica_id') );
			$usuarios = $optica->usuarios()->where('perfil_id', 2)->get();
			$medicos = Profesional::whereIn('optica_id', $admin->getOpticasIds() )->get();
			
			$response = view('administracion.index',  [
				'rutas'     => [
	                ['url' 	=> '/estadisticas', 'nombre' => 'Inicio'], 
	                ['url' 	=> '/administracion', 'nombre' => 'Administracion', 'last' => true], 
	            ],
				'usuarios'	=> $usuarios,
				'optica'	=> $optica,
				'admin'		=> $admin,
				'medicos'	=> $medicos
			]);
		} catch (Exception $e) {
			$response = $this->formatError($e);
		}
		return $response;
		
	}

	public function changePassword( Request $request){
		try {
			$administrador = Administrador::find( Auth::id() );
			$old_pass = $request->get('old_pass');
			$new_pass = $request->get('new_pass');

			if ( !Hash::check( $old_pass, $administrador->password) )
				return $response = [ 'error' => true, 'msj' => 'La contraseña ingresada no coincide con la actual.', 'selector' => 'old_password'];

			$error = Validator::make( $request->all(), [
	            'new_pass' => 'required|min:6',
	        ])->fails();

			if ( $error ) 
				return $response = [ 'error' => true, 'msj' => 'La nueva contraseña debe tener al menos 6 caracteres.', 'selector' => ['new_password', 're_password'] ];

			$administrador->update( [ 'password' => Hash::make( $new_pass ) ]);

			$response = ['error' => false];
		} catch (Exception $e) {
    		$response = $this->formatError($e);	
		}
		return $response;
	}

	public function showMedicos(){
		try {
			$admin = Auth::user();
			$optica = Optica::find( session('optica_id') );
			$usuarios = $optica->usuarios()->where('perfil_id', 2)->get();
			$medicos = Profesional::whereIn('optica_id', $admin->getOpticasIds() )->get();
			
			$response = view('administracion.medicosList',  [ 
				'rutas'     => [ 
	                ['url' 	=> '/estadisticas', 'nombre' => 'Inicio'], 
	                ['url' 	=> '/administracion', 'nombre' => 'Administracion'], 
	                ['url' 	=> '/medico', 'nombre' => 'Médicos', 'last' => true], 
	            ],
				'usuarios'	=> $usuarios,
				'optica'	=> $optica,
				'admin'		=> $admin,
				'medicos'	=> $medicos
			]);
		} catch (Exception $e) {
			$response = $this->formatError($e);
		}
		return $response;
	}

	public function showUsuarios(){
		try {
			$admin = Auth::user();
			$opticas = $admin->getOpticasIds();
			$ids = [];
			foreach( AdministradorOptica::whereIn('optica_id', $opticas)->get() as $ao ){
				array_push($ids, $ao->administrador_id);
			}
			$usuarios = Administrador::whereIn('id', $ids)->where('perfil_id', 2)->get();
			
			$response = view('administracion.usuariosList',  [ 
				'rutas'     => [ 
	                ['url' 	=> '/estadisticas', 'nombre' => 'Inicio'], 
	                ['url' 	=> '/administracion', 'nombre' => 'Administracion'], 
	                ['url' 	=> '/usuarios', 'nombre' => 'Usuarios', 'last' => true], 
	            ],
				'usuarios'	=> $usuarios,
				'admin'		=> $admin,
			]);
		} catch (Exception $e) {
			$response = $this->formatError($e);
		}
		return $response;
	}

	public function saveUsuario( Request $request ){
		try {
			$admin = Auth::user();
			$usuario = Administrador::find( $request->get('administrador_id') );
			$optica = Optica::find( $request->get('optica_id') );
			
			if ( is_null($usuario) ) {
				if ( $optica->puedeCrearUsuario() ) {
					$perfil_id = Perfil::where('nombre', 'Usuario')->first()->id;
					$pass = Hash::make( $request->get('password') );

					$error = Validator::make( $request->all(), [
			            'password' => 'required|min:6',
			        ])->fails();

					if ( $error ) 
						return $response = [ 'error' => true, 'msj' => 'La nueva contraseña debe tener al menos 6 caracteres.' ];

			        $error = Validator::make( $request->all(), [
			            'mail' => 'required|email|max:255',
			        ])->fails();

					if ( $error ) 
						return $response = [ 'error' => true, 'msj' => 'El mail es invalido.' ];

            		$usuario = Administrador::create( array( 
                                'mail'      => $request->get('mail'), 
                                'password'  => Hash::make($request->get('password')), 
                                'perfil_id' => $perfil_id,
                                'activo'    => 1
                            ) );


            		AdministradorOptica::create( array(
                                'administrador_id' => $usuario->id, 
                                'optica_id' => $optica->id,
                            ) );


	            	Mail::to( $request->get('mail') )->send( new NewUsuario($request->get('password'), $optica->nombre) ); 
		            if( !count( Mail::failures() ) ){
	            		$tipo_notificacion_id = TipoNotificacion::where('nombre', 'New Usuario')->first()->id;

			            Notificar::create(array(
			                'administrador_id'      => $usuario->id,
			                'tipo_notificacion_id'  => $tipo_notificacion_id,
			            ));
		            }

				}else{
					throw new Exception("No se pueden registrar mas usuarios");	
				}
			}else
				$usuario->update([ 'mail' => $request->get('mail'), 'activo' => $request->get('activo') ]);
			
			$response = ['error' => false];
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}
		return $response;
	}

	public function saveMedico( Request $request ){
		try {
			$profesional = Profesional::find( $request->get('profesional_id') );

			if ( is_null( $profesional ) )
				Profesional::create( $request->all() );
			else
				$profesional->update( $request->all() );

			$response = ['error' => false];
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}
		return $response;
	}

	public function showOpticas(){
		try {
			$admin = Auth::user();
			
			$response = view('administracion.opticasList',  [ 
				'rutas'     => [ 
	                ['url' 	=> '/estadisticas', 'nombre' => 'Inicio'], 
	                ['url' 	=> '/administracion', 'nombre' => 'Administracion'], 
	                ['url' 	=> '/opticas', 'nombre' => 'Ópticas', 'last' => true], 
	            ],
				'admin'		=> $admin,
			]);
		} catch (Exception $e) {
			$response = $this->formatError($e);
		}
		return $response;
	}

	public function saveOptica( Request $request ){
		try {
			$optica = Optica::find( $request->get('optica_id') );

			if ( !is_null( $optica ) )
				$optica->update( $request->all() );
			else{
				
				// Optica::create( $request->all() );
			}

			$response = ['error' => false];
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}
		return $response;
	}

}

?>