<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Optica;
use App\Estado;
use App\Producto;
use App\Receta;
use App\RecetaLente;
use App\Anteojo;
use App\LentePrueba;
use App\LenteOftalmologo;
use App\TipoProducto;
use App\TipoLente;
use App\Administrador;
use App\Usuario;
use \Illuminate\Http\Request;
use Auth;
use Exception;
use Dompdf\Dompdf;
require_once base_path('vendor/autoload.php');
	
class RecetaController extends Controller {

	public function update($usuario_id, $producto_id, Request $request){
		try {
			$optica = Optica::find( session('optica_id') );

			if ( is_null($optica) )
				throw new Exception("No se Encontro la optica");

			$usuario = $optica->getClientes()->find( $usuario_id );
			if ( is_null( $usuario ) )
				throw new Exception("El usuario enviado no corresponde con la óptica");
			
			$producto = $usuario->productos()->find( $producto_id );			
			if ( is_null( $producto ) )
				throw new Exception("El producto corresponde con el cliente");

			if ( $producto->tipoProducto->nombre == 'Lentes Aereos' ) {

				if ( !is_null( $request->get('lejos') ) )
					$producto->receta->anteojos()->where('tipo', 'lejos')->first()->update( $request->get('lejos') );

				if ( !is_null( $request->get('cerca') ) )
					$producto->receta->anteojos()->where('tipo', 'cerca')->first()->update( $request->get('cerca') );

				$producto->receta()->update( $request->get('receta'));
			}else{
				$producto->recetaLente->prueba()->update( $request->get('prueba') );
				$producto->recetaLente->oftalmologo()->update( $request->get('oftalmologo') );
				$producto->recetaLente()->update( $request->get('receta_lente') );
			}
			$producto->update( $request->get('producto') );
			
			$response = ['error' => false];
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}

		return $response;
	}

	public function create(Request $request){
		try {
			$optica = Optica::find( session('optica_id') );
			if ( is_null( $optica ) )
				throw new Exception("Error, no se encontro la óptica");

			$usuario = $optica->getClientes()->find( $request->get('usuario_id') );
			if ( is_null( $usuario ) )
				throw new Exception("Error, no se encontro el usuario");

			if ( $request->get('tipo_producto') == 'anteojo')
				$tipo_producto = TipoProducto::where('nombre', 'Lentes Aereos')->first();
			else
				$tipo_producto = TipoProducto::where('nombre', 'Lentes de Contacto')->first();

			$estado_pendiente_id = Estado::where('nombre', 'Pendiente')->first()->id;

			$producto = Producto::create( array(
				'usuario_id' 		=> $usuario->id,
				'optica_id'		 	=> $optica->id,
				'estado_id'		 	=> $estado_pendiente_id,
				'activo'			=> 1,
				'tipo_producto_id' 	=> $tipo_producto->id
			));

			if ( is_null($producto) )
				throw new Exception("Error, no se pudo crear el producto");

			if ( $tipo_producto->nombre == 'Lentes Aereos' ) {
				$tipo_lente = TipoLente::where('nombre', $request->get('tipo_lente') )->first();

				$receta = Receta::create( array( 
					'producto_id' 	=> $producto->id, 
					'tipo_lente_id' => $tipo_lente->id
				));

				if ( is_null($receta) )
					throw new Exception("Error, no se pudo crear la receta");
					
				if ( $tipo_lente->nombre != 'Monofocal' || $request->get('lejos_cerca') == 'ambos' ) {
					Anteojo::create(array(
						'tipo' 		=> 'cerca',
						'receta_id' => $receta->id
					));
					Anteojo::create(array(
						'tipo' 		=> 'lejos',
						'receta_id' => $receta->id
					));
				}else{
					Anteojo::create(array(
						'tipo' 		=> $request->get('lejos_cerca'),
						'receta_id' => $receta->id
					));
				}
				
			}else{
				$receta_lente = RecetaLente::create( array( 'producto_id' => $producto->id ) );
				LentePrueba::create( array( 'receta_lente_id' => $receta_lente->id ) );
				LenteOftalmologo::create( array( 'receta_lente_id' => $receta_lente->id ) );
			}

			$response = ['error' => false,  'usuario_id' => $usuario->id, 'producto_id' => $producto->id];
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}
		return $response;
	}

	public function getRecetas( Request $request ){
		try {

			$administrador = Administrador::find( Auth::id() );

			$optica = Optica::find( session('optica_id') );
			$recetas = $optica->productos()->get();

			if ( $request->ajax() ) {
				$recetas = $recetas->select('created_at')->get();
				return json_encode( $recetas );
			}

			$response = view('receta.recetaList', [
    			'rutas'     => [ 
                    ['url' => '/estadisticas', 'nombre' => 'Inicio'], 
                    ['url' => '/recetas', 'nombre' => 'Recetas', 'last' => true], 
                ],
                'recetas'  	=> $recetas, 
                'optica'  	=> $optica, 
                'admin'  	=> $administrador, 
    		]);

		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}

		return $response;
	}

	public function updateEstado( $producto_id, Request $request ){
		try {
			$producto = Producto::find( $producto_id );
			if (!$producto)
				throw new Exception("No se encontró la receta");
			
			if ( $request->get('estado') == 'Eliminar' ) {
				$producto->update( array( 'activo' => 0 ) );
			}else{
				$estado_id =  Estado::where( 'nombre', $request->get('estado') )->first()->id;
				$producto->update( array( 'estado_id' => $estado_id ) );
			}

			$response = ['error' => false, 'msj' => 'La receta se actualizo correctamente'];
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}

		return $response;
	}
	
	public function downloadReceta( $usuario_id, $producto_id ){
		try {
			header("Content-type:application/pdf"); 

			$dompdf = new Dompdf();
		
			$producto = Producto::find( $producto_id );
			$usuario = Usuario::find( $usuario_id );
			$optica = Optica::find( session('optica_id') );

			if ( $producto->tipoProducto->nombre == 'Anteojo') {
				$receta = $producto->receta;
				
				foreach ($receta->anteojos() as $anteojo) {
				    if ( $anteojo->tipo == 'cerca' ) 
				        $cerca = $anteojo;
				    else
				        $lejos = $anteojo; 
				} 

				$html = view('pdf.recetaAnteojo', [
					'usuario'	=> $usuario,
					'optica'	=> $optica,
					'receta'	=> $receta,
					'cerca'		=> $cerca,
					'lejos'		=> $lejos
				]);
			}else{
				$receta = $producto->recetaLente;

				$html = view('pdf.recetaLente', [
					'usuario'		=> $usuario,
					'optica'		=> $optica,
					'receta'		=> $receta,
					'oftalmologo'	=> $receta->oftalmologo,
					'prueba'		=> $receta->prueba,
				]);
			}

			// return $html;
			$dompdf->loadHtml( $html );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'portrait');

			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream('Recetas_'.$usuario->nombre.'-'.$usuario->apellido);
			
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}

		return $response;
	}
	
}