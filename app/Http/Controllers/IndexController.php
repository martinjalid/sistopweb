<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Optica;
use Auth;
use Hash;
use Carbon\Carbon;
	
class IndexController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index(){
		$optica = Optica::find( session('optica_id') );

		return view('index', [ 
					'optica'	=>	$optica,
					'rutas'     => [ 
		                ['url' 	=> '/estadisticas', 'nombre' => 'Inicio', 'last' => true], 
		            ],
					'admin'		=>	Auth::user(),
				] );
	}

	public function showFaq(){
		$optica = Optica::find( session('optica_id') );

		return view('faq', [ 
					'optica'	=>	$optica,
					'rutas'     => [ 
		                ['url' 	=> '/estadisticas', 'nombre' => 'Inicio'], 
		                ['url' 	=> '/preguntas', 'nombre' => 'Preguntas Frecuentes', 'last' => true ], 
		            ],
					'admin'		=>	Auth::user(),
				] );
	}
}

