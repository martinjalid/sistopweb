<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Administrador;
use App\Usuario;
use App\Optica;
use App\Producto;
use Dompdf\Dompdf;
require_once base_path('vendor/dompdf/autoload.inc.php');


class PruebaController extends Controller{
	public function pruebaPdf(){
		$dompdf = new Dompdf();
		
		$usuario = Usuario::first();
		$optica = Optica::first();
		$producto = Producto::first();
		$receta = $producto->receta;

		foreach ($receta->anteojos() as $anteojo) {
		    if ( $anteojo->tipo == 'cerca' ) 
		        $cerca = $anteojo;
		    else
		        $lejos = $anteojo; 
		} 
		
		$html = view('pdf.recetaAnteojo', [
			'usuario'	=> $usuario,
			'optica'	=> $optica,
			'receta'	=> $receta,
			'cerca'		=> $cerca,
			'lejos'		=> $lejos
		]);

		// return $html;
		$dompdf->loadHtml( $html );

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		// Output the generated PDF to Browser
		$dompdf->stream('Receta_'.$usuario->nombre.' '.$usuario->apellido);
	}
}