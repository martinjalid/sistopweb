<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Administrador;
use App\Perfil;
use App\Optica;
use App\AdministradorOptica;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notificar;
use App\TipoNotificacion;
use App\Mail\Register;
use Exception;
use Mail;
use Hash;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function viewRegister(){
        return view('register.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validacion($request)
    {
        return Validator::make($request->all(), [
            'mail' => 'required|email|max:255|unique:administrador',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $request){
        try {

            if( $this->validacion($request)->fails() )
                throw new Exception("El email ingresado ya esta registrado o es incorrecto");
            
            $perfil_id = Perfil::where('nombre', 'Administrador')->first()->id;
            $new_pass = str_random(10);

            $administrador = Administrador::create( array( 
                                'mail'                  => $request->get('mail'), 
                                'password'              => Hash::make($new_pass), 
                                'perfil_id'             => $perfil_id,
                                'activo'                => 1,
                                'plan_id'                => 1,
                                'free_trial_expires_at' => Carbon::now()->addMonths(1),
                            ) );

            $optica = Optica::create(array(
                        'nombre'    => $request->get('optica')
                    ) );

            AdministradorOptica::create( array(
                                'administrador_id' => $administrador->id, 
                                'optica_id' => $optica->id 
                            ) );

            Mail::to( $administrador->mail )->send( new Register($new_pass,$optica->nombre) ); 

            if( !count( Mail::failures() ) ){
                $tipo_notificacion_id = TipoNotificacion::where('nombre', 'Registro')->first()->id;

                Notificar::create(array(
                    'administrador_id'      => $administrador->id,
                    'tipo_notificacion_id'  => $tipo_notificacion_id,
                ));
            }

            $response = ['error' => false];
        } catch (Exception $e) {
            $response = $this->formatError($e);
        }
        return $response;
    }
}
