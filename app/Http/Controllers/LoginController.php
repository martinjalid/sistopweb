<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Exception;
use Validator;
use App\Http\Controllers\Controller;
use App\Administrador;
use App\Notificar;
use App\TipoNotificacion;
use App\AdministradorCobranza;
use App\Mail\ResetPass;
use Mail;
use Hash;
use Carbon\Carbon;


class LoginController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('guest')->except('logout');
    }
    
    public function viewLogin()
    {
        return view('login.login');
    }

    public function viewPasswordReset(){
        return view('login.email');
    }

    public function validacion($request){
        return Validator::make($request->all(), [
            'mail' => 'required|email|max:255',
            'password' => 'required|min:5',
        ]);
    }

    public function login(Request $request){
        try {   

            if( $this->validacion($request)->fails() )
                throw new Exception("Ingrese datos válidos", 1);        

            $info = [
                'mail' => $request->mail,
                'password' => $request->password,
            ];
            $remember = $request->remember == 'on' ? true : false;
            
            if( Auth::attempt($info, $remember) )
                $response = ['error' => false];
            else               
                return $response = ['error' => true, 'msj' => 'Datos incorrectos.'];

            $administrador = Administrador::find( Auth::id() );
            
            if( $administrador->perfil() == 'Administrador' && $administrador->opticas()->count() > 1){
                session( [ 'optica_id' => 0 ] );
            }else{
                $optica_id = $administrador->opticas()->first()->id;
                session(['optica_id' => $optica_id]);
            }

            $response['url'] = '/estadisticas';

            $now = Carbon::now();
            $fin = $administrador->free_trial_expires_at;
            session(['end_free_trial' => false]);
            session(['canceled_suscription' => false]);

            if ( !$administrador->estaSuscrito() ) {
                if ( !is_null($fin) && $now > $fin ) 
                    session(['end_free_trial' => true]);
                else
                    session(['end_free_trial' => false]);
            }else{
                if ( $administrador->activo == 0 ) 
                    session(['canceled_suscription' => true]);
                else
                    session(['canceled_suscription' => false]);
            }

        } catch (Exception $e) {
            $response = $this->formatError($e);
        }

        return $response;
    }

    public function logOptica(Request $request){
        try {
            session([ 'optica_id' => $request->get('optica_id') ]);
            $response = ['error' => false];
        } catch (Exception $e) {
            $response = $this->formatError($e);
        }
        return $response;
    }

    public function logout(Request $request){
        Auth::logout();
        return redirect('/login');
    }

    public function passwordReset(Request $request)
    {   
        try {
            $email = $request->email;
            if( !$email )
                throw new Exception("Ingrese un email", 1);

            $administrador = Administrador::where('mail', '=', $email)->first();
            if( !$administrador )
                throw new Exception("Email no registrado", 1);

            $new_pass = str_random(10);

            $administrador->password = Hash::make($new_pass);
            $administrador->save();

            Mail::to( $administrador->mail )->send( new ResetPass($new_pass)); 

            if( !count( Mail::failures() ) ){
                $tipo_notificacion_id = TipoNotificacion::where('nombre', 'Reset Pass')->first()->id;
                
                Notificar::create(array(
                    'administrador_id'      => $administrador->id,
                    'tipo_notificacion_id'  => $tipo_notificacion_id,
                ));
            }

            $response = ['error' => false];    
            
        } catch (Exception $e) {
            $response = $this->formatError($e);
        }
        return $response;
    }
}
