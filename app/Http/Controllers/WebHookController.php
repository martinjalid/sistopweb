<?php  


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\Request;
use App\Administrador;
use App\Notificar;
use App\AdministradorCobranza;
use Exception;
use Validator;
use Mail;
use Hash;
use Carbon\Carbon;

class WebHookController extends Controller {

	public function prueba( Request $request ){
		$input = $request->all();

		Notificar::create(array(
            'administrador_id'      => 1,
            'tipo_notificacion_id'  => 2,
        ));

		// $cobranza = AdministradorCobranza::create([
		// 	'administrador_id'	=> 1,
		// 	'estado_id'			=> 1,
		// 	'fecha_cuota'		=> '2018-06-06',
		// 	'plan_id'			=> 1,
		// 	'monto'				=> 200
		// ]);
		return $input;
	}

}