<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
	
class HomeController extends Controller {

	public function viewLanding(){
		return view('landing.index');
	}
}
?>