<?php  


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\Request;
use App\Administrador;
use App\AdministradorOptica;
use App\AdministradorCobranza;
use App\Profesional;
use App\Perfil;
use App\Optica;
use App\Plan;
use App\Notificar;
use App\TipoNotificacion;
use App\Estado;
use Exception;
use Validator;
use Mail;
use Hash;
use Carbon\Carbon;
require_once base_path('vendor/mercadopago/sdk/lib/mercadopago.php');

class SuscripcionController extends Controller {

	public function show(){
		try {

			$admin = Auth::user();

			if ( $admin->perfil() != 'Administrador' ) 
				return redirect('/estadisticas');

			$optica = Optica::find( session('optica_id') );

			$tarjetas = [];
			$mp = new \MP ( config('services.mercadopago.access_token') );

			$mail = $admin->id == 1 ? 'test@test.com' : $admin->mail;
			$proxima_cuota = '';
			if ( $admin->suscripcion_mp != '' ) {
				$suscripcion = $mp->get("/v1/subscriptions/".$admin->suscripcion_mp);
				$proxima_cuota = $suscripcion['response']['charges_detail']['next_payment_date'];
				$proxima_cuota = new Carbon($proxima_cuota);
				$proxima_cuota = $proxima_cuota->format('d/m/Y');	
			}

			$customer = $mp->get("/v1/customers/search", array( "email" => $mail ));

			if( !empty($customer["response"]['results']) ){
				$customer_id = $customer["response"]["results"][0]['id'];
				$default_card = $customer["response"]["results"][0]['default_card'];
				$cards = $mp->get ("/v1/customers/".$customer_id."/cards");
				$tarjetas = $cards['response'];
			}
			
			$response = view('suscripcion.index',  [
				'rutas'     		=> [
	                ['url' 			=> '/estadisticas', 'nombre' => 'Inicio'], 
	                ['url' 			=> '/suscripcion', 'nombre' => 'Suscripcion', 'last' => true], 
	            ],
				'optica'			=> $optica,
				'admin'				=> $admin,
				'tarjetas'			=> $tarjetas,
				'default_card'		=> isset($default_card) ? $default_card : '',
				'suscripcion_status'	=> isset( $suscripcion ) ? $suscripcion['response']['status'] : '',
				'proxima_cuota'		=> $proxima_cuota, 
				'plan'				=> Plan::find( $admin->plan_id ),
			]);
		} catch (Exception $e) {
			$response = $this->formatError($e);
		}
		return $response;
	}

	public function asociarTarjeta( Request $request ){
		try {
			$admin = $request->user();
			$response = [];

			$mp = new \MP ( config('services.mercadopago.access_token') );
		
			$mail = $admin->id == 1 ? 'test@test.com' : $admin->mail;

			$customer = $mp->get("/v1/customers/search", array( "email" => $mail ));

			if( empty($customer["response"]['results']) ){
				$customer = $mp->post ("/v1/customers", array("email" => $mail )); 
				$customer_id = $customer["response"]["id"];
			}else{
				$customer_id = $customer["response"]['results'][0]["id"];
			}

			//ACA ASOCIO LA TARJETA AL CUSTOMER
			$card = $mp->post ("/v1/customers/".$customer_id."/cards",  ["token" => $request->token ] );

			if( $card['status'] == 200 || $card['status'] == 201){
 				$mp->put ("/v1/customers/".$customer_id, ['default_card' => $card['response']['id'] ] );


				if ( $admin->suscripcion_mp == '' ) {
					$plan = Plan::find( $admin->plan_id );
					
					$suscripcion = $mp->post('/v1/subscriptions', [ 'plan_id' => $plan->id_mp, 'payer' => [ 'id' => $customer_id ] ] );
					$admin->update([ 'suscripcion_mp' => $suscripcion['response']['id'] ]);
                    session(['end_free_trial' => false]);
				}
			}
			else
				throw new Exception("Hubo un error al añadir la tarjeta");
			

			$response['error'] = false;
			$response['msj'] = 'La tarjeta se añadio correctamente';
		} catch (Exception $e) {
			$response = $this->formatError($e);
		}
		return json_encode($response);
	}

	public function deleteTarjeta( Request $request ){
		try {
			$admin = $request->user();

			$mp = new \MP ( config('services.mercadopago.access_token') );

			$mail = $admin->id == 1 ? 'test@test.com' : $admin->mail;

			$customer = $mp->get("/v1/customers/search", array( "email" => $mail ));
			
			$customer_id = $customer["response"]['results'][0]["id"];
				
			$card = $mp->delete ("/v1/customers/".$customer_id."/cards/".$request->card_id );

			if ( $card['status'] == 200 ) 
				$response = json_encode([ 'error' => false]);
			else
				throw new Exception("Hubo un error al eliminar la tarjeta");
	
		} catch (Exception $e) {
			$response = $this->formatError($e);
		}
		return $response;
	}

	public function updateDefaultTarjeta( Request $request ){
		try {
			$admin = $request->user();

			$mp = new \MP ( config('services.mercadopago.access_token') );

			$mail = $admin->id == 1 ? 'test@test.com' : $admin->mail;

			$customer = $mp->get("/v1/customers/search", array( "email" => $mail ));
			
			$customer_id = $customer["response"]['results'][0]["id"];

			$customer = $mp->put ("/v1/customers/".$customer_id, ['default_card' => $request->card_id] );

			if ( $customer['status'] == 200 ) 
				$response = json_encode([ 'error' => false]);
			else
				throw new Exception("Hubo un error");
	
		} catch (Exception $e) {
			$response = $this->formatError($e);
		}
		return $response;
	}

	public function editSuscripcion( Request $request ){
		try {
			$admin = $request->user();

			$admin->estaSuscrito();

			$opticas = $admin->opticas();

			$mp = new \MP ( config('services.mercadopago.access_token') );
			
			$suscripcion = $mp->get("/v1/subscriptions/".$admin->suscripcion_mp);

			$status = $request->action == 'stop' ? 'paused' : 'authorized';
			
			$suscripcion = $mp->put("/v1/subscriptions/".$admin->suscripcion_mp, [ 'status' => $status ]);

			$activo = $request->action == 'stop' ? 0 : 1;
			
			foreach ($opticas as $optica) {
				$usuarios = $optica->usuarios()->get();
				
				foreach ($usuarios as $usuario) {
					$usuario->update(['activo' => $activo]);
				}

			}

            session(['canceled_suscription' => $activo == 1 ? false : true]);

			$response = json_encode(['error' => false]);
		} catch (Exception $e) {
			$response = $this->formatError($e);
		}
		return $response;
	}

	public function checkSuscripcion( Request $request ){
		try {
			$admin = $request->user();

			if ( $admin->perfil() != 'Administrador' )
				$admin = $this->getAdministradorGral();

			if ( $admin->suscripcion_mp != '' ) {
				$mp = new \MP ( config('services.mercadopago.access_token') );
				$suscripcion = $mp->get("/v1/subscriptions/".$admin->suscripcion_mp);
				$response = json_encode([ 'error' => false, 'status' => $suscripcion['response']['status'] ]);
			}else{
				$response = json_encode([ 'error' => false, 'status' => 'no_suscript']);
			}
		} catch (Exception $e) {
			$response = $this->formatError($e);
		}
		return $response;
	}
}