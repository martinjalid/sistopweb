<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Optica;
use App\Usuario;
use App\Estado;
use App\Producto;
use App\Receta;
use App\RecetaLente;
use App\Anteojo;
use App\LentePrueba;
use App\LenteOftalmologo;
use App\TipoProducto;
use App\Administrador;
use App\Profesional;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Exception;

	
class EstadisticasController extends Controller {

	public function getRecetasByEstados( Request $request ){
		try {
			if ( $request->get('optica_id') == 'all' )
				$opticas = Auth::user()->getOpticasIds();
			else
				$opticas = [ $request->get('optica_id') ];
			
			$estados = Estado::where('tipo', 'Producto')->orderBy('id')->get();
			$array = [];

			$productos = $this->getProductos( $request, $opticas )->count();

			if (!$productos)
				return $array;


			foreach ($estados as $estado ) {
				$array[$estado->nombre] = ($this->getProductos( $request, $opticas )->where('estado_id', $estado->id)->count()/$productos) * 100;
			}

			$response = $array;
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}
		return $response;
	}

	public function getProductos( $request, $opticas ){
		return Producto::whereBetween('created_at', [ $request->get('start'), $request->get('end').' 23:59:59' ] )->where('activo', 1)->whereIn('optica_id', $opticas);
	}

	public function getRecetasByMedico( Request $request ){
		try {
			if ( $request->get('optica_id') == 'all' )
				$opticas = Auth::user()->getOpticasIds();
			else
				$opticas = [ $request->get('optica_id') ];

			$profesionales = Profesional::whereIn('optica_id', $opticas)->get();

			$productos = $this->getProductos( $request, $opticas )->count();

			if (!$productos)
				return $array = [];

			$array = [
				'Sin Profesional' => ($this->getProductos( $request, $opticas )->where('profesional_id', null)->count() / $productos) * 100,
			];

			foreach ($profesionales as $profesional ) {
				$array[$profesional->nombre] = ($this->getProductos( $request, $opticas )->where('profesional_id', $profesional->id)->count() / $productos) * 100;
			}

			$response = $array;
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}
		return $response;
	}

	public function getInfoBoxes( Request $request ){
		try {
			if ( $request->get('optica_id') == 'all' )
				$opticas = Auth::user()->getOpticasIds();
			else
				$opticas = [ $request->get('optica_id') ];

			$array = [
				'clientes' 	=> Usuario::whereIn('optica_id', $opticas)->whereBetween('created_at', [ $request->get('start'), $request->get('end').' 23:59:59' ] )->count(),
				'recetas' 	=> $this->getProductos( $request, $opticas )->count() ,
			];
			
			return $array;
		} catch (Exception $e) {
			return 	$response = $this->formatError($e);
		}
	}

}