<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\ObraSocial;
use App\Receta;
use App\Optica;
use App\Administrador;
use App\Color;
use App\MaterialLente;
use App\Tratamiento;
use App\Profesional;
use App\Usuario;
use App\Producto;
use App\TipoLente;
use App\Notificar;
use App\TipoNotificacion;
use \Illuminate\Http\Request;
use Auth;
use Exception;
use App\Mail\NotificarProducto;
use Mail;


class UsuarioController extends Controller {

	public function editCreate($usuario_id = null, Request $request){
		try{
			$usuario = Usuario::find($usuario_id);

			if ( !$usuario )
				$usuario = Usuario::create( $request->all() );
			else
				$usuario->update( $request->all() );

			$response = ['error' => false];

		}catch( Exception $e ){
    		$response = $this->formatError($e);
		}

		return $response;
	}

	public function saveMail( Request $request ){
		try {
			$usuario = Usuario::find(Producto::find( $request->get('producto_id') )->usuario_id);

			if ( is_null(Usuario::where('mail', $request->get('mail') )->first()) ) {
				$usuario->update( [ 'mail' => $request->get('mail') ] );
			}else
				throw new Exception("Ya hay un usuario con el mail ".$request->get('mail'));

			$response = ['error' => false, 'mail' => $request->get('mail')];
		}catch( Exception $e ){
    		$response = $this->formatError($e);
		}

		return $response;
	}

	public function sendMail( Request $request ){
		try {
			$usuario = Usuario::find(Producto::find( $request->get('producto_id') )->usuario_id);
			$optica = Optica::find(Producto::find( $request->get('producto_id') )->optica_id);

            Mail::to( $usuario->mail )->send( new NotificarProducto($usuario->nombre, $optica->nombre) ); 

            if( !count( Mail::failures() ) ){
				$tipo_notificacion_id = TipoNotificacion::where('nombre', 'Notificacion Cliente')->first()->id;

	            Notificar::create(array(
	                'administrador_id'      => Auth::id(),
	                'usuario_id'      		=> $usuario->id,
	                'tipo_notificacion_id'  => $tipo_notificacion_id,
	            ));
            }


			$response = ['error' => false, 'mail' => $usuario->mail];
		}catch( Exception $e ){
    		$response = $this->formatError($e);
		}

		return $response;
	}

	public function showCliente( $usuario_id, $producto_id){
		try {
			$optica = Optica::find( session('optica_id') );
			$usuario = Usuario::find($usuario_id);
			$producto = $usuario->productos()->find($producto_id);
			$admin = Auth::user();

			if ( $producto->tipoProducto->nombre == 'Lentes Aereos' ) {
				$receta = $producto->receta;
			}else{
				$receta = $producto->recetaLente;
				$prueba = $receta->prueba;
				$oftalmologo = $receta->oftalmologo;
			}
			
			$obras = ObraSocial::orderBy('id')->get();
			$material_lente = MaterialLente::orderBy('id')->get();
			$color = Color::orderBy('id')->get();
			$tratamiento = Tratamiento::orderBy('id')->get();

			if ( is_null( $receta ) )
				throw new Exception("No se encontro la receta", 1);

			$tipos_lente = TipoLente::all();

			$response = view('usuario.usuarioEdit', [
				'optica' 		=> $optica,
    			'obras'			=> $obras,
    			'receta'		=> $receta,
    			'producto'		=> $producto,
                'rutas'     => [ 
                    ['url' => '/estadisticas', 'nombre' => 'Inicio'], 
                    ['url' => '/cliente', 'nombre' => 'Clientes'], 
                    ['url' => '/cliente/'.$usuario->id.'/receta/'.$producto->id, 'nombre' => $usuario->nombre.' '.$usuario->apellido, 'last' => true], 
                ],
                'usuario'   	=> $usuario,
				'material_lente'=> $material_lente,
				'color'			=> $color,
				'tratamiento'	=> $tratamiento,
				'tipos_lente'	=> $tipos_lente, 
				'prueba'		=> !isset($prueba) ? '' : $prueba, 
				'oftalmologo'	=> !isset($oftalmologo) ? '' : $oftalmologo, 
				'admin'			=> $admin
    		]);

		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}

		return $response;
	}

	public function getCliente( Request $request ){
		try {
			$administrador = Administrador::find( Auth::id() );
			$optica = Optica::find( session('optica_id') );
			$usuarios = $optica->getClientes()->get();
			$obras = ObraSocial::orderBy('id')->get();

			$response = view('usuario.usuarioList', [
    			'rutas'     => [ 
                    ['url' => '/estadisticas', 'nombre' => 'Inicio'], 
                    ['url' => '/cliente', 'nombre' => 'Clientes', 'last' => true], 
                ],
                'usuarios'  => $usuarios, 
                'optica'  	=> $optica, 
                'admin'  	=> $administrador, 
    			'obras'		=> $obras,
    		]);

		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}

		return $response;
	}

}