<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Administrador;
use App\Notificar;
use App\Plan;
use App\Optica;
use App\TipoNotificacion;
use \Illuminate\Http\Request;
use App;
use Auth;
use Exception;
require_once base_path('vendor/mercadopago/sdk/lib/mercadopago.php');


class SuperAdminController extends Controller {

	public function show(){
		try {
			$admin = Auth::user();
			
			$response = view('superadmin.index', [
    			'rutas'     => [ 
                    ['url' => '/estadisticas', 'nombre' => 'Inicio'], 
                    ['url' => '/superadmin', 'nombre' => 'SuperAdmin', 'last' => true], 
                ],
                'admin'			=> $admin 
    		]);
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}

		return $response;
	}

	public function showNotificaciones(){
		try {
			$notificaciones = Notificar::orderBy('id')->get();
			$administrador = Auth::user();

			$response = view('superadmin.notificacionesList', [
    			'rutas'     => [ 
                    ['url' => '/estadisticas', 'nombre' => 'Inicio'], 
                    ['url' => '/superadmin', 'nombre' => 'SuperAdmin'], 
                    ['url' => '/notificaciones', 'nombre' => 'Notificaciones', 'last' => true], 
                ],
                'notificaciones'  => $notificaciones,
                'admin'			=> $administrador 
    		]);

		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}

		return $response;
	}

	public function showPlanes(){
		try {
			$planes = Plan::orderBy('id')->get();
			$administrador = Auth::user();

			$mp = new \MP ( config('services.mercadopago.access_token') );
			
			$response = view('superadmin.planes', [
    			'rutas'     => [ 
                    ['url' => '/estadisticas', 'nombre' => 'Inicio'], 
                    ['url' => '/superadmin', 'nombre' => 'SuperAdmin'], 
                    ['url' => '/planes', 'nombre' => 'Planes', 'last' => true], 
                ],
                'planes'  => $planes,
                'admin'			=> $administrador 
    		]);

		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}

		return $response;
	}

	public function showOpticas(){
		try {
			$opticas = Optica::withCount(['productos', 'getClientes'])->orderBy('id')->get();
			$administrador = Auth::user();

			$response = view('superadmin.opticas', [
    			'rutas'     => [ 
                    ['url' => '/estadisticas', 'nombre' => 'Inicio'], 
                    ['url' => '/superadmin', 'nombre' => 'SuperAdmin'], 
                    ['url' => '/planes', 'nombre' => 'Planes', 'last' => true], 
                ],
                'opticas'  => $opticas,
                'admin'		=> $administrador 
    		]);

		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}

		return $response;
	}

	public function savePlanes( Request $request ){
		try {
			$planes = $request->all();
			$mp = new \MP ( config('services.mercadopago.access_token') );

			foreach ($planes as $plan_id => $value) {
				$plan = Plan::find( $plan_id );
				$plan->update( $value );

				if ( $plan->id_mp == '') {
					$plan_mp = $mp->post('/v1/plans/', 
						array( 
							'description' => $plan->nombre, 
							'auto_recurring' => [ 'frequency' => 1, 'frequency_type' => 'months', 'transaction_amount' => (int) $plan->monto ] 
						)
					);

					$plan->update([ 'id_mp' => $plan_mp['response']['id'] ]);
				}else{
					$plan_mp = $mp->get('/v1/plans/'.$plan->id_mp);
					$mp->put('/v1/plans/'.$plan->id_mp,['auto_recurring' => [ 'transaction_amount' => (int) $plan->monto ] ]);
				}
			}

			$response = json_encode([ 'error' => false ]);
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}
		return $response;
	}

	public function getAdministradores()
	{
		try {
			$administradores = Administrador::where('id', '!=', 1)->count();

			return [
					'clientes' => $administradores,
			];
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}
		return $response;
	}

	public function getAdministradoresByPago()
	{
		try {
			$administradores = Administrador::where('id', '!=', 1);

			return [
					'free' 		=> $administradores->where('suscripcion_mp', '=', '' )->count(),
					'premium' 	=> $administradores->where('suscripcion_mp', '!=', '' )->count(),
			];
		} catch (Exception $e) {
    		$response = $this->formatError($e);
		}
		return $response;
	}
}