<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificarProducto extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nombre, $optica)
    {
        $this->nombre = $nombre;
        $this->optica = $optica;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = array( 
            'nombre' => $this->nombre,
            'optica' => $this->optica
        );

        return $this->view('mail.notificarProducto', $data)->subject('Su receta esta lista');
    }
}