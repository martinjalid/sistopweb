<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUsuario extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($new_pass, $optica)
    {
        $this->new_pass = $new_pass;
        $this->optica = $optica;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = array( 
                    'new_pass'  => $this->new_pass, 
                    'optica'    => $this->optica
                );

        return $this->view('mail.newUsuario', $data)->subject('Bienvenido a SistopWeb');
    }
}
