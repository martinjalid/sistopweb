<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model{

	protected $table = 'estado';

	static public function getByNombre($nombre){
		return self::where('nombre', $nombre)->first();
	}
}