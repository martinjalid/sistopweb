<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificar extends Model{

	protected $table = 'notificar';

	protected $fillable = [
		'administrador_id',
		'usuario_id',
		'tipo_notificacion_id',
	];

	public function tipo_notificacion(){
        return $this->hasOne('App\TipoNotificacion', 'id', 'tipo_notificacion_id')->first()->nombre;
    }

    public function administrador(){
        return $this->hasOne('App\Administrador', 'id', 'administrador_id')->first();
    }

    public function usuario(){
        return $this->hasOne('App\Usuario', 'id', 'usuario_id')->first();
    }
}