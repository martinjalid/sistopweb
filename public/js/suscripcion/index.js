var sus = Suscripcion = function(){
	console.log('SUSCRIPCION');

	this.general = new General();
	this.general.csrf = document.getElementsByName('_token')[0].value;

	this.init();
	this.bind_events();
}

Suscripcion.prototype.init = function(){

}

Suscripcion.prototype.bind_events = function(){
	var _this = this;

	$(".deleteCard").click(function(event) {
	    var card_id = $(this).attr('tarjeta-id');

	    swal({
	        title: 'Desea eliminar esta tarjeta?',
	        type: 'question',
	        confirmButtonColor: '#e43d3d',
	        confirmButtonText: 'Eliminar',
	        showCancelButton: true
	    }).then((result) => {
	        if ( result.dismiss == undefined ) {
	            var info = {
	                card_id :card_id,
	            }
	            _this.general.send_ajax('POST', '/suscripcion/deleteTarjeta', info, _this.callback );
	        }
	    })
	});

	$(".defaultCard").click(function(event) {
	    var card_id = $(this).attr('tarjeta-id');

	    swal({
	        title: 'Desea realizar los pagos con esta tarjeta?',
	        type: 'question',
	        confirmButtonColor: '#8bc34a',
	        confirmButtonText: 'Guardar',
	        showCancelButton: true
	    }).then((result) => {
	        if ( result.dismiss == undefined ) {
	            var info = {
	                card_id :card_id,
	            }
	            _this.general.send_ajax('POST', '/suscripcion/updateDefaultTarjeta', info, _this.callback );
	        }
	    })
	});

	$(".suscripcion").click(function(){
		var action = $(this).attr('action');
		
		swal({
	        title: 'Desea '+( action == 'stop' ? 'cancelar' : 'reanudar' )+' su suscripcion a SistOpWeb?',
	        type: 'question',
	        confirmButtonColor: '#8bc34a',
	        confirmButtonText: 'Guardar',
	        showCancelButton: true
	    }).then((result) => {
	        if ( result.dismiss == undefined ) {
	            var info = {
	                action :action,
	            }
	            _this.general.send_ajax('POST', '/suscripcion/editSuscripcion', info, _this.callback );
	        }
	    })
	});
}

Suscripcion.prototype.callback = function(){
	location.reload();
};