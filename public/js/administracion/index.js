var Adm = Administracion = function () {
	console.log('ADMINISTRACION');

	this.general = new General();
	this.general.csrf = document.getElementsByName('_token')[0].value;
	this.bind_events();
}

Administracion.prototype.bind_events = function(){
	var _this = this;

	document.getElementById('save_new_password').addEventListener('click', function(){
		$(this).attr('disabled', true);
		var old_pass = document.getElementById('old_password');
		var new_pass = document.getElementById('new_password');
		var re_pass = document.getElementById('re_password');
		var error = false;
		if ( old_pass.value == '' ){
			_this.general.add_error(old_pass);
		}

		if ( new_pass.value == '' ){
			_this.general.add_error(new_pass);
		}

		if ( re_pass.value == '' ){
			_this.general.add_error(re_pass);
		}

		if (error) {
			toastr['error']('Complete todos los campos.');
			error = true;
		}

		if ( new_pass.value != re_pass.value ) {
			toastr['error']('Las nuevas contraseñas no coinciden.');
			_this.general.add_error(re_pass);
			error = true;
		};

		if ( new_pass.value == old_pass.value ) {
			toastr['error']('Las nueva contraseña nueva no puede ser igual a la actual.');
			_this.general.add_error(new_pass);
			error = true;
		};

		var info = {
			new_pass : new_pass.value,
			old_pass : old_pass.value
		}

		if ( !error ) {
			_this.general.send_ajax('POST', location.href+'/change_password', info, _this.callback_new_password, true);
		}else{
			$(this).removeAttr('disabled');
		}
	});
}

Administracion.prototype.callback_new_password = function(resp){
	var _this = this;

	if (resp.error) {
		toastr['error'](resp.msj, 'Error');
		for (var i = resp.selector.length - 1; i >= 0; i--) {
			selector = document.getElementById( resp.selector[i] );
			_this.General.prototype.add_error( selector );
		}
	}else{
		toastr.options.onHidden = function() { 
			location.reload();
		}
		toastr['success']('La contraseña se cambio exitosamente', 'Ok');
	}
}

var Med = Medico = function(){
	console.log('MEDICO');

	this.table = $(".table").DataTable({
		columns: [
	        { data: 'nombre' },
	        { data: 'optica' },
	        { data: 'activo' },
	        { data: 'position' }
    	]
	});

	this.general = new General();
	this.general.csrf = document.getElementsByName('_token')[0].value;
	this.bind_events();
}

Medico.prototype.bind_events = function(){
	var _this = this;

	$('.edit_medico').on( 'click', function () {
		$(".modal-title").text('Editar Profesional')

		$('#modal_nombre').val( $(this).attr('nombre') );
		$('#modal_optica').val( $(this).attr('optica-id') ).trigger('change');
		$("#save_medico").attr('medico-id', $(this).attr('medico-id') );
	} );

	$("#new_medico").click(function(event) {
		$(".modal-title").text('Nuevo Profesional')
		$('#modal_nombre').val('');
		$('#modal_optica').val('0').trigger('change');
		$("#save_medico").attr('medico-id', 'new');
	});

	$("#save_medico").click(function(event) {
		$(this).attr('disabled', 'true');
		
		var medico_id = $(this).attr('medico-id');

		if ( $('#modal_nombre').val() == '' ) {
			$(this).removeAttr('disabled');
			toastr['error']('Complete el nombre', 'Error');
			return false;			
		}

		if ( $('#modal_optica').val() == '0' ) {
			$(this).removeAttr('disabled');
			toastr['error']('Complete la óptica', 'Error');
			return false;			
		}

		var info = {
			nombre 			: $('#modal_nombre').val(),
			optica_id 		: $('#modal_optica').val() == undefined ? window.optica_id : $('#modal_optica').val(),
			profesional_id 	: medico_id,
			activo 			: $('#modal_activo').val()
		}

		_this.general.send_ajax('POST', location.href+'/save_medico', info, _this.callback);		
	});
};

Medico.prototype.callback = function(){
	location.reload();
}

var Usu = Usuario = function(){
	console.log('USUARIOS');

	this.table = $(".table").DataTable({
		columns: [
	        { data: 'mail' },
	        { data: 'optica' },
	        { data: 'activo' },
	        { data: 'position' }
    	],
	});

	this.general = new General();
	this.general.csrf = document.getElementsByName('_token')[0].value;
	this.bind_events();
}

Usuario.prototype.bind_events = function(){
	var _this = this;
	var optica_id = '';

	$("#new_usuario").click(function(event) {
		$("#contraseña").show();
		$(".modal-title").text('Nuevo Usuario')
		$('#modal_optica').val('0').trigger('change');
		$('#modal_mail').val('');
		$("#save_usuario").attr('usuario-id', 'new');
	});

	$(".edit").click(function(event) {
		var usuario_id = $(this).attr('usuario-id');
		
		$("#contraseña").hide();
		$('#modal_optica').val( $(this).attr('optica-id') ).trigger('change');
		$("#save_usuario").attr('usuario-id', usuario_id);
		$("#modal_mail").val( $(this).attr('mail') );	
	});

	$("#save_usuario").click(function(event) {
		$(this).attr('disabled', 'true');
		
		var administrador_id = $(this).attr('usuario-id');

		if ( $('#modal_optica').val() == '0' ) {
			$(this).removeAttr('disabled');
			toastr['error']('Complete la óptica', 'Error');
			return false;			
		}

		var info = {
			mail 			: $('#modal_mail').val(),
			password 		: $('#modal_password').val(),
			optica_id 		: $('#modal_optica').val() == undefined ? window.optica_id : $('#modal_optica').val(),
			administrador_id: administrador_id,
			activo 			: $('#modal_activo').val()
		}

		_this.general.send_ajax('POST', location.href+'/save_usuario', info, _this.callback_new_usuario, true);		
	});
}

Usuario.prototype.callback_new_usuario = function(resp){
	if ( resp.error ) {
		toastr['error'](resp.msj, 'Error');
		$("#save_usuario").removeAttr('disabled');
	}else{
		toastr.options.onHidden = function() { 
			location.reload();
		}
		toastr['success']('Revise el mail ingresado para verificar su registro.', 'El usuario se creo correctamente');
	}
}


var Opt = Optica = function(){
	console.log('OPTICA');

	this.table = $(".table").DataTable({
		columns: [
	        { data: 'nombre' },
	        { data: 'activo' },
	        { data: 'position' }
    	]
	});

	this.general = new General();
	this.general.csrf = document.getElementsByName('_token')[0].value;
	this.bind_events();
}

Optica.prototype.bind_events = function(){
	var _this = this;

	$('tbody').on( 'click', 'tr', function () {
		$(".modal-title").text('Editar Óptica')
		var rowData = _this.table.row( this ).data();
		$('#modal_nombre').val( rowData.nombre );
		$("#save_optica").attr('optica-id', $(this).attr('optica-id') );
	} );

	$("#new_optica").click(function(event) {
		$(".modal-title").text('Nueva Óptica')
		$('#modal_nombre').val('');
		$("#save_optica").attr('optica-id', 'new');
	});

	$("#save_optica").click(function(event) {
		$(this).attr('disabled', 'true');
		var optica_id = $(this).attr('optica-id');

		if ( $('#modal_nombre').val() == '' ) {
			toastr['error']('Complete el nombre', 'Error');
			$(this).removeAttr('disabled');
			return false;			
		}

		if ( $('#modal_optica').val() == '0' ) {
			toastr['error']('Complete la óptica', 'Error');
			$(this).removeAttr('disabled');
			return false;			
		}

		var info = {
			nombre 			: $('#modal_nombre').val(),
			optica_id 		: optica_id,
			activo 			: $('#modal_activo').val()
		}

		_this.general.send_ajax('POST', location.href+'/save_optica', info, _this.callback);		

	});
};

Optica.prototype.callback = function(){
	location.reload();
}