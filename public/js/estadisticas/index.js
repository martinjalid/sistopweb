var est = Estadisticas = function () {
	moment.locale('es');
	var start = moment().startOf('month').format('YYYY-MM-DD');
    var end = moment().format('YYYY-MM-DD');

    this.run(window.optica_id, start, end);
    this.bind_events();
}

Estadisticas.prototype.bind_events = function(){
	var _this = this;

    var start = moment().startOf('month');
    var end = moment();
    var select = document.getElementById('optica_estadisticas');

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
    }

    var picker = $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Hoy'			: [moment(), moment()],
           'Ayer'			: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Semana Pasada'	: [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
           'Este Mes'		: [moment().startOf('month'), moment().endOf('month')],
           'Mes Anterior'	: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'Siempre'		: [moment('2017-01-01') , moment()],
        }
    }, cb);

    cb(start, end);
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
    	start = picker.startDate;
    	end =  picker.endDate;

    	if ( select ) {
	   		_this.run( $("#optica_estadisticas").val(), picker.startDate.format('YYYY-MM-DD'), picker.endDate.endOf('day').format('YYYY-MM-DD') );
    	}else{
	   		_this.run( window.optica_id, picker.startDate.format('YYYY-MM-DD'), picker.endDate.endOf('day').format('YYYY-MM-DD') );
    	}
	});
	
	if ( select ) {
		$("#text").html( 'ESTADISTICAS DE '+$("#optica_estadisticas").find('option:selected').text().toUpperCase() );
	}else{
		$("#text").html( 'ESTADISTICAS DE '+$("#sidebar_optica").text().toUpperCase() );
	}


	$("#optica_estadisticas").change(function(event) {
		_this.run( $(this).val(), start.format('YYYY-MM-DD'), end.endOf('day').format('YYYY-MM-DD'));
		$("#text").html( 'ESTADISTICAS DE '+$(this).find('option:selected').text().toUpperCase() );
	});

}

Estadisticas.prototype.run = function(optica_id, start, end){
	var _this = this;

	this.recetasPorEstados(optica_id, start, end);
	this.recetasPorMedicos(optica_id, start, end);
	this.infoBoxes(optica_id, start, end);	
}

Estadisticas.prototype.infoBoxes = function(optica_id, start, end){
	$.ajax({
		url: 'estadisticas/getInfoBoxes',
		dataType: 'json',
		data: {
			optica_id 	: optica_id,
			start 		: start,
			end 		: end
		},
	})
	.done(function(resp) {
		var boxes =  Object.keys(resp);
		for (var i = boxes.length - 1; i >= 0; i--) {
			$("#infobox_"+boxes[i]).countTo({ from: 0, to: resp[ boxes[i] ] });
		}
	})
	
}

Estadisticas.prototype.recetasPorEstados = function(optica_id, start, end){
	var _this = this;

	$.ajax({	
    	url: 'estadisticas/getRecetasByEstados',
		dataType: 'json',
    	data: {
    		optica_id 	: optica_id,
    		start 		: start,
    		end 		: end
    	}
    }).done( function(resp){
    	var estados = Object.keys(resp);
    	var data = []; 
    	var colors = { 'Pendiente' : '#2a8ad4', 'Entregada' :'#8cc549' };

    	for (var i = 0; i < estados.length; i++) {
    		if ( resp[ estados[i] ] ) {
    			data.push({ 'label' : estados[i], 'value' : Math.round( resp[ estados[i] ] * 10) / 10, 'color' : colors[ estados[i] ] });
    		}
    	}
    	if (data.length) {
    		_this.draw_donut_chart(data, 'recetas_estado');
    	}else
    		$("#recetas_estado").html('<p style="text-align:center">No Hay recetas cargadas para el rango de fechas seleccionado</p>')
   
    })
}

Estadisticas.prototype.recetasPorMedicos = function(optica_id, start, end){
	var _this = this;

	$.ajax({	
    	url: 'estadisticas/getRecetasByMedico',
		dataType: 'json',
    	data: {
    		optica_id 	: optica_id,
    		start 		: start,
    		end 		: end
    	}
    }).done( function(resp){
    	var estados = Object.keys(resp);
    	var data = []; 
    	var colors = ['rgb(218, 40, 40)',  'rgb(109, 193, 57)', 'rgb(29, 131, 204)', 'rgb(243, 156, 26)', 'rgb(187, 86, 184)'];
    	for (var i = 0; i < estados.length; i++) {
    		if ( resp[ estados[i] ] ) {
    			data.push({ 'label' : estados[i], 'value' : Math.round( resp[ estados[i] ] * 10) / 10});
    		}
    	}
    	if (data.length) {
    		_this.draw_donut_chart(data, 'recetas_medico', colors);
    	}else
    		$("#recetas_medico").html('<p style="text-align:center">No Hay recetas cargadas para el rango de fechas seleccionado</p>')
   
    })
}

Estadisticas.prototype.draw_donut_chart = function(data, element, colors = null){
	$('#'+element).html('');
	Morris.Donut({
	    element: element,
	    data: data,
	    colors: colors,
	    formatter: function (y) {
	        return y + '%'
	    }
	});
}

