var sprad = SuperAdmin = function (argument) {
	console.log('SUPERADMIN');

	this.general = new General();
	this.general.csrf = document.getElementsByName('_token')[0].value;

    this.bind_events();
	this.init();
}

SuperAdmin.prototype.bind_events = function(){
	var _this = this;

    $("#save_planes").click( function(){
        var planes = $(".plan");
        var info = {};
        
        for (var i = planes.length - 1; i >= 0; i--) {
        	plan = $(planes[i]);
        	var id = plan.attr('id');

        	var monto = plan.find('input[name=monto]');
        	var max_opticas = plan.find('input[name=max_opticas]');
        	var max_usuarios = plan.find('input[name=max_usuarios]');
        	
        	info[id] = {
	        	'max_opticas' : max_opticas.val(),
	        	'max_usuarios' : max_usuarios.val(),
	        	'monto' : monto.val(),
        	};
        }
        _this.general.send_ajax('PUT', location.href, info, _this.callback);

    });
}

SuperAdmin.prototype.callback = function(){
	location.reload();
}

SuperAdmin.prototype.init = function(){
    var _this = this;
    $.ajax({
        url: 'superadmin/getAdministradores',
        dataType: 'json',
    })
    .done(function(resp) {
        var boxes =  Object.keys(resp);
        for (var i = boxes.length - 1; i >= 0; i--) {
            $("#infobox_"+boxes[i]).countTo({ from: 0, to: resp[ boxes[i] ] });
        }
    })

    $.ajax({    
        url: 'superadmin/getAdministradoresByPago',
    }).done( function(resp){
        var estados = Object.keys(resp);
        var data = []; 
        var colors = { 'free' : '#2a8ad4', 'premium' :'#8cc549' };

        for (var i = 0; i < estados.length; i++) {
            if ( resp[ estados[i] ] ) {
                data.push({ 'label' : estados[i], 'value' : Math.round( resp[ estados[i] ] * 10) / 10, 'color' : colors[ estados[i] ] });
            }
        }
        if (data.length)
            _this.draw_donut_chart(data, 'recetas_estado');
        else
            $("#recetas_estado").html('<p style="text-align:center">No Hay recetas cargadas para el rango de fechas seleccionado</p>')

    });

}

SuperAdmin.prototype.draw_donut_chart = function(data, element, colors = null){
    $('#'+element).html('');
    Morris.Donut({
        element: element,
        data: data,
        colors: colors,
        formatter: function (y) {
            return y
        }
    });
}