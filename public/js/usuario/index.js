var us = Usuario = function(){
	console.log('USUARIO');

	$("#data-table-basic").DataTable({
		"lengthMenu": [
			[10, 25, 50, -1],
			[10, 25, 50, "All"]
		],
		dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
        ],
		language: {
			"lengthMenu": "Ver _MENU_ resultados",
			"zeroRecords": "Ningun resultado encontrado",
			"info": "Mostrando _PAGE_ de _PAGES_",
			"infoEmpty": "Ningun resultado",
			"search": "<b>BUSCAR:</b>",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Sig",
				"previous": "Ant"
			},
		}
	});

	this.general = new General();
	this.general.csrf = document.getElementsByName('_token')[0].value;

	this.bind_events();
	this.bind_table_events();
}

Usuario.prototype.bind_table_events = function(){
	var _this = this;
	$('.paginate_button').click( function(){
		_this.bind_events();
		_this.bind_table_events();
	});
	
	$("thead th").click(function(event) {
		_this.bind_events();
	});

	$("input[type=search]").keypress( function(){
		_this.bind_events();
	});
};

Usuario.prototype.bind_events = function(){
	var _this = this;
	var url_origin = '/cliente/';

	$("[name=new_receta_button]").off('click').on('click', function(){
		$("#create_receta").attr('usuario-id', $(this).attr('usuario-id') );
	});
	
	$('#modal_tipo_producto').off('change').on('change', function(){
		if ( $(this).val() == 'anteojo' ) {
			$("#tipo_lente_anteojo").show(200);
		}else{
			$("#tipo_lente_anteojo").hide(200);
		}
	});


	$("#modal_tipo_anteojo").off('change').change(function(event) {
		if ( $(this).val() == 'monofocal' ) {
			$("#cantidad_anteojo").show(200);
		}else{
			$("#cantidad_anteojo").hide(200);
		}
	});

	$("#create_receta").off('click').click( function(){
		$(this).attr('disabled', true);
		var info = {
			usuario_id: $(this).attr('usuario-id'),
		}

		if ( $('#modal_tipo_producto').val() == '' ) {
			_this.general.notification('Error', 'Complete todos los campos', 'error');
			$(this).removeAttr('disabled');
			return false;
		}

		if ( $('#modal_tipo_producto').val() == 'anteojo' ) {
			info['tipo_producto'] = 'anteojo';

			if ( $("#modal_tipo_anteojo").val() == '' ) {
				_this.general.notification('Error', 'Complete todos los campos', 'error');
			$(this).removeAttr('disabled');
				return false;
			}else{
				if ( $("#modal_tipo_anteojo").val() == "monofocal" && $("#modal_anteojo").val() == '' ) {
					_this.general.notification('Error', 'Complete todos los campos', 'error');
			$(this).removeAttr('disabled');
					return false;
				}
			}

			info['tipo_lente'] = $("#modal_tipo_anteojo").val();
			info['lejos_cerca'] = $("#modal_anteojo").val();
		}else{
			info['tipo_producto'] = 'lentes';
		}

		_this.general.send_ajax('POST', '/create_producto', info, _this.callback, true);
	});	

	var delay = (function(){
		var timer = 0;
		return function(callback, ms){
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};
	})();
}

Usuario.prototype.callback_reload_table = function(resp){
	$("#table-body").html('')
}

Usuario.prototype.callback = function(resp){
	if ( resp.error ) {
		toastr.options.onHidden = function() { 
			location.reload();
		}
		this.General.prototype.notification('Error', 'Hubo un error al crear la receta', 'error');
	}else{
		toastr.options.onHidden = function() { 
			location.href = '/cliente/'+resp.usuario_id+'/receta/'+resp.producto_id;
		}
		this.General.prototype.notification('OK', 'La receta se creo correctamente', 'success');
	}
}

var usE = UsuarioEdit = function(){
	console.log('USUARIO EDIT');

	this.general = new General();
	this.general.csrf = document.getElementsByName('_token')[0].value;
	this.bind_events();

	this.user_general = new UsuarioGeneral();
}

UsuarioEdit.prototype.callback_save_perfil = function(){
	$('#usuario-perfil').modal('hide');
	location.reload();
}

UsuarioEdit.prototype.bind_events = function(){
	var _this = this;

	document.getElementById('save_perfil').addEventListener('click', function(e){
		_this.user_general.save_usuario('POST', _this.callback_save_perfil);		
	});
	
	// document.getElementById('save_perfil').addEventListener('click', function(e){
	// 	$('.modal-body').waitMe({
	// 		effect : 'rotation',
	// 		text : 'Guardando',
	// 		bg : 'rgba(255,255,255,0.7)',
	// 		color : '#4cbf51',
	// 		maxSize : '',
	// 		waitTime : -1,
	// 		textPos : 'vertical',
	// 		fontSize : '',
	// 		source : '',
	// 	});
	// });

	$("#print").click(function(event) {
		$.ajax({
			url: location.href+='/download',
			type: 'GET',
			contentType: "application/pdf",
		})
		.done(function(resp) {
			console.log("success");
		})
	});
}

var usNew = UsuarioNew = function(){
	console.log('USUARIO NUEVO');

	this.general = new General();
	this.general.csrf = document.getElementsByName('_token')[0].value;

	this.user_general = new UsuarioGeneral();

	this.bind_events();
}

UsuarioNew.prototype.bind_events = function(){
	var _this = this;

	// SAVE USUARIO
	document.getElementById('save_perfil').addEventListener('click', function(){
		_this.user_general.save_usuario('POST', _this.callback_save_new_perfil );
	});
}

UsuarioNew.prototype.callback_save_new_perfil = function(){
	$('#usuario-perfil').modal('hide');
	location.reload();
}

var usGeneral = UsuarioGeneral = function(){
	this.general = new General();
	this.general.csrf = document.getElementsByName('_token')[0].value;

	this.bind_events(); 
}

UsuarioGeneral.prototype.save_usuario = function(type, callback){
	var _this = this;

	var id 			= document.getElementById('save_perfil').getAttribute('usuario');
	var nombre 		= document.getElementById('perfil_nombre').value;
	var apellido 	= document.getElementById('perfil_apellido').value;
	var dni 		= document.getElementById('perfil_dni').value;	
	var direccion 	= document.getElementById('perfil_direccion').value;	
	var telefono 	= document.getElementById('perfil_telefono').value;
	var obra 		= document.getElementById('perfil_obra').value;
	var num_obra 	= document.getElementById('perfil_num_obra').value;
	var mail 		= document.getElementById('perfil_mail').value;
	
	if ( id == 'new') {
		var info = { 
			'nombre'  			: nombre,
			'apellido' 			: apellido,
			'dni'				: dni,
			'direccion'			: direccion,
			'telefono'			: telefono,
			'obra_social_id'  	: obra,
			'num_obra_social'  	: num_obra,
			'mail'				: mail,
			'optica_id'			: window.optica_id
		};
	}else{
		var info = { 
			'usuario'			: id,
			'nombre'  			: nombre,
			'apellido' 			: apellido,
			'dni'				: dni,
			'direccion'			: direccion,
			'telefono'			: telefono,
			'obra_social_id'  	: obra,
			'num_obra_social'  	: num_obra,
			'mail'				: mail,
		};
	}


	$('#save_perfil').attr('disabled', true);
	if( _this.validarPerfil(info) )
		_this.general.send_ajax(type, '/cliente/'+id+'/edit', info, callback);		
	else{	
		$('#save_perfil').removeAttr('disabled');
		_this.general.notification('Error', 'Complete los campos faltantes', 'error');
	}
}

UsuarioGeneral.prototype.bind_events = function(){
	var _this 	= this;

	$('#perfil_obra').change(function(event) {
		if ( $(this).val() == '' ) 
			$('#perfil_num_obra').attr('disabled', 'true');
		else
			$('#perfil_num_obra').removeAttr('disabled');
		
	});

}

UsuarioGeneral.prototype.validarPerfil = function(perfil){
	valido = true;

	if( perfil.nombre == '' )
		valido = false;
	else if( perfil.apellido == '' )
		valido = false;
	else if( perfil.telefono == '' )
		valido = false;
	else if( perfil.direccion == '' )
		valido = false;	

	return valido;
}