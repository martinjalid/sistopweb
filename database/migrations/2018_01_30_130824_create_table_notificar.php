<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotificar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('notificar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('administrador_id');
            $table->integer('usuario_id');
            $table->integer('tipo_notificacion_id');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });

        Schema::create('tipo_notificacion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){   
        Schema::drop('notificar');
    }
}
