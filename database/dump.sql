-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: sistopweb
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrador`
--

DROP TABLE IF EXISTS `administrador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(45) NOT NULL,
  `password` varchar(70) NOT NULL,
  `created_at` varchar(45) NOT NULL,
  `updated_at` varchar(45) NOT NULL,
  `perfil_id` int(11) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrador`
--

LOCK TABLES `administrador` WRITE;
/*!40000 ALTER TABLE `administrador` DISABLE KEYS */;
INSERT INTO `administrador` VALUES (1,'martinjalid@gmail.com','$2y$10$Ani8sWeEvFH501bWDfOJ.OtErD/xo4f.N05T8VhT5G5VpP/q7l1d.','2018-01-03 17:31:06','2018-02-01 15:45:42',1,'vXle067zm2o98tEfbSXuw00XYWx75HKVlYVQOHkdetCrFWxaMXlklhNLPSZo',1),(2,'norjalid@gmail.com','$2y$10$Ionkkt6fKFg6Y2pkx42m1O8DRAAZjzMMZqwVqTzs6RMtQNN1pc3bK','2018-01-09 16:43:24','2018-01-09 16:43:24',1,'5SvRNTNlKO9bej3cRJI9LqUIgoW3vAvVtBQMP8RBrShdehm6pvOpQd9m9taQ',1),(8,'martinjalid@outlook.com','$2y$10$ilbCnCy3WaErDPiQMfaIF.M5LyirJouO6iU5NTwF5QWdNGVAKEPcC','2018-01-25 13:49:21','2018-01-30 13:32:35',2,'ul9VfsmW3NfuGt8qbtg3k0xXwYedObzmtbEkr2LsLmHkjap6OVcIJFs0mj1O',1);
/*!40000 ALTER TABLE `administrador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `administrador_optica`
--

DROP TABLE IF EXISTS `administrador_optica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrador_optica` (
  `optica_id` int(11) NOT NULL,
  `administrador_id` int(11) NOT NULL,
  PRIMARY KEY (`optica_id`,`administrador_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrador_optica`
--

LOCK TABLES `administrador_optica` WRITE;
/*!40000 ALTER TABLE `administrador_optica` DISABLE KEYS */;
INSERT INTO `administrador_optica` VALUES (3,1),(3,8),(4,2),(8,1);
/*!40000 ALTER TABLE `administrador_optica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anteojo`
--

DROP TABLE IF EXISTS `anteojo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anteojo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` enum('cerca','lejos') NOT NULL,
  `od_esferico` decimal(4,2) DEFAULT NULL,
  `od_cilindrico` decimal(4,2) DEFAULT NULL,
  `od_eje` int(11) DEFAULT NULL,
  `oi_esferico` decimal(4,2) DEFAULT NULL,
  `oi_cilindrico` decimal(4,2) DEFAULT NULL,
  `oi_eje` int(11) DEFAULT NULL,
  `armazon` varchar(45) DEFAULT NULL,
  `valor_lente` int(11) DEFAULT NULL,
  `valor_armazon` int(11) DEFAULT NULL,
  `tratamiento_color` varchar(45) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `tratamiento_id` int(11) DEFAULT NULL,
  `material_lente_id` int(11) DEFAULT NULL,
  `receta_id` int(11) NOT NULL,
  `origen_lente` enum('Laboratorio','Stock') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_anteojo_color1_idx` (`color_id`),
  KEY `fk_anteojo_tratamiento1_idx` (`tratamiento_id`),
  KEY `fk_anteojo_material_lente1_idx` (`material_lente_id`),
  KEY `fk_anteojo_receta1_idx` (`receta_id`),
  CONSTRAINT `fk_anteojo_color1` FOREIGN KEY (`color_id`) REFERENCES `color` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_anteojo_material_lente1` FOREIGN KEY (`material_lente_id`) REFERENCES `material_lente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_anteojo_receta1` FOREIGN KEY (`receta_id`) REFERENCES `receta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_anteojo_tratamiento1` FOREIGN KEY (`tratamiento_id`) REFERENCES `tratamiento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anteojo`
--

LOCK TABLES `anteojo` WRITE;
/*!40000 ALTER TABLE `anteojo` DISABLE KEYS */;
INSERT INTO `anteojo` VALUES (1,'cerca',2.00,3.00,1,-1.00,-3.00,-15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL),(2,'lejos',1.00,1.25,NULL,2.00,2.00,3,'APA',122,333,'AAA',1,1,1,1,NULL),(3,'cerca',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL),(4,'lejos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL),(5,'cerca',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,NULL),(6,'lejos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,NULL),(7,'cerca',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL),(8,'lejos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL),(9,'cerca',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,NULL),(10,'lejos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,NULL);
/*!40000 ALTER TABLE `anteojo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (1,'Blanco'),(2,'Fotocromatico');
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Pendiente'),(2,'Entregada');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lente_oftalmologo`
--

DROP TABLE IF EXISTS `lente_oftalmologo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lente_oftalmologo` (
  `receta_lente_id` int(11) NOT NULL AUTO_INCREMENT,
  `od_esferico` decimal(4,2) DEFAULT NULL,
  `od_cilindrico` decimal(4,2) DEFAULT NULL,
  `od_eje` decimal(4,2) DEFAULT NULL,
  `oi_esferico` decimal(4,2) DEFAULT NULL,
  `oi_cilindrico` decimal(4,2) DEFAULT NULL,
  `oi_eje` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`receta_lente_id`),
  KEY `fk_receta_lente2_idx` (`receta_lente_id`),
  CONSTRAINT `fk_receta_lente2` FOREIGN KEY (`receta_lente_id`) REFERENCES `receta_lente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lente_oftalmologo`
--

LOCK TABLES `lente_oftalmologo` WRITE;
/*!40000 ALTER TABLE `lente_oftalmologo` DISABLE KEYS */;
INSERT INTO `lente_oftalmologo` VALUES (1,1.00,2.00,3.00,1.00,2.00,3.00),(2,NULL,NULL,NULL,NULL,NULL,NULL),(3,NULL,NULL,NULL,NULL,NULL,NULL),(4,NULL,NULL,NULL,NULL,NULL,NULL),(5,NULL,NULL,NULL,NULL,NULL,NULL),(6,NULL,NULL,NULL,NULL,NULL,NULL),(7,NULL,NULL,NULL,NULL,NULL,NULL),(8,NULL,NULL,NULL,NULL,NULL,NULL),(9,NULL,NULL,NULL,NULL,NULL,NULL),(10,NULL,NULL,NULL,NULL,NULL,NULL),(11,NULL,NULL,NULL,NULL,NULL,NULL),(12,NULL,NULL,NULL,NULL,NULL,NULL),(13,NULL,NULL,NULL,NULL,NULL,NULL),(14,NULL,NULL,NULL,NULL,NULL,NULL),(15,NULL,NULL,NULL,NULL,NULL,NULL),(16,NULL,NULL,NULL,NULL,NULL,NULL),(17,NULL,NULL,NULL,NULL,NULL,NULL),(18,NULL,NULL,NULL,NULL,NULL,NULL),(19,NULL,NULL,NULL,NULL,NULL,NULL),(20,NULL,NULL,NULL,NULL,NULL,NULL),(21,NULL,NULL,NULL,NULL,NULL,NULL),(22,NULL,NULL,NULL,NULL,NULL,NULL),(23,NULL,NULL,NULL,NULL,NULL,NULL),(24,NULL,NULL,NULL,NULL,NULL,NULL),(25,NULL,NULL,NULL,NULL,NULL,NULL),(26,NULL,NULL,NULL,NULL,NULL,NULL),(27,NULL,NULL,NULL,NULL,NULL,NULL),(28,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `lente_oftalmologo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lente_prueba`
--

DROP TABLE IF EXISTS `lente_prueba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lente_prueba` (
  `receta_lente_id` int(11) NOT NULL AUTO_INCREMENT,
  `od_esferico` decimal(4,2) DEFAULT NULL,
  `od_cilindrico` decimal(4,2) DEFAULT NULL,
  `od_eje` decimal(4,2) DEFAULT NULL,
  `oi_esferico` decimal(4,2) DEFAULT NULL,
  `oi_cilindrico` decimal(4,2) DEFAULT NULL,
  `oi_eje` decimal(4,2) DEFAULT NULL,
  `od_agudeza` int(11) DEFAULT NULL,
  `oi_agudeza` int(11) DEFAULT NULL,
  `ao_agudeza` int(11) DEFAULT NULL,
  PRIMARY KEY (`receta_lente_id`),
  KEY `fk_receta_lente1_idx` (`receta_lente_id`),
  CONSTRAINT `fk_receta_lente1` FOREIGN KEY (`receta_lente_id`) REFERENCES `receta_lente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lente_prueba`
--

LOCK TABLES `lente_prueba` WRITE;
/*!40000 ALTER TABLE `lente_prueba` DISABLE KEYS */;
INSERT INTO `lente_prueba` VALUES (1,3.00,2.00,1.00,NULL,34.00,4.00,4,6,8),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `lente_prueba` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material_lente`
--

DROP TABLE IF EXISTS `material_lente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material_lente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material_lente`
--

LOCK TABLES `material_lente` WRITE;
/*!40000 ALTER TABLE `material_lente` DISABLE KEYS */;
INSERT INTO `material_lente` VALUES (1,'Organico'),(2,'Mineral');
/*!40000 ALTER TABLE `material_lente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2018_01_25_182543_create_table_planes',1),(2,'2018_01_30_130824_create_table_notificar',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notificar`
--

DROP TABLE IF EXISTS `notificar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `administrador_id` int(11) NOT NULL,
  `tipo_notificacion_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notificar`
--

LOCK TABLES `notificar` WRITE;
/*!40000 ALTER TABLE `notificar` DISABLE KEYS */;
INSERT INTO `notificar` VALUES (1,8,2,'2018-01-30 13:32:35','2018-01-30 13:32:35',NULL),(2,1,3,'2018-01-30 13:41:15','2018-01-30 13:41:15',NULL),(3,1,3,'2018-01-30 13:43:06','2018-01-30 13:43:06',NULL),(4,1,3,'2018-01-30 13:43:27','2018-01-30 13:43:27',2),(5,1,2,'2018-02-01 14:18:36','2018-02-01 14:18:36',NULL),(6,1,2,'2018-02-01 15:47:21','2018-02-01 15:47:21',NULL);
/*!40000 ALTER TABLE `notificar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obra_social`
--

DROP TABLE IF EXISTS `obra_social`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obra_social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obra_social`
--

LOCK TABLES `obra_social` WRITE;
/*!40000 ALTER TABLE `obra_social` DISABLE KEYS */;
INSERT INTO `obra_social` VALUES (2,'OSDE'),(3,'GALENO'),(4,'OSDE'),(5,'OMINT'),(6,'Swiss Medical'),(7,'Galeno'),(8,'Medicus'),(9,'Medife'),(10,'Union Personal'),(11,'Accord Salud'),(12,'Bristol Medicine'),(13,'OSECAC'),(14,'ACA Salud'),(15,'AMFFA'),(16,'COMI'),(17,'Construir Salud'),(18,'FEMEBA'),(19,'DOSUBA'),(20,'OSDEPyM'),(21,'IOMA'),(22,'IOSE'),(23,'Forjar Salud'),(24,'Sancor Salud'),(25,'MOA'),(26,'OSALARA'),(27,'OSDEM'),(28,'OSMEBA'),(29,'OSMECON'),(30,'OSMEDICA'),(31,'OSDIPP'),(32,'OSJOMN'),(33,'OSFOT'),(34,'OSPAT'),(35,'OSPE'),(36,'OSPEPBA'),(37,'OSPETAX'),(38,'OSPERYHRA'),(39,'OSPIA'),(40,'OSPPEA'),(41,'OSPP'),(42,'OSSdeB'),(43,'OTRA');
/*!40000 ALTER TABLE `obra_social` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `optica`
--

DROP TABLE IF EXISTS `optica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `created_at` varchar(45) NOT NULL,
  `updated_at` varchar(45) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `optica`
--

LOCK TABLES `optica` WRITE;
/*!40000 ALTER TABLE `optica` DISABLE KEYS */;
INSERT INTO `optica` VALUES (3,'Sistopweb','2017-11-26 15:05:25','2018-01-25 17:24:30',1),(4,'Optica Villa Luzuriaga','2018-01-09 16:45:04','2018-01-09 16:45:04',1),(8,'SistopWeb2','2018-01-19 12:32:00','2018-01-19 12:32:00',1);
/*!40000 ALTER TABLE `optica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil`
--

LOCK TABLES `perfil` WRITE;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` VALUES (1,'Administrador'),(2,'Usuario');
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plan`
--

DROP TABLE IF EXISTS `plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_opticas` int(11) NOT NULL,
  `max_usuarios` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plan`
--

LOCK TABLES `plan` WRITE;
/*!40000 ALTER TABLE `plan` DISABLE KEYS */;
INSERT INTO `plan` VALUES (1,'Basico',1,3),(2,'Intermedio',3,5),(3,'Avanzado',10,10);
/*!40000 ALTER TABLE `plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `profesional_id` int(6) DEFAULT NULL,
  `usuario_id` int(6) NOT NULL,
  `tipo_producto_id` int(6) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '0',
  `observacion` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `optica_id` int(11) NOT NULL,
  `estado_id` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,NULL,2,1,1,'PRUEBAAAAA','2017-12-12 00:00:00','2018-01-24 16:21:14',3,2),(2,2,6,2,1,'PAAAAAA','2018-01-19 15:03:22','2018-01-24 16:22:38',3,2),(3,NULL,8,1,1,NULL,'2018-01-19 15:07:26','2018-01-19 15:07:26',3,1),(4,NULL,9,2,0,NULL,'2018-01-19 18:01:45','2018-01-25 14:40:04',8,1),(5,NULL,9,2,1,NULL,'2018-01-19 18:04:12','2018-01-19 18:04:12',8,1),(6,NULL,10,2,1,NULL,'2018-01-19 20:33:42','2018-01-19 20:33:42',4,1),(7,NULL,11,1,1,NULL,'2018-01-19 20:37:22','2018-01-19 20:37:31',4,2),(8,NULL,10,1,1,NULL,'2018-01-13 18:27:37','2018-01-18 17:40:17',4,2),(9,NULL,10,1,1,NULL,'2018-01-15 21:09:48','2018-01-18 17:44:04',4,2),(10,NULL,10,1,1,NULL,'2018-01-15 22:41:27','2018-01-15 22:41:27',5,1),(11,NULL,10,2,1,NULL,'2018-01-16 14:31:59','2018-01-22 23:43:07',4,2),(12,NULL,10,1,1,NULL,'2018-01-16 14:34:51','2018-01-16 14:34:51',4,1),(13,NULL,10,1,1,NULL,'2018-01-16 20:45:51','2018-01-16 20:45:51',3,1),(14,NULL,10,1,1,NULL,'2018-01-18 15:49:04','2018-01-22 23:38:59',4,1),(15,NULL,10,1,1,NULL,'2018-01-18 15:49:12','2018-01-22 23:40:51',4,1),(16,NULL,10,1,1,NULL,'2018-01-18 15:49:35','2018-01-22 23:42:06',4,2),(17,NULL,10,1,1,NULL,'2018-01-18 15:49:52','2018-01-22 23:42:29',4,1),(18,NULL,10,1,0,NULL,'2018-01-18 15:50:47','2018-01-18 16:44:10',4,2),(19,NULL,10,1,1,NULL,'2018-01-18 15:51:12','2018-01-18 15:51:12',4,1),(20,NULL,10,1,1,NULL,'2018-01-18 16:38:56','2018-01-18 16:44:37',4,2),(21,NULL,10,1,1,NULL,'2018-01-18 17:29:17','2018-01-18 17:31:18',4,2),(22,NULL,10,1,1,NULL,'2018-01-18 17:37:17','2018-01-18 17:37:17',4,1),(23,NULL,10,1,1,NULL,'2018-01-18 22:14:56','2018-01-18 22:14:56',5,1),(24,NULL,10,1,1,NULL,'2018-01-19 21:42:45','2018-01-19 21:42:45',4,1),(25,NULL,10,1,1,NULL,'2018-01-20 13:59:15','2018-01-20 14:00:48',4,2),(26,NULL,10,1,1,NULL,'2018-01-20 15:15:58','2018-01-20 15:21:08',4,2),(27,NULL,10,1,1,NULL,'2018-01-20 15:19:30','2018-01-20 15:21:00',4,2),(28,NULL,10,1,1,'laboratorio  Flash','2018-01-22 22:09:02','2018-01-22 22:13:36',5,2),(29,NULL,7,2,1,NULL,'2018-01-23 19:15:33','2018-01-23 19:15:33',3,1),(30,2,9,2,1,NULL,'2018-01-23 19:16:01','2018-01-23 19:16:08',8,1),(31,2,12,1,1,NULL,'2018-01-23 19:16:35','2018-01-23 19:16:35',8,1),(32,NULL,67,2,1,NULL,'2018-01-25 15:08:27','2018-01-25 15:08:27',4,1),(33,2,75,2,1,NULL,'2018-01-25 15:29:22','2018-01-26 14:02:03',3,2),(34,NULL,75,2,1,NULL,'2018-01-25 16:33:26','2018-01-26 14:02:06',3,2),(35,NULL,75,2,1,NULL,'2018-01-25 16:33:26','2018-01-26 14:02:09',3,2),(36,2,75,2,1,NULL,'2018-01-25 16:33:26','2018-01-26 14:01:02',3,1),(37,2,75,2,1,NULL,'2018-01-25 16:33:27','2018-01-26 14:01:08',3,1),(38,NULL,75,2,1,NULL,'2018-01-25 16:34:00','2018-01-26 14:02:18',3,2),(39,2,75,2,1,NULL,'2018-01-25 16:34:00','2018-01-26 14:02:15',3,2),(40,NULL,75,2,1,NULL,'2018-01-25 16:34:01','2018-01-26 14:02:43',3,2),(41,NULL,75,2,1,NULL,'2018-01-25 16:34:01','2018-01-26 14:02:22',3,2),(42,2,75,2,1,NULL,'2018-01-25 16:34:01','2018-01-25 17:16:44',3,2),(43,NULL,75,2,1,NULL,'2018-01-25 16:34:02','2018-01-25 16:34:02',3,1),(44,NULL,75,2,1,NULL,'2018-01-25 16:34:02','2018-01-25 16:34:02',3,1),(45,NULL,75,2,1,NULL,'2018-01-25 16:34:02','2018-01-25 16:34:02',3,1),(46,2,75,2,1,NULL,'2018-01-25 16:34:03','2018-01-25 16:34:03',3,1),(47,NULL,75,2,1,NULL,'2018-01-25 16:34:03','2018-01-25 16:34:03',3,1),(48,NULL,75,2,1,NULL,'2018-01-25 16:34:03','2018-01-25 16:34:03',3,1),(49,NULL,75,2,1,NULL,'2018-01-25 16:34:03','2018-01-25 16:34:03',3,1),(50,2,75,2,1,NULL,'2018-01-25 16:34:28','2018-01-25 16:34:28',3,1),(51,NULL,75,2,1,NULL,'2018-01-25 16:34:29','2018-01-25 16:34:29',3,1),(52,NULL,75,2,1,NULL,'2018-01-25 16:35:52','2018-01-25 16:35:52',3,1),(53,NULL,75,1,1,NULL,'2018-02-01 15:34:03','2018-02-01 15:34:03',3,1),(54,NULL,75,2,1,NULL,'2018-02-01 15:34:48','2018-02-01 15:34:48',3,1);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profesional`
--

DROP TABLE IF EXISTS `profesional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profesional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `optica_id` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_profesional_administrador1_idx` (`optica_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profesional`
--

LOCK TABLES `profesional` WRITE;
/*!40000 ALTER TABLE `profesional` DISABLE KEYS */;
INSERT INTO `profesional` VALUES (2,'Pedro Perez',3,1),(4,'Juan Perez',8,1),(5,'Cornetin',4,1);
/*!40000 ALTER TABLE `profesional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receta`
--

DROP TABLE IF EXISTS `receta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distancia` int(11) DEFAULT NULL,
  `altura` int(11) DEFAULT NULL,
  `adicion` decimal(4,2) DEFAULT NULL,
  `detalle_lente` varchar(45) DEFAULT NULL,
  `tipo_lente_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_receta_tipo_lente1_idx` (`tipo_lente_id`),
  CONSTRAINT `fk_receta_tipo_lente1` FOREIGN KEY (`tipo_lente_id`) REFERENCES `tipo_lente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receta`
--

LOCK TABLES `receta` WRITE;
/*!40000 ALTER TABLE `receta` DISABLE KEYS */;
INSERT INTO `receta` VALUES (1,3,34,5.00,'Prueba',2,1),(2,NULL,NULL,NULL,NULL,3,3),(3,NULL,NULL,NULL,NULL,2,7),(4,NULL,NULL,NULL,NULL,3,31),(5,NULL,NULL,NULL,NULL,2,53);
/*!40000 ALTER TABLE `receta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receta_lente`
--

DROP TABLE IF EXISTS `receta_lente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receta_lente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `od_esferico` decimal(4,2) DEFAULT NULL,
  `od_cilindrico` decimal(4,2) DEFAULT NULL,
  `od_eje` decimal(4,2) DEFAULT NULL,
  `oi_esferico` decimal(4,2) DEFAULT NULL,
  `oi_cilindrico` decimal(4,2) DEFAULT NULL,
  `oi_eje` decimal(4,2) DEFAULT NULL,
  `queratometria_od` decimal(4,2) DEFAULT NULL,
  `queratometria_oi` decimal(4,2) DEFAULT NULL,
  `tipo_lente` varchar(40) DEFAULT NULL,
  `diametro` varchar(40) DEFAULT NULL,
  `radio` varchar(40) DEFAULT NULL,
  `color` varchar(40) DEFAULT NULL,
  `producto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receta_lente`
--

LOCK TABLES `receta_lente` WRITE;
/*!40000 ALTER TABLE `receta_lente` DISABLE KEYS */;
INSERT INTO `receta_lente` VALUES (1,4.00,56.00,NULL,7.00,NULL,7.00,5.00,7.00,'Paaaa','33','2','11',2),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4),(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6),(5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,29),(6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30),(7,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,32),(8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,33),(9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,34),(10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,35),(11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36),(12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,37),(13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,38),(14,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,39),(15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40),(16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,41),(17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,42),(18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,43),(19,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,44),(20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,45),(21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,46),(22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,47),(23,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,48),(24,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,49),(25,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50),(26,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,51),(27,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,52),(28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,54);
/*!40000 ALTER TABLE `receta_lente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_lente`
--

DROP TABLE IF EXISTS `tipo_lente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_lente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_lente`
--

LOCK TABLES `tipo_lente` WRITE;
/*!40000 ALTER TABLE `tipo_lente` DISABLE KEYS */;
INSERT INTO `tipo_lente` VALUES (1,'Monofocal'),(2,'Bifocal'),(3,'Multifocal');
/*!40000 ALTER TABLE `tipo_lente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_notificacion`
--

DROP TABLE IF EXISTS `tipo_notificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_notificacion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_notificacion`
--

LOCK TABLES `tipo_notificacion` WRITE;
/*!40000 ALTER TABLE `tipo_notificacion` DISABLE KEYS */;
INSERT INTO `tipo_notificacion` VALUES (1,'Registro'),(2,'Reset Pass'),(3,'Notificacion Cliente'),(4,'New Usuario');
/*!40000 ALTER TABLE `tipo_notificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_producto`
--

DROP TABLE IF EXISTS `tipo_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_producto` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_producto`
--

LOCK TABLES `tipo_producto` WRITE;
/*!40000 ALTER TABLE `tipo_producto` DISABLE KEYS */;
INSERT INTO `tipo_producto` VALUES (1,'Anteojo'),(2,'Lente de Contacto');
/*!40000 ALTER TABLE `tipo_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tratamiento`
--

DROP TABLE IF EXISTS `tratamiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tratamiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tratamiento`
--

LOCK TABLES `tratamiento` WRITE;
/*!40000 ALTER TABLE `tratamiento` DISABLE KEYS */;
INSERT INTO `tratamiento` VALUES (1,'Antirreflejo'),(2,'Tenido'),(3,'Blue Light');
/*!40000 ALTER TABLE `tratamiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `dni` int(11) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `num_obra_social` varchar(45) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `obra_social_id` int(11) DEFAULT NULL,
  `optica_id` int(11) NOT NULL,
  `mail` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_obra_social_idx` (`obra_social_id`),
  KEY `fk_usuario_optica1_idx` (`optica_id`),
  CONSTRAINT `fk_usuario_optica1` FOREIGN KEY (`optica_id`) REFERENCES `optica` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (2,'Martin','Jalid',37349950,'1133241876','Jujuy 3464','1112223334','2017-05-24 00:59:57','2018-01-24 17:53:56',2,3,'martinjalid@gmail.com'),(3,'Agustin','Gomez',39220022,'1131456666','Amenabar 2980','1112223334','2017-06-08 02:05:07','2018-01-18 13:57:48',2,3,'jefe@gmail.com'),(6,'Agustin','Gomez',37349950,'1133241876','Jujuy 34644','1112223334','2018-01-16 18:41:33','2018-01-16 18:41:33',2,3,NULL),(7,'Agustina','Gomez',37349950,'1133241876','Jujuy 34644','1112223334','2018-01-16 18:41:43','2018-01-16 18:41:43',2,3,NULL),(8,'Fernando','Jalid',36280826,'1122344333','Av de Mayo 100',NULL,'2018-01-19 14:39:09','2018-01-19 14:39:09',NULL,3,'ferjalid@gmail.com'),(9,'Martin','Jaild',NULL,'3333','Corneta 123',NULL,'2018-01-19 18:01:37','2018-01-19 18:01:37',NULL,8,NULL),(10,'Martin','Jalid',NULL,'22355533','jujuy2222',NULL,'2018-01-19 20:33:36','2018-01-19 20:33:36',NULL,4,NULL),(11,'Prueba','Corneta',NULL,'1111','GAA244',NULL,'2018-01-19 20:37:14','2018-01-19 20:37:14',NULL,4,NULL),(12,'Pedro','123',NULL,'1111','AAA344',NULL,'2018-01-23 19:16:27','2018-01-23 19:16:27',NULL,8,NULL),(55,'Carlos A','Gandulfo',21500000,'20565868','Salvini y Ortega - Laferrere','83885/00','2018-01-10 14:17:42','2018-01-10 14:17:42',2,4,NULL),(56,'Horacio','Riedes',2122222,'46503642','Garibaldi 2847  -  Villa Luzuriaga','079635/3','2018-01-10 14:22:18','2018-01-10 14:22:18',2,4,NULL),(57,'Mirta','Saavedra',NULL,'1139191671','Usuahia 341  -  Isidro Casanova','992722222/00','2018-01-12 19:37:23','2018-01-12 19:37:23',19,4,NULL),(58,'Graciela','Tagliante',NULL,'46503088','Ombú 4448 Dto. 9  -  Villa Luzuriaga','243765','2018-01-15 21:09:15','2018-01-15 21:09:15',25,4,NULL),(59,'Veronica','Galindre',NULL,'1530594365','Florencio Varela 765 - Moron','225000','2018-01-15 22:40:41','2018-01-15 22:40:41',2,4,NULL),(60,'Norberto','Jalid',14211135,'4443-7356','Bermudez 2882 - Villa Luzuriaga','2135','2018-01-16 14:34:13','2018-01-24 22:49:35',11,4,NULL),(61,'Carlos','Villarroel',NULL,'15 57489431','Del Carmen 2385 - Rafael Castillo','1288077720/00','2018-01-18 15:48:28','2018-01-18 15:48:28',19,4,NULL),(62,'Ludmila','Sayago',NULL,'4650 3179','Miguel Cane 2164  -  Villa Luzuriaga','486883098010/3','2018-01-18 17:28:25','2018-01-18 17:28:25',4,4,NULL),(63,'Paula','Guedes',NULL,'4699-0401','Colon 2885 - Lomas del Mirador','2326003297*00','2018-01-18 17:36:40','2018-01-18 17:36:40',19,4,NULL),(64,'Silvia','Trevisan',NULL,'1541718647','French 260  (chispita) - Ramos mejia','2121','2018-01-18 22:13:48','2018-01-18 22:13:48',2,4,NULL),(65,'Raul','Godoy',NULL,'4603-8241','Drago 229 - Rafael Castillo','1206189515/00','2018-01-19 21:42:06','2018-01-19 21:43:23',19,4,NULL),(66,'Adriana','Gonzalez',NULL,'4603-8241','Drago 220 - Rafael castillo','12061895150/01','2018-01-20 13:58:05','2018-01-20 13:58:05',19,4,NULL),(67,'Adriana','Carnero',23376835,'11 5807 5744','El resero 4320  -  Laferrere','2233768553','2018-01-20 15:15:26','2018-01-20 15:15:26',19,4,NULL),(68,'Juan','Cantero',NULL,'Marido Andrea','San justo',NULL,'2018-01-20 15:18:32','2018-01-20 15:18:32',NULL,4,NULL),(69,'María del Carmen','Viturro',NULL,'4656-7205','Alem 647 Ramos Mejía','sin','2018-01-22 22:07:14','2018-01-22 22:07:14',NULL,4,NULL),(70,'Juan Jose','Abella',NULL,'58301197','no pedida','9625561702','2018-01-25 13:51:21','2018-01-25 13:51:21',19,4,NULL),(71,'Ruben','Santillan',NULL,'3966-5020','Amambay 445  -  Rafael Castillo','PBSBA 374852/00','2018-01-25 14:04:06','2018-01-25 14:04:06',41,4,NULL),(72,'Karen','Zapala',NULL,'1544347333','San Matias 499  -  Rafael Castillo','123546987','2018-01-25 14:07:17','2018-01-25 14:07:17',19,4,NULL),(73,'Mauro','Santillan',NULL,'1157292673','Fernandez  630 - G. Catan','288244309/00','2018-01-25 14:19:11','2018-01-25 14:19:11',19,4,NULL),(74,'Thiago','Uyuquipa',NULL,'4457-0509','Sequeira 2982 - Laferrere','1188618993/02','2018-01-25 14:22:25','2018-01-25 14:22:25',19,4,NULL),(75,'Prueba','AAAAA',NULL,'33333','aa33',NULL,'2018-01-25 15:29:14','2018-01-25 15:29:14',NULL,3,NULL),(76,'2','3',4,'5','222',NULL,'2018-01-25 16:26:48','2018-01-25 16:26:48',NULL,3,NULL),(77,'2','3',4,'5','222',NULL,'2018-01-25 16:26:50','2018-01-25 16:26:50',NULL,3,NULL),(78,'11','11',NULL,'111','1111',NULL,'2018-01-25 16:27:07','2018-01-25 16:27:07',NULL,3,NULL),(79,'11','11',NULL,'111','1111',NULL,'2018-01-25 16:27:08','2018-01-25 16:27:08',NULL,3,NULL),(80,'11','11',NULL,'111','1111',NULL,'2018-01-25 16:27:13','2018-01-25 16:27:13',NULL,3,NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 10:56:46
