<?php

use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()    {
        $planes = [
    		['nombre' => 'Basico', 'max_opticas' =>  1, 'max_usuarios' => 3],
            ['nombre' => 'Intermedio', 'max_opticas' =>  3, 'max_usuarios' => 5],
    		['nombre' => 'Avanzado', 'max_opticas' => 10 , 'max_usuarios' => 10],
    	];

    	foreach ($planes as $plan) {
			DB::table('plan')->insert($plan);
    	}
    }
}
