<?php

use Illuminate\Database\Seeder;

class EstadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    	$estados = [
    		['nombre' => 'Pendiente' ],
    		['nombre' => 'Entregada' ]
    	];

    	foreach ($estados as $estado) {
			DB::table('estado')->insert($estado);
    	}
    }
}
