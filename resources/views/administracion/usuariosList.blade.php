@extends('layout.layout')

@section('content')
    <div class="crsf">
        {{ csrf_field() }}
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-3">
                            <h4>
                                USUARIOS DEL SISTEMA
                            </h4>
                        </div>
                        <div class="col-sm-3 pull-right">
                            <button type="button" id="new_usuario" href="#modal_usuario" data-toggle="modal" class="btn btn-lg btn-info waves-effect pull-right">
                                <i class="material-icons">person_add</i>
                                <span>NUEVO USUARIO</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive" style="overflow-x: hidden !important;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Mail</th>
                                    <th>Optica</th>
                                    <th>Activo</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if( $usuarios->count() == 0 )
                                    <tr>
                                        <td> No Hay Usuarios Registrados</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                                @foreach( $usuarios as $usuario )
                                    <tr usuario-id="{{ $usuario->id }}">
                                        <td> {{ $usuario->mail }} </td>
                                        <td> {{ $usuario->opticas()->first()->nombre }}</td>
                                        <td> {{ $usuario->activo ? 'Activo' : 'No Activo' }}</td>
                                        <td style="width: 10%"> 
                                            <a class="material-icons edit pointer" href="#modal_usuario" data-toggle="modal" usuario-id="{{ $usuario->id }}" optica-id="{{ $usuario->opticas()->first()->id }}" mail="{{ $usuario->mail }}" title="Editar Profesional">edit_mode</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="modal_usuario" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">             
                <div class="modal-header" style="height: 70px">
                    <h4 class="modal-title"></h4>  
                    <hr>
                </div>          
                <div class="modal-body">
                    <br>
                    <div class="row">
                        <div class="col-md-offset-2 col-md-4">
                            <div class="form-group">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input class="form-control" placeholder="Mail" id="modal_mail" >
                                    </div>                                  
                                </div>                                  
                            </div>
                        </div>
                        <div class="col-md-4" id="contraseña">
                            <div class="form-group">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input class="form-control" placeholder="Contraseña" id="modal_password" >
                                    </div>                                  
                                </div>                                  
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @if( $admin->opticas()->count() > 1 )
                            <div class="col-md-offset-2 col-md-4">
                                <div class="form-group">
                                    <div class="form-group">
                                        <select class="show-tick" id="modal_optica">
                                            <option value="0">Óptica</option>
                                            @foreach( $admin->opticas() as $optica)
                                                <option value="{{ $optica->id }}">{{ $optica->nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>                                  
                                </div>
                            </div>
                        @endif
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="form-group">
                                    <select id="modal_activo">
                                        <option value="1">Activo</option>
                                        <option value="0">Desactivo</option>
                                    </select>
                                </div>                                  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <hr>
                    <button id="save_usuario" type="button" class="btn btn-success waves-effect pull-right">Guardar</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script type="text/javascript" src="/js/general/index.js"></script>
    <script type="text/javascript" src="/js/administracion/index.js"></script>
    <script type="text/javascript"> // INIT
        $(document).ready( function() {
            var usu = new Usuario();
        });
    </script>
@endsection