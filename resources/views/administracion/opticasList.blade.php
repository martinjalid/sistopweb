@extends('layout.layout')

@section('content')
    <div class="crsf">
        {{ csrf_field() }}
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-3">
                            <h4>
                                OPTICAS
                            </h4>
                        </div>
                        <!-- <div class="col-sm-3 pull-right">
                            <button type="button" id="new_optica" href="#modal_optica" data-toggle="modal" class="btn btn-lg btn-info waves-effect pull-right">
                                <i class="material-icons">person_add</i>
                                <span>NUEVA OPTICA</span>
                            </button>
                        </div> -->
                    </div>
                </div>
                <div class="body">
                    <div class="table-responsive" style="overflow-x: hidden !important;">
                        <table class="table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Activo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if( $admin->opticas()->count() == 0 )
                                <tr>
                                    <td> No Hay Opticas Registradas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endif
                            @foreach( $admin->opticas() as $optica )
                                <tr optica-id="{{ $optica->id }}">
                                    <td> {{ $optica->nombre }} </td>
                                    <td> {{ $optica->activo ? 'Activo' : 'No Activo' }}</td>
                                    <td style="width: 10%"> 
                                        <a class="material-icons pointer" name="edit_optica" href="#modal_optica" data-toggle="modal" optica-id="{{ $optica->id }}" title="Editar Médico">edit_mode</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="modal_optica" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">             
                <div class="modal-header" style="height: 70px">
                    <h4 class="modal-title"></h4>  
                    <hr>
                </div>          
                <div class="modal-body">
                    <br>
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-5">
                            <div class="form-group">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input class="form-control" placeholder="Nombre" id="modal_nombre" >
                                    </div>                                  
                                </div>                                  
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <div class="form-group">
                                    <select id="modal_activo">
                                        <option value="1">Activo</option>
                                        <option value="0">Desactivo</option>
                                    </select>
                                </div>                                  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <hr>
                    <button id="save_optica" type="button" class="btn btn-success waves-effect pull-right">Guardar</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script type="text/javascript" src="/js/general/index.js"></script>
    <script type="text/javascript" src="/js/administracion/index.js"></script>
    <script type="text/javascript"> // INIT
        $(document).ready( function() {
            var opt = new Optica();
        });
    </script>
@endsection