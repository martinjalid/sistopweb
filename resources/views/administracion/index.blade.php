@extends('layout.layout')

@section('content')
<div class="crsf">
    {{ csrf_field() }}
</div>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-sm-5">
                        <h2>ADMINISTRACIÓN</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                <div class="row clearfix">
                    @if( $admin->perfil() == 'Administrador' )
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-2 bg-blue hover-expand-effect" onclick="location.href += '/medicos'">
                                <div class="icon">
                                    <i class="material-icons">face</i>
                                </div>
                                <div class="content">
                                    <div class="text">PROFESIONALES</div>
                                    <h6>Editar o añadir profesionales.</h6>
                                </div>
                            </div>
                        </div>
                        <!--
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-2 bg-green hover-expand-effect" onclick="location.href += '/usuarios'" >
                                <div class="icon">
                                    <i class="material-icons">accessibility</i>
                                </div>
                                <div class="content">
                                    <div class="text">USUARIOS</div>
                                    <h6>Editar los usuarios del sistema.</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-2 bg-deep-purple hover-expand-effect" onclick="location.href += '/opticas'" >
                                <div class="icon">
                                    <i class="material-icons">tune</i>
                                </div>
                                <div class="content">
                                    <div class="text">Opticas</div>
                                    <h6>Editar o añadir opticas.</h6>
                                </div>
                            </div>
                        </div>
                        -->
                    @endif
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box-2 bg-teal hover-expand-effect" data-toggle="modal" href="#password">
                            <div class="icon">
                                <i class="material-icons">vpn_key</i>
                            </div>
                            <div class="content">
                                <div class="text">Contraseña</div>
                                <h6>Cambiar la contraseña de tu cuenta</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </div>
@endsection('content')


@section('modals')
    @include('modal.changePassword')
@endsection
@section('js')
    <script type="text/javascript" src="/plugins/flot-charts/jquery.flot.js"></script>
    <script type="text/javascript" src="/plugins/flot-charts/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="/plugins/flot-charts/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="/plugins/flot-charts/jquery.flot.categories.js"></script>
    <script type="text/javascript" src="/plugins/flot-charts/jquery.flot.time.js"></script>

    <script type="text/javascript" src="/js/general/index.js"></script>
    <script type="text/javascript" src="/js/administracion/index.js"></script>
    <script type="text/javascript"> // INIT
        $(document).ready(function() {
            var admi = new Administracion();
        });
    </script>
@endsection