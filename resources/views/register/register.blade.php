@extends('layout.login')

@section('content')
    <div class="crsf">
        {{ csrf_field() }}
    </div>
    <div class="col-lg-3 col-md-4 col-xs-12">
        <div class="card">
            <h4 class="l-login">Registrate</h4>
            <form class="col-md-12" id="sign_in">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                        <label class="form-label">Email</label>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <input id="optica" class="form-control" name="optica" required>
                        <label class="form-label">Nombre de tu Óptica</label>
                    </div>
                </div>
                
                <a href="#terminos" data-toggle="modal" style="color: #457fca">Terminos y condiciones</a>
                <input type="checkbox" id="terminos_check" class="chk-col-blue">
                <label for="terminos_check" style="font-size: 12px; color: black">Acepto los Terminos y Condiciones</label>
                
                <a class="btn btn-raised waves-effect bg-green" id="register">REGISTRAR</a> 
                <div class="text-left"> 
                    <a href="/login" style="color: #78b83e">Ya estás registrado? Inicia Sesión</a> 
                </div>
            </form>
        </div>
    </div>
@endsection

@section('modals')
    @include('modal.terminos')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/login/index.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/toastr.js') }}"></script>
    <script type="text/javascript"> // INIT
        document.addEventListener("DOMContentLoaded", function(event) { 
            var lg = new Login();
        });        
    </script>
@endsection