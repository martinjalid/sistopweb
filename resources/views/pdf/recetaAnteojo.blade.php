<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Receta</title>
        
        <style type="text/css">
            p{
                margin:10px 0;
                padding:0;
            }
            table{
                border-collapse:collapse;
            }
            h1,h2,h3,h4,h5,h6{
                display:block;
                margin:0;
                padding:0;
            }
            img,a img{
                border:0;
                height:auto;
                outline:none;
                text-decoration:none;
            }
            body,#bodyTable,#bodyCell{
                height:100%;
                margin:0;
                padding:0;
                width:100%;
            }
            .mcnPreviewText{
                display:none !important;
            }
            #outlook a{
                padding:0;
            }
            img{
                -ms-interpolation-mode:bicubic;
            }
            table{
                mso-table-lspace:0pt;
                mso-table-rspace:0pt;
            }
            .ReadMsgBody{
                width:100%;
            }
            .ExternalClass{
                width:100%;
            }
            p,a,li,td,blockquote{
                mso-line-height-rule:exactly;
            }
            a[href^=tel],a[href^=sms]{
                color:inherit;
                cursor:default;
                text-decoration:none;
            }
            p,a,li,td,body,table,blockquote{
                -ms-text-size-adjust:100%;
                -webkit-text-size-adjust:100%;
            }
            .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
                line-height:100%;
            }
            a[x-apple-data-detectors]{
                color:inherit !important;
                text-decoration:none !important;
                font-size:inherit !important;
                font-family:inherit !important;
                font-weight:inherit !important;
                line-height:inherit !important;
            }
            #bodyCell{
                padding:10px;
            }
            .templateContainer{
                max-width:600px !important;
            }
            a.mcnButton{
                display:block;
            }
            .mcnImage,.mcnRetinaImage{
                vertical-align:bottom;
            }
            .mcnTextContent{
                word-break:break-word;
            }
            .mcnTextContent img{
                height:auto !important;
            }
            .mcnDividerBlock{
                table-layout:fixed !important;
            }
            body,#bodyTable{
                background-color:#FFFFFF;
                background-image:none;
                background-repeat:no-repeat;
                background-position:center;
                background-size:cover;
            }
            #bodyCell{
                border-top:0;
            }
            .templateContainer{
                border:0;
            }
            h1{
                color:#202020;
                font-family:Helvetica;
                font-size:26px;
                font-style:normal;
                font-weight:bold;
                line-height:125%;
                letter-spacing:normal;
                text-align:left;
            }
            h2{
                color:#202020;
                font-family:Helvetica;
                font-size:22px;
                font-style:normal;
                font-weight:bold;
                line-height:125%;
                letter-spacing:normal;
                text-align:left;
            }
            h3{
                color:#202020;
                font-family:Helvetica;
                font-size:20px;
                font-style:normal;
                font-weight:bold;
                line-height:125%;
                letter-spacing:normal;
                text-align:left;
            }
            h4{
                color:#202020;
                font-family:Helvetica;
                font-size:18px;
                font-style:normal;
                font-weight:bold;
                line-height:125%;
                letter-spacing:normal;
                text-align:left;
            }
            #templateHeader{
                border-top:0;
                border-bottom:0;
            }
            #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
                color:#202020;
                font-family:Helvetica;
                font-size:16px;
                line-height:150%;
                text-align:left;
            }
            #templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
                color:#2BAADF;
                font-weight:normal;
                text-decoration:underline;
            }
            #templateBody{
                border-top:0;
                border-bottom:0;
            }
            #templateBody .mcnTextContent,#templateBody .mcnTextContent p{
                color:#202020;
                font-family:Helvetica;
                font-size:16px;
                line-height:150%;
                text-align:left;
            }
            #templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
                color:#2BAADF;
                font-weight:normal;
                text-decoration:underline;
            }
            #templateFooter{
                border-top:0;
                border-bottom:0;
            }
            #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
                color:#202020;
                font-family:Helvetica;
                font-size:12px;
                line-height:150%;
                text-align:left;
            }
            #templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
                color:#202020;
                font-weight:normal;
                text-decoration:underline;
            }
                @media only screen and (min-width:768px){
                    .templateContainer{
                        width:600px !important;
                    }

            }   @media only screen and (max-width: 480px){
                    body,table,td,p,a,li,blockquote{
                        -webkit-text-size-adjust:none !important;
                    }

            }   @media only screen and (max-width: 480px){
                    body{
                        width:100% !important;
                        min-width:100% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    #bodyCell{
                        padding-top:10px !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnRetinaImage{
                        max-width:100% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnImage{
                        width:100% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageCardLeftImageContentContainer,.mcnImageCardRightImageContentContainer{
                        max-width:100% !important;
                        width:100% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnBoxedTextContentContainer{
                        min-width:100% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnImageGroupContent{
                        padding:9px !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
                        padding-top:9px !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnImageCardTopImageContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
                        padding-top:18px !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnImageCardBottomImageContent{
                        padding-bottom:9px !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnImageGroupBlockInner{
                        padding-top:0 !important;
                        padding-bottom:0 !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnImageGroupBlockOuter{
                        padding-top:9px !important;
                        padding-bottom:9px !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnTextContent,.mcnBoxedTextContentColumn{
                        padding-right:18px !important;
                        padding-left:18px !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
                        padding-right:18px !important;
                        padding-bottom:0 !important;
                        padding-left:18px !important;
                    }

            }   @media only screen and (max-width: 480px){
                    .mcpreview-image-uploader{
                        display:none !important;
                        width:100% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    h1{
                        font-size:22px !important;
                        line-height:125% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    h2{
                        font-size:20px !important;
                        line-height:125% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    h3{
                        font-size:18px !important;
                        line-height:125% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    h4{
                        font-size:16px !important;
                        line-height:150% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    table.mcnBoxedTextContentContainer td.mcnTextContent,td.mcnBoxedTextContentContainer td.mcnTextContent p{
                        font-size:14px !important;
                        line-height:150% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    td#templateHeader td.mcnTextContent,td#templateHeader td.mcnTextContent p{
                        font-size:16px !important;
                        line-height:150% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    td#templateBody td.mcnTextContent,td#templateBody td.mcnTextContent p{
                        font-size:16px !important;
                        line-height:150% !important;
                    }

            }   @media only screen and (max-width: 480px){
                    td#templateFooter td.mcnTextContent,td#templateFooter td.mcnTextContent p{
                        font-size:14px !important;
                        line-height:150% !important;
                    }
            }
        </style>
    </head>
    <body>
        <script type="text/javascript">
            // try { this.print(); } catch (e) { window.onload = window.print; }
        </script>
        <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="width: 100%" id="bodyTable">
        <!-- <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="54%" style="width: 58%" id="bodyTable"> -->
            <tbody>
                <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; line-height: 200%;">
                    <div style="text-align: center; border-bottom: solid 2px black; padding-bottom: 20px"><span style="font-size:24px"><strong>SistOpWeb</strong></span></div>
                    <table style="width:100%; display: inline-table;">
                        <tr>
                            <td colspan="2">
                                Cliente: {{  $usuario->nombre.' '.$usuario->apellido }}
                            </td>
                            <td colspan="2">
                                Optica: {{ $optica->nombre }}
                            </td>
                        </tr>
                        <tr>
                            <td>Tipo Lente: {{ $receta->tipo_lente->nombre }}</td>
                            <td>Adicion: {{ $receta->adicion }}</td>
                            <td>Altura: {{ $receta->altura }}</td>
                            <td>Distancia: {{ $receta->distancia }}</td>
                        </tr>
                    </table>
                    
                    <br>
                    @if( $lejos )
                    <table style="width:100%; display: inline-table; border-bottom: solid 3px black">
                        <tr style=" font-weight: bold">
                            <td colspan="2" style="border-bottom: solid 1px black">
                                LEJOS
                            </td>
                        </tr>
                        <tr style="border-bottom: solid 1px black;  font-weight: bold">
                            <td >Ojo Izquierdo</td>
                            <td >Ojo Derecho</td>
                        </tr>
                        <tr>
                            <td>Esf: {{ $lejos->oi_esferico }}</td>
                            <td>Esf: {{ $lejos->od_esferico }}</td>
                        </tr>
                        <tr>
                            <td>Cil: {{ $lejos->oi_cilindrico }}</td>
                            <td>Cil: {{ $lejos->od_cilindrico }}</td>
                        </tr>                        
                        <tr>
                            <td style="border-bottom: solid 1px black">Eje: {{ $lejos->oi_eje }}</td>
                            <td style="border-bottom: solid 1px black">Eje: {{ $lejos->od_eje }}</td>
                        </tr>
                        <tr>
                            <td>Material: {{ $lejos->material ? $lejos->material->nombre : '' }}</td>
                            <td>Color: {{ $lejos->color_id ? $lejos->color->nombre : '' }}</td>
                        </tr>
                        <tr>
                            <td>Origen: {{ $lejos->origen_lente }}</td>
                            <td>Tratamiento: {{ $lejos->trataemiento ? $lejos->tratamiento->nombre.' - '.$lejos->tratamiento_color : '' }}</td>
                        </tr>
                        <tr>
                            <td>Armazon: {{ $lejos->armazon }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                    </table>
                    @endif
                    <br>
                    @if( $cerca )
                    <table style="width:100%; display: inline-table;">
                        <tr style=" font-weight: bold">
                            <td colspan="2" style="border-bottom: solid 1px black">
                                CERCA
                            </td>
                        </tr>
                        <tr style="border-bottom: solid 1px black;  font-weight: bold">
                            <td >Ojo Izquierdo</td>
                            <td >Ojo Derecho</td>
                        </tr>
                        <tr>
                            <td>Esf: {{ $cerca->oi_esferico }}</td>
                            <td>Esf: {{ $cerca->od_esferico }}</td>
                        </tr>
                        <tr>
                            <td>Cil: {{ $cerca->oi_cilindrico }}</td>
                            <td>Cil: {{ $cerca->od_cilindrico }}</td>
                        </tr>                        
                        <tr>
                            <td style="border-bottom: solid 1px black">Eje: {{ $cerca->oi_eje }}</td>
                            <td style="border-bottom: solid 1px black">Eje: {{ $cerca->od_eje }}</td>
                        </tr>
                        <tr>
                            <td>Material: {{ $cerca->material ? $cerca->material->nombre : '' }}</td>
                            <td>Color: {{ $cerca->color_id ? $cerca->color->nombre : '' }}</td>
                        </tr>
                        <tr>
                            <td>Origen: {{ $cerca->origen_lente }}</td>
                            <td>Tratamiento: {{ $cerca->trataemiento ? $cerca->tratamiento->nombre.' - '.$cerca->tratamiento_color : '' }}</td>
                        </tr>
                        <tr>
                            <td>Armazon: {{ $cerca->armazon }}</td>
                        </tr>
                    </table>
                    @endif
                </td>
            </tbody>
        </table>
    </body>
</html>
