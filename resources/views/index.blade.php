﻿@extends('layout.layout')

@section('js')
    <script type="text/javascript" src="/js/estadisticas/index.js"></script>
        <!-- Moment Plugin Js -->
    <script src="/plugins/momentjs/moment.js"></script>
    <script src="/plugins/momentjs/moment_locale_es.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/plugins/daterangepicker/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="/plugins/daterangepicker/daterangepicker.css">
    
    <script type="text/javascript">
        $(document).ready(function($) {
            var est = new Estadisticas();
        });    
    </script>
@endsection

@section('content')
    <div class="crsf">
        {{ csrf_field() }}
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-5" style="margin-top: 1%">
                            <h2>SistOpWeb</h2>
                        </div>                        
                    </div>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-blue pointer hover-expand-effect" onclick="location.href = '/cliente'">
                                <div class="icon">
                                    <i class="material-icons">person_add</i>
                                </div>
                                <div class="content">
                                    <div class="text">NUEVO CLIENTE</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-5" style="margin-top: 1%">
                            <h2 id="text"></h2>
                        </div>
                        @if( $admin->opticas()->count() > 1 )
                            <div class="col-sm-3">
                                <select id="optica_estadisticas" class="form-control show-tick">
                                    <option value="all">Todas las Opticas</option>
                                    @foreach( $admin->opticas() as $optica )
                                        <option value="{{ $optica->id }}" {{ $optica->id == session('optica_id') ? 'selected' : '' }}>{{ $optica->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="col-sm-4">
                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                <span></span> <b class="caret"></b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-blue hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">face</i>
                                </div>
                                <div class="content">
                                    <div class="text">CLIENTES</div>
                                    <div class="number count-to" id="infobox_clientes" data-from="0" data-to="0" data-speed="1000"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-green hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">description</i>
                                </div>
                                <div class="content">
                                    <div class="text">RECETAS</div>
                                    <div class="number count-to" id="infobox_recetas" data-from="0" data-to="" data-speed="1000"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="card">
                                <div class="header bg-teal">
                                    <h2>RECETAS DE POR ESTADO</h2>
                                </div>
                                <div class="body">
                                    <div id="recetas_estado" class="dashboard-donut-chart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="card">
                                <div class="header bg-teal">
                                    <h2>RECETAS DE POR PROFESIONAL</h2>
                                </div>
                                <div class="body">
                                    <div id="recetas_medico" class="dashboard-donut-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection