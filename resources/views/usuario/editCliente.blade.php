<div class="modal fade" id="edit_user" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">             
            <div class="modal-header">
                <h4 class="modal-title">Editar Cliente</h4>  
                <hr>
            </div>          
            <div class="modal-body">
                <div class="row clearfix"> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input id="perfil_nombre" type="text" value="{{ $usuario->nombre }}" class="form-control">
                                    <label class="form-label">Nombre</label>
                                </div>
                            </div>                             
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input id="perfil_apellido" type="text" value="{{ $usuario->apellido }}" class="form-control">
                                    <label class="form-label">Apellido</label>
                                </div>
                            </div>                             
                        </div>
                    </div>
                </div>
                <div class="row clearfix"> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input id="perfil_dni" type="text" value="{{ $usuario->dni }}" class="form-control">
                                    <label class="form-label">DNI</label>
                                </div>
                            </div>                             
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input id="perfil_telefono" type="text" value="{{ $usuario->telefono }}" class="form-control">
                                    <label class="form-label">Telefono</label>
                                </div>
                            </div>                             
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line"  data-toggle="tooltip" data-placement="top" data-original-title="Agregando el email, despues se puede avisar al cliente cuando la receta esta lista.">
                                    <input id="perfil_direccion" type="text" value="{{ $usuario->direccion }}" class="form-control">
                                    <label class="form-label">Dirección</label>
                                </div>
                            </div>                             
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input id="perfil_mail" type="text" value="{{ $usuario->mail }}" class="form-control">
                                    <label class="form-label">Email</label>
                                </div>
                            </div>                             
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-6">
                        <select id="perfil_obra" class="form-control show-tick">
                            <option value="">Sin Obra Social</option>
                            @foreach( $obras as $obra )
                                <option value="{{ $obra->id }}" {{ $usuario->obra_social_id == $obra->id  ? 'selected' : '' }} >{{ $obra->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input id="perfil_num_obra" value="{{ $usuario->num_obra_social }}" type="text" class="form-control" {{ $usuario->obra_social_id ? '' : 'disabled' }}>
                                    <label class="form-label">N° Obra Social</label>
                                </div>
                            </div>                             
                        </div>
                    </div>
                    <label class="m-l-15" style="font-size: 12px">*Si elige [OTRA] especifique en el campo adjunto</label>
                </div>
            </div>
            <div class="modal-footer">
                <hr>
                <button id="save_perfil" type="button" usuario="{{ $usuario->id }}" class="btn btn-lg bg-green waves-effect pull-right">Guardar</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>