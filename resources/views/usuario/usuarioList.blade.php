@extends('layout.layout')

@section('content')
<div class="crsf">
    {{ csrf_field() }}
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-sm-3">
                        <h4>
                            CLIENTES
                        </h4>
                    </div>
                    <div class="col-sm-3 pull-right">
                        <button type="button" href="#new_user" data-toggle="modal" class="btn btn-lg btn-info waves-effect pull-right">
                            <i class="material-icons">person_add</i>
                            <span>NUEVO CLIENTE</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="body">
                <div class="table-responsive" style="overflow-x: hidden !important;">
                    <table id="data-table-basic" class="table table-hover">
                        <thead>
                            <tr>
                                <th>Apellido</th>
                                <th>Nombre</th>
                                <th>DNI</th>
                                <th>Telefono</th>
                                <th>Ultima Receta</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table-body">
                            @foreach($usuarios as $usuario)
                            <tr>
                                <td>{{ $usuario->apellido }}</td>
                                <td>{{ $usuario->nombre }}</td>
                                <td>{{ $usuario->dni }}</td>
                                <td>{{ $usuario->telefono }}</td>
                                <td style="text-align: justify;">{{ is_null($usuario->ultimoProducto() ) ? 'No tiene receta' : $usuario->UltimoProducto()->tipoProducto->nombre.' - '.$usuario->ultimoProducto()->created_at->format('m/Y') }}</td>
                                <td>
                                    @if( $usuario->productos()->count() )
                                        <a href="/cliente/{{ $usuario->id }}/receta/{{ $usuario->ultimoProducto()->id }}" class="material-icons" title="Ver Ultima Receta del Cliente">remove_red_eye</a>
                                    @endif
                                    <a class="material-icons" name="new_receta_button" data-toggle="modal" href="#new_receta" usuario-id="{{ $usuario->id }}" data-placement="bottom" title="Nueva Receta">note_add</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modals')
    @include('usuario.nuevoCliente')

    <div class="modal fade" id="new_receta" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">             
                <div class="modal-header">
                    <h4 class="modal-title">Nueva Receta</h4>  
                    <hr>
                </div>          
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-2">
                            <label class="control-label m-t-5">Producto</label>   
                        </div>                              
                        <div class="col-sm-4">
                            <div class="fg-line">
                                <select id="modal_tipo_producto" class="show-tick form-control">
                                    <option value="">Producto</option>
                                    <option value="anteojo">Lentes Aereos</option>
                                    <option value="lente">Lentes de Contacto</option>
                                </select>
                            </div>                                  
                        </div>
                        <div id="tipo_lente_anteojo" style="display: none">
                            <div class="col-sm-2">
                                <label class="control-label m-t-5">Tipo</label>   
                            </div>                              
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <select id="modal_tipo_anteojo" class="show-tick form-control">
                                        <option value="">Tipo Lente</option>
                                        <option value="bifocal">Bifocal</option>
                                        <option value="multifocal">Multifocal</option>
                                        <option value="monofocal">Monofocal</option>
                                    </select>
                                </div>                                  
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div id="cantidad_anteojo" class="m-t-10" style="display: none">
                            <div class="col-sm-2">
                                <label class="control-label m-t-5">Cantidad Anteojos</label>   
                            </div>                              
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <select id="modal_anteojo" class="show-tick form-control">
                                        <option value="">Lejos o Cerca</option>
                                        <option value="lejos">Lejos</option>
                                        <option value="cerca">Cerca</option>
                                        <option value="ambos">Lejos y Cerca</option>
                                    </select>
                                </div>                                  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <hr>
                    <button id="create_receta" type="button" usuario-id="" class="btn bg-green waves-effect pull-right">Guardar</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script type="text/javascript" src="/js/general/index.js"></script>
    <script type="text/javascript" src="/js/usuario/index.js"></script>
    <script type="text/javascript"> // INIT
        $(document).ready(function() {
            var us = new Usuario();
            var newUs = new UsuarioNew()
        });
    </script>
@endsection