@extends('layout.layout')

@section('content')
<div class="crsf">
	{{ csrf_field() }}
</div>
<div class="col-sm-12">
    <div class="row clearfix">
        <div class="card">
            <div class="header">
                <?php $meses = array(   'January'   => 'Enero',
                                        'February'  => 'Febrero',
                                        'March'     => 'Marzo',
                                        'April'     => 'Abril',
                                        'May'       => 'Mayo',
                                        'June'      => 'Junio',
                                        'July'      => 'Julio',
                                        'August'    => 'Agosto',
                                        'September' => 'Septiembre',
                                        'October'   => 'Octubre',
                                        'November'  => 'Noviembre',
                                        'December'  => 'Diciembre'  );?>
                <h4 >Receta de {{ $producto->tipoProducto->nombre }} - {{ $meses[ $producto->created_at->format('F') ].' '.$producto->created_at->format('Y')}}</h4>
            </div>
            <div class="body">
                @if( $producto->tipoProducto->nombre == 'Lentes Aereos' )
                    <div id="{{ $receta->id }}" class="row clearfix">
                        @include('receta.anteojoEdit')
                    </div>
                @else
                    <div id="{{ $receta->id }}" class="row clearfix">
                        @include('receta.lenteEdit')
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- BUTTONS -->
<div class="row clearfix">
    <div class="col-sm-2">
        <div class="info-box-4 bg-blue pointer hover-expand-effect" data-toggle="modal" href="#edit_user">
            <div class="icon">
                <i class="material-icons">mode_edit</i>
            </div>
            <div class="content">
                <div class="text">EDITAR CLIENTE</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="info-box-4 bg-orange pointer hover-expand-effect" data-toggle="modal" href="#more_recetas">
            <div class="icon">
                <i class="material-icons">contacts</i>
            </div>
            <div class="content">
                <div class="text">OTRAS RECETAS DE {{ strtoupper($usuario->nombre) }}</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <a href="/cliente/{{ $usuario->id }}/receta/{{ $producto->id }}/download" target="_blank">    
            <div class="info-box-4 pointer bg-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">print</i>
                    </div>
                    <div class="content">
                        <div class="text">IMPRIMIR RECETA</div>
                        <div class="number"></div>
                    </div>
            </div>
        </a>
    </div>
</div>
@endsection

@section('modals')
    @include('usuario.editCliente')
    <div class="modal fade" id="more_recetas" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">             
                <div class="modal-header">
                    <h4 class="modal-title">Recetas de {{ $usuario->nombre.' '.$usuario->apellido }}</h4>  
                    <hr>
                </div>          
                <div class="modal-body">
                    <div class="table-responsive" style="overflow-x: hidden !important;">
                        <table id="data-table-basic" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Fecha</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if( $usuario->productos()->count() == 1 )
                                    <li class="list-group-item">No hay mas recetas a parte de esta</li>
                                @else
                                    @foreach( $usuario->productos()->orderBy('created_at', 'desc')->limit(3)->get() as $prod )
                                        @if( $prod->id != $producto->id )
                                            <tr>
                                                <td>{{ $prod->tipoProducto->nombre }}</td>
                                                <td>{{ $prod->created_at->format('d').' '.$meses[ $prod->created_at->format('F') ].' '.$prod->created_at->format('Y') }}</td>
                                                <td>
                                                    <a href="{{ $prod->id }}" class="material-icons" title="Ver Ultima Receta del Cliente">remove_red_eye</a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <link rel="stylesheet" type="text/css" href="/plugins/waitme/waitMe.css">

    <script type="text/javascript" src="/plugins/waitme/waitMe.js"></script>
    <script type="text/javascript" src="/js/general/index.js"></script>
    <script type="text/javascript" src="/js/usuario/index.js"></script>
    <script type="text/javascript" src="/js/receta/index.js"></script>
    <script type="text/javascript"> // INIT
        $(document).ready(function() {
            var usuarioEdit = new UsuarioEdit();
        });
    </script>
@endsection