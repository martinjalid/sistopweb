<div class="col-sm-12">
    <div class="col-sm-3">
        <div class="form-group">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Tipo de Lente</label> 
                    <input id="tipo_lente" class="form-control" type="text" value="{{ $receta->tipo_lente }}">
                </div>                                  
            </div>                                  
        </div>                                  
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Diametro</label> 
                    <input class="form-control color" name="diametro" value="{{ $receta->diametro }}">
                </div>                                  
            </div>                                  
        </div>                                  
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Radio</label>
                    <input class="form-control" name="radio" value="{{ $receta->radio }}"> 
                </div>                                  
            </div>                                  
        </div>                                  
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Color</label>
                    <input class="form-control" name="color" value="{{ $receta->color }}"> 
                </div>                                  
            </div>                                  
        </div>                                  
    </div>
    <div class="col-sm-3 ">
        <div class="form-group">
            <div class="form-group form-float">
                <div class="form-line">
                    <select class="show-tick form-control" name="profesional_id"> 
                        <option value="">Sin Profesional</option>
                        @foreach( $optica->profesionales as $p)
                            <option value="{{ $p->id }}" {{ $producto->profesional_id == $p->id ? 'selected' : '' }}>{{ $p->nombre }}</option>    
                        @endforeach
                    </select>
                </div>                                  
            </div>                                  
        </div>                               
    </div>
</div>
<div id="oftalmologo" class="col-sm-6">
    <div class="row clearfix">
        <h4 class="font-bold">Receta Oftalmológica</h4>
        <div class="row clearfix">
            <div class="col-sm-2">
                <label class="control-label m-t-5 m-l-10">Ojo D</label> 
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Esférico</label> 
                            <input class="form-control color" name="od_esferico" value="{{ $oftalmologo->od_esferico }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Cilindrico</label>
                            <input class="form-control color" name="od_cilindrico" value="{{ $oftalmologo->od_cilindrico }}"> 
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Eje</label> 
                            <input class="form-control color" name="od_eje" value="{{ $oftalmologo->od_eje }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-sm-2">
                <label class="control-label m-t-5 m-l-10">Ojo I</label> 
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Esférico</label> 
                            <input class="form-control color" name="oi_esferico" value="{{ $oftalmologo->oi_esferico }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Cilindrico</label>
                            <input class="form-control color" name="oi_cilindrico" value="{{ $oftalmologo->oi_cilindrico }}"> 
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Eje</label> 
                            <input class="form-control color" name="oi_eje" value="{{ $oftalmologo->oi_eje }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
        </div>
    </div>
</div>
<div id="prueba" class="col-sm-6">
    <div class="row clearfix">
        <h4 class="lead-label">Lente de Prueba</h4>   
        <div class="row clearfix">
            <div class="col-sm-2">
                <label class="control-label m-t-5 m-l-10">Ojo D</label> 
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Esférico</label> 
                            <input class="form-control color" name="od_esferico" value="{{ $prueba->od_esferico }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Cilindrico</label>
                            <input class="form-control color" name="od_cilindrico" value="{{ $prueba->od_cilindrico }}"> 
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Eje</label> 
                            <input class="form-control color" name="od_eje" value="{{ $prueba->od_eje }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-sm-2">
                <label class="control-label m-t-5 m-l-10">Ojo I</label> 
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Esférico</label> 
                            <input class="form-control color" name="oi_esferico" value="{{ $prueba->oi_esferico }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Cilindrico</label>
                            <input class="form-control color" name="oi_cilindrico" value="{{ $prueba->oi_cilindrico }}"> 
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Eje</label> 
                            <input class="form-control color" name="oi_eje" value="{{ $prueba->oi_eje }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
        </div>
    </div>
</div>
<div id="lentes_pedidas" class="col-sm-6">
    <div class="row clearfix">
        <h4 class="lead-label">Lentes Pedidas</h4>
        <div class="row clearfix">
            <div class="col-sm-2">
                <label class="control-label m-t-5 m-l-10">Ojo D</label> 
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Esférico</label> 
                            <input class="form-control color" name="od_esferico" value="{{ $receta->od_esferico }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Cilindrico</label>
                            <input class="form-control color" name="od_cilindrico" value="{{ $receta->od_cilindrico }}"> 
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Eje</label> 
                            <input class="form-control color" name="od_eje" value="{{ $receta->od_eje }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
        <br>
        </div>
        <div class="row clearfix">
            <div class="col-sm-2">
                <label class="control-label m-t-5 m-l-10">Ojo I</label> 
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Esférico</label> 
                            <input class="form-control color" name="oi_esferico" value="{{ $receta->oi_esferico }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Cilindrico</label>
                            <input class="form-control color" name="oi_cilindrico" value="{{ $receta->oi_cilindrico }}"> 
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Eje</label> 
                            <input class="form-control color" name="oi_eje" value="{{ $receta->oi_eje }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
        </div>

    </div>
</div>
<div id="agudeza" class="col-sm-4">   
    <div class="row clearfix">
        <h4 class="lead-label">Agudeza Visual</h4>
        <div class="row clearfix">
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Ojo D</label> 
                            <input class="form-control color" name="od" value="{{ $prueba->od_agudeza }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Ojo I</label>
                            <input class="form-control color" name="oi" value="{{ $prueba->oi_agudeza }}"> 
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Ambos</label> 
                            <input class="form-control color" name="ao" value="{{ $prueba->ao_agudeza }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
        </div>
    </div>
</div>
<div id="queratometria" class="col-sm-4">
    <div class="row clearfix">
        <h4 class="lead-label">Queratometria</h4>
        <div class="row clearfix">
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Ojo D</label> 
                            <input class="form-control color" name="od" value="{{ $receta->queratometria_od }}">
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Ojo I</label>
                            <input class="form-control color" name="oi" value="{{ $receta->queratometria_oi }}"> 
                        </div>                                  
                    </div>                                  
                </div>                                  
            </div>
        </div>
    </div>
</div>
<div id="medidas_r" class="col-sm-12">
    <div class="col-sm-8">
        <div class="form-group">
            <div class="form-line">
                <label class="form-label">Observación</label>
                <textarea rows="1" id="observacion" class="form-control no-resize auto-growth" style="overflow: hidden; word-wrap: break-word; height: 80px;">{{ $producto->observacion }}</textarea>
            </div>
        </div>
    </div>
        <div class="col-sm-3" style="margin-top: 5px;">
            <button type="button" id="guardar_receta" class="btn btn-lg bg-green waves-effect pull-left" style="background: #468e46">Guardar Receta</button>
        </div>
</div>

<script type="text/javascript">
    $(document).ready(function($) {
        var recetaLenteEdit = new RecetaLenteEdit();
    });
</script>