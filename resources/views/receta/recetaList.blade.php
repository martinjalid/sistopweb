@extends('layout.layout')

@section('content')
<div class="crsf">
    {{ csrf_field() }}
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h4>RECETAS</h4>
            </div>
            <div class="body">
                <div class="table-responsive" style="overflow-x: hidden !important;">
                    <table id="data-table-basic" class="table table-hover">
                        <thead>
                            <tr>
                                <th data-column-id="apellido">Apellido</th>
                                <th data-column-id="nombre">Nombre</th>
                                <th data-column-id="producto">Producto</th>
                                <th data-column-id="created_at">Fecha Creación</th>
                                <th data-column-id="created_at">Profesional</th>
                                <th data-column-id="estado">Estado</th>
                                <th data-column-id="acciones"></th>
                            </tr>
                        </thead>
                        <tbody id="table-body">
                            @foreach($recetas as $receta)
                            <tr>
                                <td>{{ $receta->usuario->apellido }}</td>
                                <td>{{ $receta->usuario->nombre }}</td>
                                <td style="text-align: justify;">{{ $receta->tipoProducto->nombre }}</td>
                                <td>{{ $receta->created_at->format('Y-m-d') }}</td>
                                <td>{{ $receta->medico() == null ? 'Sin Profesional' : $receta->medico()->nombre }}</td>
                                <td>{{ $receta->estado() }}</td>
                                <td>
                                    <a href="/cliente/{{ $receta->usuario_id }}/receta/{{ $receta->id }}" class="material-icons" title="Ver Receta">remove_red_eye</a>
                                    <a class="material-icons pointer changeEstado" to-estado="Eliminar" producto-id="{{ $receta->id }}" title="Eliminar Receta">close</a>
                                    @if( $receta->estado() == 'Pendiente' )
                                        <a class="material-icons pointer changeEstado" to-estado="Entregada" producto-id="{{ $receta->id }}" title="Marcar Receta Como Entregada">done</a>
                                        <a class="material-icons pointer sendMail" producto-id="{{ $receta->id }}" mail="{{ $receta->usuario->mail }}" title="Enviar mail al cliente">mail_outline</a>
                                    @endif
                                    @if( $receta->estado() != 'Pendiente' )
                                        <a class="material-icons pointer changeEstado" to-estado="Pendiente" producto-id="{{ $receta->id }}" title="Marcar Receta Como Pendiente">alarm</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table> 
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="row clearfix">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <div class="info-box-2 bg-blue hover-expand-effect" data-toggle="modal" href="#new_user">
            <div class="icon">
                <i class="material-icons">person_add</i>
            </div>
            <div class="content">
                <div class="text">NUEVO CLIENTE</div>
                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
</div> -->
@endsection

@section('modals')

    <div class="modal fade" id="new_receta" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">             
                <div class="modal-header">
                    <h4 class="modal-title">Nueva Receta</h4>  
                    <hr>
                </div>          
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-2">
                            <label class="control-label m-t-5">Tipo de Producto</label>   
                        </div>                              
                        <div class="col-sm-4">
                            <div class="fg-line">
                                <select id="modal_tipo_producto" class="select2 form-control">
                                    <option value="">Producto</option>
                                    <option value="anteojo">Anteojo</option>
                                    <option value="lente">Lente de Contacto</option>
                                </select>
                            </div>                                  
                        </div>
                        <div id="tipo_lente_anteojo" style="display: none">
                            <div class="col-sm-2">
                                <label class="control-label m-t-5">Tipo Lente Anteojo</label>   
                            </div>                              
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <select id="modal_tipo_anteojo" class="select2 form-control">
                                        <option value="">Tipo Lente</option>
                                        <option value="bifocal">Bifocal</option>
                                        <option value="multifocal">Multifocal</option>
                                        <option value="monofocal">Monofocal</option>
                                    </select>
                                </div>                                  
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div id="cantidad_anteojo" class="m-t-10" style="display: none">
                            <div class="col-sm-2">
                                <label class="control-label m-t-5">Cantidad Anteojos</label>   
                            </div>                              
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <select id="modal_anteojo" class="select2 form-control">
                                        <option value="">Lejos o Cerca</option>
                                        <option value="lejos">Lejos</option>
                                        <option value="cerca">Cerca</option>
                                        <option value="ambos">Lejos y Cerca</option>
                                    </select>
                                </div>                                  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <hr>
                    <button id="create_receta" type="button" usuario-id="" class="btn bg-green waves-effect pull-right">Guardar</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script type="text/javascript" src="/js/general/index.js"></script>
    <script type="text/javascript" src="/js/receta/index.js"></script>
    <script type="text/javascript"> // INIT
        $(document).ready(function() {
            var rec = new RecetaList();
        });
    </script>
@endsection