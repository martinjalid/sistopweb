<?php foreach ($receta->anteojos() as $anteojo) {
    if ( $anteojo->tipo == 'cerca' ) 
        $cerca = $anteojo;
    else
        $lejos = $anteojo; 
}
?>
<div class="col-sm-12">
    <div class="col-sm-3">
        <div class="form-group">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Tipo de Lente</label> 
                    <input id="tipo_lente" class="form-control" type="text" value="{{ $receta->tipo_lente->nombre }}" disabled>
                </div>                                  
            </div>                                  
        </div>                                  
    </div>
    @if( $receta->tipo_lente->nombre == 'Monofocal' )
    <?php $select = ( $receta->anteojos()->count() == '2' ) ? '3' : ( $receta->anteojos()->first()->tipo == 'cerca' ? '2' : '1') ;?>
        <div class="col-sm-3 ">
            <select class="show-tick form-control" disabled> 
                <option {{ $select ==  '1' ? 'selected' : '' }}>Lejos</option>
                <option {{ $select ==  '2' ? 'selected' : '' }}>Cerca</option>    
                <option {{ $select ==  '3' ? 'selected' : '' }}>Lejos y Cerca</option>    
            </select>
        </div>                            
    @endif
    <div class="col-sm-3">
        <div class="form-group">
            <div class="form-group form-float">
                <div class="form-line">
                    <label class="form-label">Detalle Lente</label> 
                    <input id="detalle_lente" class="form-control" type="text" name="detalle_lente"value="{{ $receta->detalle_lente }}">
                </div>                                  
            </div>                                  
        </div>                                  
    </div>
    <div class="col-sm-3 ">
        <div class="form-group">
            <div class="form-group form-float">
                <div class="form-line">
                    <select class="show-tick form-control" name="profesional_id"> 
                        <option value="">Sin Profesional</option>
                        @foreach( $optica->profesionales as $p)
                            <option value="{{ $p->id }}" {{ $producto->profesional_id == $p->id ? 'selected' : '' }}>{{ $p->nombre }}</option>    
                        @endforeach
                    </select>
                </div>                                  
            </div>                                  
        </div>                               
    </div>
</div>
@if( isset( $lejos ) )
    <hr>
    <div id="lejos">
        <h4 class="lead-label m-l-15">Lejos</h4>
        <div id="lejos-container" class="m-l-20">
            <div class="row">
                <div class="col-sm-12 p-l-0">
                    <div class="col-sm-1">
                        <label class="control-label m-t-5 m-l-10">Ojo D</label> 
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Esférico</label> 
                                    <input class="form-control color" name="od_esferico" value="{{ $lejos->od_esferico }}">
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Cilindrico</label>
                                    <input class="form-control color" name="od_cilindrico" value="{{ $lejos->od_cilindrico }}"> 
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Eje</label> 
                                    <input class="form-control color" name="od_eje" value="{{ $lejos->od_eje }}">
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                    <div class="col-sm-2 multi">
                        <select class="show-tick form-control" name="material_lente_id"> 
                            <option value="">Material</option>
                            @foreach($material_lente as $m)
                                <option value="{{ $m->id }}" {{ $lejos->material_lente_id == $m->id ? 'selected' : '' }}>{{ $m->nombre }}</option>    
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 multi">
                        <select class="show-tick form-control" name="color_id"> 
                            <option value="">Color</option>
                            @foreach($color as $c)
                                <option value="{{ $c->id }}" {{ $lejos->color_id == $c->id ? 'selected' : '' }} >{{ $c->nombre }}</option>    
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 c_tratamiento">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Tipo Tratamiento</label> 
                                    <input class="form-control" name="tratamiento_color" value="{{ $lejos->tratamiento_color }}">
                                </div>                                  
                            </div>                                  
                        </div> 
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                     <select class="show-tick form-control" name="origen_lente"> 
                                        <option value='' >Origen Lente</option>
                                        <option value="Stock" {{ $anteojo->origen_lente ==  'Stock' ? 'selected' : '' }}>Stock</option>
                                        <option value="Laboratorio" {{ $anteojo->origen_lente ==  'Laboratorio' ? 'selected' : '' }}>Laboratorio</option>    
                                    </select>
                                </div>                                  
                            </div>                                  
                        </div> 
                    </div>
                </div>

                <div class="col-sm-12 p-l-0">
                    <div class="col-sm-1">
                        <label class="control-label m-t-5 m-l-10">Ojo I</label> 
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Esférico</label> 
                                    <input class="form-control color" name="oi_esferico" value="{{ $lejos->oi_esferico }}">
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Cilindrico</label>
                                    <input class="form-control color" name="oi_cilindrico" value="{{ $lejos->oi_cilindrico }}"> 
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Eje</label> 
                                    <input class="form-control color" name="oi_eje" value="{{ $lejos->oi_eje }}">
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                    <div class="col-sm-2" style="display: block">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Armazon</label> 
                                    <input class="form-control" name="armazon" value="{{ $lejos->armazon }}" >
                                </div>                                  
                            </div>                                  
                        </div> 
                    </div>
                    <div class="col-sm-2 multi">
                        <select class="show-tick form-control" name="tratamiento_id"> 
                            <option value="">Sin Tratamiento</option>
                            @foreach($tratamiento as $t)
                                <option value="{{ $t->id }}" {{ $lejos->tratamiento_id == $t->id ? 'selected' : '' }} >{{ $t->nombre }}</option>  
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 multi">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Valor Lente</label> 
                                    <input class="form-control" name="valor_lente" value="{{ $lejos->valor_lente }}">
                                </div>                                  
                            </div>                                  
                        </div>
                    </div>
                    <div class="col-sm-2 multi" >
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Valor Armazon</label> 
                                    <input class="form-control" name="valor_armazon" value="{{ $lejos->valor_armazon }}">
                                </div>                                  
                            </div>                                  
                        </div>
                    </div>
                    
                </div>                
            </div>
        </div>
    </div>
@endif
@if( isset( $cerca ) )
    <hr>
    <div id="cerca">
        <h4 class="lead-label m-l-15">Cerca</h4>
        <div id="cerca-container" class="m-l-20">
            <div class="row">
                    <div class="col-sm-1">
                        <label class="control-label m-t-5 m-l-10">Ojo D</label> 
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Esférico</label> 
                                    <input class="form-control color" name="od_esferico" value="{{ $cerca->od_esferico }}">
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Cilindrico</label>
                                    <input class="form-control color" name="od_cilindrico" value="{{ $cerca->od_cilindrico }}"> 
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Eje</label> 
                                    <input class="form-control color" name="od_eje" value="{{ $cerca->od_eje }}">
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                    @if( $receta->tipo_lente->nombre == 'Monofocal' ) 
                        <div class="col-sm-2 multi">
                            <select class="show-tick form-control" name="material_lente_id"> 
                                <option value="">Material</option>
                                @foreach($material_lente as $m)
                                    <option value="{{ $m->id }}" {{ $cerca->material_lente_id == $m->id ? 'selected' : '' }}>{{ $m->nombre }}</option>    
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2 multi">
                            <select class="show-tick form-control" name="color_id"> 
                                <option value="">Color</option>
                                @foreach($color as $c)
                                    <option value="{{ $c->id }}" {{ $cerca->color_id == $c->id ? 'selected' : '' }} >{{ $c->nombre }}</option>    
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2 c_tratamiento">
                            <div class="form-group">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Tipo Tratamiento</label> 
                                        <input class="form-control" name="tratamiento_color" value="{{ $cerca->tratamiento_color }}">
                                    </div>                                  
                                </div>                                  
                            </div> 
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                         <select class="show-tick form-control" name="origen_lente"> 
                                            <option value='' >Origen Lente</option>
                                            <option value="Stock" {{ $anteojo->origen_lente ==  'Stock' ? 'selected' : '' }}>Stock</option>
                                            <option value="Laboratorio" {{ $anteojo->origen_lente ==  'Laboratorio' ? 'selected' : '' }}>Laboratorio</option>    
                                        </select>
                                    </div>                                  
                                </div>                                  
                            </div> 
                        </div>
                    @endif
            </div>
            <div class="row">
                <div class="col-sm-1">
                    <label class="control-label m-t-5 m-l-10">Ojo I</label> 
                </div>
                <div class="col-sm-1">
                    <div class="form-group">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="form-label">Esférico</label> 
                                <input class="form-control color" name="oi_esferico" value="{{ $cerca->oi_esferico }}">
                            </div>                                  
                        </div>                                  
                    </div>                                  
                </div>
                <div class="col-sm-1">
                    <div class="form-group">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="form-label">Cilindrico</label>
                                <input class="form-control color" name="oi_cilindrico" value="{{ $cerca->oi_cilindrico }}"> 
                            </div>                                  
                        </div>                                  
                    </div>                                  
                </div>
                <div class="col-sm-1">
                    <div class="form-group">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="form-label">Eje</label> 
                                <input class="form-control color" name="oi_eje" value="{{ $cerca->oi_eje }}">
                            </div>                                  
                        </div>                                  
                    </div>                                  
                </div>
                @if( $receta->tipo_lente->nombre == "Monofocal" )
                    <div class="col-sm-2" style="display: block">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Armazon</label> 
                                    <input class="form-control" name="armazon" value="{{ $cerca->armazon }}" >
                                </div>                                  
                            </div>                                  
                        </div> 
                    </div>
                    <div class="col-sm-2 multi">
                        <select class="show-tick form-control" name="tratamiento_id"> 
                            <option value="">Sin Tratamiento</option>
                            @foreach($tratamiento as $t)
                                <option value="{{ $t->id }}" {{ $cerca->tratamiento_id == $t->id ? 'selected' : '' }} >{{ $t->nombre }}</option>  
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 multi">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Valor Lente</label> 
                                    <input class="form-control" name="valor_lente" value="{{ $cerca->valor_lente }}">
                                </div>                                  
                            </div>                                  
                        </div>
                    </div>
                    <div class="col-sm-2 multi" >
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Valor Armazon</label> 
                                    <input class="form-control" name="valor_armazon" value="{{ $cerca->valor_armazon }}">
                                </div>                                  
                            </div>                                  
                        </div>
                    </div>

                @endif
            </div>
        </div>
    </div>
@endif
<div id="receta">
    <div id="receta-container" class="m-l-20">
        <div class="row">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="form-label">Distancia Interpupilar</label> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Ojo Der</label> 
                                    <input class="form-control color" name="distancia_der" value="{{ $receta->distancia_der }}">
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Ojo Izq</label> 
                                    <input class="form-control color" name="distancia_izq" value="{{ $receta->distancia_izq }}">
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                </div>
            </div>
            @if( $receta->tipo_lente->nombre != 'Monofocal' )
                <div class="col-sm-2">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Altura</label>
                                    <input class="form-control" name="altura" value="{{ $receta->altura }}"> 
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Adición</label>
                                    <input class="form-control" name="adicion" value="{{ $receta->adicion }}"> 
                                </div>                                  
                            </div>                                  
                        </div>                                  
                    </div>
                </div>
            @endif
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <label class="form-label">Observación</label>
                            <textarea rows="1" id="observacion" class="form-control no-resize auto-growth" style="overflow: hidden; word-wrap: break-word; height: 150px;">{{ $producto->observacion }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3" style="margin-top: 5%;">
                <div class="col-sm-12">
                    <?php 
                        $precio = 0;
                        if ( isset($lejos) ) 
                            $precio += $lejos->valor_lente + $lejos->valor_armazon;

                        if ( isset($cerca) ) 
                            $precio += $cerca->valor_lente + $cerca->valor_armazon;
                        
                     ?>
                    <h4 class="control-label m-t-10">Precio Total : $ {{ $precio }}</h4> 
                </div>
                <button type="button" id="guardar_receta" class="btn btn-lg bg-green waves-effect pull-left" style="background: #468e46">Guardar Receta</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function($) {
        var recetaEdit = new RecetaEdit();
    });
</script>