<!DOCTYPE html>
<html>

<head>
	<title>SistOpWeb</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
    <link href="/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="/plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/sweetalert2.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/css/themes/all-themes.css" rel="stylesheet" />
    
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/toastr.css">
    <link rel="icon" type="image/jpg" href="/images/icon.png">

    <script src="/plugins/jquery/jquery.min.js"></script>
    <script src="/plugins/bootstrap/js/bootstrap.js"></script>
    <script src="/plugins/bootstrap-select/js/bootstrap-select.js"></script>
    <script src="/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <script src="/plugins/node-waves/waves.js"></script>
    <script src="/plugins/jquery-countto/jquery.countTo.js"></script>
    <script src="/plugins/raphael/raphael.min.js"></script>
    <script src="/plugins/morrisjs/morris.js"></script>
    <script src="/plugins/jquery-sparkline/jquery.sparkline.js"></script>



    <script src="/js/admin.js"></script>
    <script src="/js/sweetalert2.min.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="/plugins/jquery-countto/jquery.countTo.js"></script>
    <script type="text/javascript" src="/js/functions.js"></script>
    <script type="text/javascript" src="/js/toastr.js"></script>
    <script type="text/javascript" src="/js/general/index.js"></script>
    <script type="text/javascript">
        $(document).ready(function($) {
            window.optica_id = '{{ session("optica_id") }}';
            window.end_free_trial = '{{ session("end_free_trial")}}';
            window.canceled_suscription = '{{ session("canceled_suscription")}}';

            var raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);
            console.log( raw ? parseInt(raw[2], 10) : false );

            var _this = this;

            var csrf = document.getElementsByName('_token')[0].value;

            var url = location.href.split('/');

            if ( window.optica_id == 0 ){
                $("#elegir_optica").modal();
                
                $("#log_in_optica").click(function(){
                    $("#log_in_optica").attr('disabled', 'true');
                    if( $("#optica_modal").val() == 0 ){
                        $("#log_in_optica").removeAttr('disabled');
                        toastr['error']('Debe Elegir una Óptica');
                    }else{
                        $.ajax({
                            url: '/logOptica',
                            dataType: 'json',
                            data: { optica_id: $("#optica_modal").val()},
                        })
                        .done(function(resp) {
                            if (resp.error) {
                                toastr['error']('Hubo un error.', 'Error');
                            }else{
                                location.reload();
                            }
                        })                        
                    }
                });
            }
            
            /*var suscript = true; 

            if ( window.end_free_trial && url[ url.length - 1 ] != 'suscripcion' ) {
                $.ajax({
                    url: '/checkSuscripcion',
                    dataType: 'json',
                })
                .done(function(resp) {
                    if (resp.error) {
                        toastr['error']('Hubo un error.', 'Error');
                    }else{
                        if ( resp.status != 'authorized' ) {
                            suscript = false;
                        }
                    }
                }) 
            }
         
            if ( window.end_free_trial && url[ url.length - 1 ] != 'suscripcion' )
                $("#end_free_trial").modal();

            if ( (window.canceled_suscription || !suscript ) && url[ url.length - 1 ] != 'suscripcion')
                $("#inactive_account").modal();*/

        });
    </script>
</head>

<body class="theme-blue-grey">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Cargando...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay" style="display: none"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                @foreach( $rutas as $ruta )
                    @if( $ruta === reset($rutas) )
                        <a href="{{ $ruta['url'] }}" class="navbar-brand" style="margin-left: 10px">{{ $ruta['nombre'] }} {{ isset($ruta['last']) ? '' : '/' }}</a>
                    @else
                        <a href="{{ $ruta['url'] }}" class="navbar-brand">{{ $ruta['nombre'] }} {{ isset($ruta['last']) ? '' : '/' }}</a>
                    @endif
                @endforeach
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="preguntas" class="js-search" title="Preguntas Frecuentes" data-close="true" style="padding-right: 15px">
                            <i class="fa fa-question-circle-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <!-- <div class="image">
                    <img src="/images/user.png" width="48" height="48" alt="User" />
                </div> -->
                <div class="info-container" style="top: 65%">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" id="sidebar_optica" aria-expanded="false">{{ session('optica_id') == 0 ? 'Opticas' : $admin->opticas()->find( session('optica_id') )->nombre }}</div>
                    <div class="email">{{ $admin->mail }}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            @if( $admin->opticas()->count() > 1 )
                                <li><a data-toggle="modal" href="#elegir_optica"><i class="material-icons">launch</i>Cambiar Óptica</a></li>
                                <li role="seperator" class="divider"></li>
                            @endif
                            <li><a href="/logout"><i class="material-icons">input</i>Cerrar Sesion</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="active">
                        <a href="/estadisticas">
                            <i class="material-icons">home</i>
                            <span>Inicio</span>
                        </a>
                    </li>
                    <li>
                        <a href="/cliente">
                            <i class="material-icons">supervisor_account</i>
                            <span>Clientes</span>
                        </a>
                    </li>
                    <li >
                        <a href="/recetas">
                            <i class="material-icons">description</i>
                            <span>Recetas</span>
                        </a>
                    </li>
                    <li >
                        <a href="/administracion">
                            <i class="material-icons">settings</i>
                            <span>Administración</span>
                        </a>
                    </li>
                    @if( $admin->perfil() == "Administrador" )
                        <!--
                        <li >
                            <a href="/suscripcion">
                                <i class="material-icons">bookmark_border</i>
                                <span>Suscripción</span>
                            </a>
                        </li>-->
                    @endif
                    @if( $admin->mail == "martinjalid@gmail.com" )
                        <li >
                            <a href="/superadmin">
                                <i class="material-icons">star</i>
                                <span>Super Admin</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; {{ date('Y') }} <a href="/">SistOpWeb</a> | <a href="#terminos" data-toggle="modal">Terminos y Condiciones</a>.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    <section class="content">
        @yield('content')   
        @yield('modals')    
        @yield('js')
    </section>
</body>
@if( $admin->opticas()->count() > 1 )
    @include('modal.elegirOptica')
@endif
@include('modal.endFreeTrial')
@include('modal.inactiveAccount')
@include('modal.terminos')
</html>
