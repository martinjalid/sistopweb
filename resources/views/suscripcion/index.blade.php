@extends('layout.layout')

@section('content')
<div class="crsf">
    {{ csrf_field() }}
</div>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-sm-5">
                        <h2>SUSCRIPCION</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                <div class="row m-l-10">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="header bg-blue">
                                <h2>
                                    Mi Plan - {{ $plan->nombre }}
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-4" >
                                        <h4 style="text-align: center; font-weight: lighter;"> Max. Opticas : {{ $plan->max_opticas }} </h4>
                                    </div>
                                    <div class="col-sm-4" style="border-left: solid 1px black; border-right: solid 1px black;">
                                        <h4 style="text-align: center; font-weight: lighter;"> Max. Usuarios : {{ $plan->max_usuarios }} </h4>
                                    </div>
                                    <div class="col-sm-4">
                                        <h4 style="text-align: center; font-weight: lighter;"> Precio : $ {{ $plan->monto }} </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if( $proxima_cuota != '' )
                        <div class="col-sm-2">
                            <div class="card">
                                <div class="header bg-blue">
                                    <h2>
                                        Próxima Cuota
                                    </h2>
                                </div>
                                <div class="body">
                                    <h4 style="text-align: center; font-weight: normal">
                                        {{ $proxima_cuota }}                                    
                                    </h4>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>  
                <hr>
                <div class="row clearfix">
                    @foreach( $tarjetas as $tarjeta )
                        <div class="col-sm-3 m-l-20 m-t-5 p-l-0" style="width: 340px; height: 210px">
                            <div class="tarjeta">
                                <div class="row m-l-15 m-t-5">
                                    <div class="col-sm-10 m-l--15">
                                        <h5>Titular</h5>
                                        <h4>{{ $tarjeta['cardholder']['name'] }}</h4>
                                    </div>
                                    @if( $default_card == $tarjeta['id'] )
                                        <div class="col-sm-2 m-t-10">
                                            <i class="fa fa-check-circle-o fa-2x" style="float: right;" title="Tarjeta elegida para pagar" aria-hidden="true"></i>
                                        </div>
                                    @endif
                                </div>
                                <div class="row m-l-15">
                                    <div class="col-sm-6 p-l-0 p-r-0">
                                        <h5>N° Tarjeta</h5>
                                        <h4>**** **** **** {{ $tarjeta['last_four_digits'] }}</h4>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Vencimiento</h5>
                                        <h4>{{ str_pad($tarjeta['expiration_month'], 2, 0, STR_PAD_LEFT).'/'.$tarjeta['expiration_year'] }}</h4>
                                    </div>
                                </div>
                                <div class="row m-l-15">
                                    <div class="col-sm-8 p-l-0 p-r-0">
                                        <h5>{{ $tarjeta['issuer']['name'] }}</h5>
                                    </div>
                                    <div class="col-sm-3 m-t-5" style="height: 30px; background: white">
                                        <img src="{{ $tarjeta['payment_method']['secure_thumbnail'] }}" alt="creditCard" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" style="color:#6f6f6f; text-align: center">
                                    @if( $default_card != $tarjeta['id'] )
                                        <i class="fa fa-times fa-2x pointer deleteCard" tarjeta-id="{{ $tarjeta['id'] }}" title="Eliminar tarjeta" aria-hidden="true"></i>
                                        <i class="fa fa-check fa-2x pointer defaultCard" tarjeta-id="{{ $tarjeta['id'] }}" title="Marcar tarjeta para pagar" aria-hidden="true"></i>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <br>
                <hr>
                <div class="row clearfix">
                    <h5>(*) Para eliminar una tarjeta no puede estar marcada para realizar los pagos, para esto debera elegir otra tarjeta para realizarlos. </h5>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box-2 bg-indigo hover-expand-effect" data-toggle="modal" href="#mercado_pago">
                            <div class="icon">
                                <i class="material-icons">credit_card</i>
                            </div>
                            <div class="content">
                                <div class="text">Datos de Pago</div>
                                <h6>Añadir un medio de pago</h6>
                            </div>
                        </div>
                    </div>
                    @if( $admin->suscripcion_mp != '' )
                        @if( $suscripcion_status == 'authorized' )
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box-2 bg-red hover-expand-effect suscripcion" data-toggle="modal" action="stop">
                                    <div class="icon">
                                        <i class="material-icons">block</i>
                                    </div>
                                    <div class="content">
                                        <div class="text">Cancelar Suscripción</div>
                                        <h6>Cancelar la suscripción a SistOpWeb</h6>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box-2 bg-light-green hover-expand-effect suscripcion" data-toggle="modal" action="restart">
                                    <div class="icon">
                                        <i class="material-icons">reply</i>
                                    </div>
                                    <div class="content">
                                        <div class="text">Volver a suscribirme</div>
                                        <h6>Volver a suscribirme a SistOpWeb</h6>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
 </div>
@endsection('content')


@section('modals')
    @include('modal.mercadoPago')
@endsection
@section('js')
    <style type="text/css">
        .tarjeta{
            height: 182px;
            width: 340px;
            border-radius: 9px;
            color: white;
            background-image: url('/images/credit_card/visa.png');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            display: inline-block;
            vertical-align: top;
        }
        .fa-times:hover{
            color:#ce3f3f;
        }
        .fa-check:hover{
            color:#8bc34a;
        }
    </style>
    <script type="text/javascript" src="/js/general/index.js"></script>
    <script type="text/javascript" src="/js/suscripcion/index.js"></script>
    <script type="text/javascript"> // INIT
        $(document).ready(function() {
            $("#end_free_trial").modal('hide');
            var sus = new Suscripcion();
        });
    </script>
@endsection