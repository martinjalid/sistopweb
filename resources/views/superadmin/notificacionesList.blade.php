@extends('layout.layout')

@section('content')
<div class="crsf">
    {{ csrf_field() }}
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-sm-3">
                        <h4>
                            NOTIFICACIONES
                        </h4>
                    </div>
                </div>
            </div>
            <div class="body">
                <div class="table-responsive" style="overflow-x: hidden !important;">
                    <table id="data-table-basic" class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tipo Notificacion</th>
                                <th>Administrador</th>
                                <th>Usuario</th>
                                <th>Created_at</th>
                            </tr>
                        </thead>
                        <tbody id="table-body">
                            @if( $notificaciones->count() == 0 )
                                <tr>
                                    <td>No hay notificaciones</td>
                                </tr>
                            @else
                                @foreach($notificaciones as $notificacion)
                                <tr>
                                    <td>{{ $notificacion->id }}</td>
                                    <td>{{ $notificacion->tipo_notificacion() }}</td>
                                    <td>{{ $notificacion->administrador()->mail }}</td>
                                    @if( !is_null( $notificacion->usuario_id ) )
                                        <td>{{ $notificacion->usuario()->mail }}</td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td>{{ $notificacion->created_at }}</td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modals')

@endsection
@section('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script type="text/javascript" src="/js/general/index.js"></script>
    <script type="text/javascript" src="/js/superadmin/index.js"></script>
    <script type="text/javascript"> // INIT
        $(document).ready(function() {
            var spa = new SuperAdmin();

            $("#data-table-basic").DataTable({
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                ],
                language: {
                    "lengthMenu": "Ver _MENU_ resultados",
                    "zeroRecords": "Ningun resultado encontrado",
                    "info": "Mostrando _PAGE_ de _PAGES_",
                    "infoEmpty": "Ningun resultado",
                    "search": "<b>BUSCAR:</b>",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Sig",
                        "previous": "Ant"
                    },
                }
            });
        });
    </script>
@endsection