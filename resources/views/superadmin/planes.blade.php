@extends('layout.layout')

@section('content')
    <div class="crsf">
        {{ csrf_field() }}
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-sm-3">
                            <h4>
                                PLANES
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        @foreach( $planes as $plan )
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 plan" id="{{ $plan->id }}">
                                <div class="card">
                                    <div class="header bg-blue">
                                        <h2>{{ $plan->nombre }}</h2>
                                    </div>
                                    <div class="body">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <label class="form-label">Max. Ópticas</label> 
                                                            <input class="form-control color" name="max_opticas" value="{{ $plan->max_opticas }}">
                                                        </div>                                  
                                                    </div>                                  
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <label class="form-label">Max. Usuarios</label> 
                                                            <input class="form-control color" name="max_usuarios" value="{{ $plan->max_usuarios }}">
                                                        </div>                                  
                                                    </div>                                  
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group form-float">
                                                        <div class="form-line">
                                                            <label class="form-label">Monto</label> 
                                                            <input class="form-control color" name="monto" value="{{ $plan->monto }}">
                                                        </div>                                  
                                                    </div>                                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="row">
                            <div class="col-lg-4 col-lg-offset-4" style="display: flex;align-items: center;justify-content: center;">
                                <button type="button" id="save_planes" class="btn btn-lg bg-green waves-effect pull-left" style="background: #468e46">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="/js/general/index.js"></script>
    <script type="text/javascript" src="/js/superadmin/index.js"></script>
    <script type="text/javascript"> // INIT
        $(document).ready(function() {
            var spa = new SuperAdmin();
        });
    </script>
@endsection