@extends('layout.layout')

@section('content')
<div class="crsf">
    {{ csrf_field() }}
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-sm-3">
                        <h4>
                            OPTICAS
                        </h4>
                    </div>
                </div>
            </div>
            <div class="body">
                <div class="table-responsive" style="overflow-x: hidden !important;">
                    <table id="data-table-basic" class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Administrador</th>
                                <th>Clientes</th>
                                <th>Productos</th>
                                <th>Created_at</th>
                            </tr>
                        </thead>
                        <tbody id="table-body">
                            @if( $opticas->count() == 0 )
                                <tr>
                                    <td>No hay Opticas</td>
                                </tr>
                            @else
                                @foreach($opticas as $optica)
                                <tr>
                                    <td>{{ $optica->id }}</td>
                                    <td>{{ $optica->nombre }}</td>
                                    <td>{{ $optica->usuarios()->where('perfil_id', 1)->first()->mail }}</td>
                                    <td>{{ $optica->get_clientes_count }}</td>
                                    <td>{{ $optica->productos_count }}</td>
                                    <td>{{ $optica->created_at->format('Y-m-d') }}</td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modals')

@endsection
@section('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script type="text/javascript" src="/js/general/index.js"></script>
    <script type="text/javascript" src="/js/superadmin/index.js"></script>
    <script type="text/javascript"> // INIT
        $(document).ready(function() {
            var spa = new SuperAdmin();

            $("#data-table-basic").DataTable({
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                ],
                language: {
                    "lengthMenu": "Ver _MENU_ resultados",
                    "zeroRecords": "Ningun resultado encontrado",
                    "info": "Mostrando _PAGE_ de _PAGES_",
                    "infoEmpty": "Ningun resultado",
                    "search": "<b>BUSCAR:</b>",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Sig",
                        "previous": "Ant"
                    },
                }
            });
        });
    </script>
@endsection