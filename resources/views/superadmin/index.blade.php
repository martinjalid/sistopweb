@extends('layout.layout')

@section('js')
    <script type="text/javascript" src="/js/superadmin/index.js"></script>
        <!-- Moment Plugin Js -->
    <script src="/plugins/momentjs/moment.js"></script>
    <script src="/plugins/momentjs/moment_locale_es.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/plugins/daterangepicker/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="/plugins/daterangepicker/daterangepicker.css">
    
    <script type="text/javascript">
        $(document).ready(function($) {
            var est = new SuperAdmin();
        });    
    </script>
@endsection

@section('content')
    <div class="crsf">
        {{ csrf_field() }}
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="card">
                <div class="header">
                    <h2 id="text">SUPER ADMIN</h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-blue hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">face</i>
                                </div>
                                <div class="content">
                                    <div class="text">CLIENTES</div>
                                    <div class="number count-to" id="infobox_clientes" data-from="0" data-to="0" data-speed="1000"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-green hover-expand-effect" onclick="location.href += '/planes'">
                                <div class="icon">
                                    <i class="material-icons">attach_money</i>
                                </div>
                                <div class="content">
                                    <div class="text">PLANES</div>
                                    <div class="number count-to" id="infobox_recetas" data-from="0" data-to="" data-speed="1000"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-red pointer hover-expand-effect" onclick="location.href += '/notificaciones'">
                                <div class="icon">
                                    <i class="material-icons">mail</i>
                                </div>
                                <div class="content">
                                    <div class="text">NOTIFICACIONES</div>
                                    <div class="number count-to" id="infobox_recetas" data-from="0" data-to="" data-speed="1000"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box-4 bg-purple pointer hover-expand-effect" onclick="location.href += '/opticas'">
                                <div class="icon">
                                    <i class="material-icons">business</i>
                                </div>
                                <div class="content">
                                    <div class="text">OPTICAS</div>
                                    <div class="number count-to" id="infobox_recetas" data-from="0" data-to="" data-speed="1000"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="card">
                                <div class="header bg-teal">
                                    <h2>RECETAS DE POR ESTADO</h2>
                                </div>
                                <div class="body">
                                    <div id="recetas_estado" class="dashboard-donut-chart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="card">
                                <div class="header bg-teal">
                                    <h2>RECETAS DE POR PROFESIONAL</h2>
                                </div>
                                <div class="body">
                                    <div id="recetas_medico" class="dashboard-donut-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection