<div class="modal fade" id="elegir_optica" data-backdrop="static" data-keyboard="false" tabindex="-1" href="#" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">             
            <div class="modal-header">
                <h4 class="modal-title">Elegir Optica</h4>  
                <hr>
            </div>          
            <div class="modal-body">
                <div class="row clearfix">
                    <h5 class="col-sm-offset-1 col-sm-10" style="text-align: -webkit-center;">Al elegir la óptica, todos los clientes y recetas que cree se van a asignar a la óptica elegida.<br>
                        <br>
                    Igualmente las estadísticas son de todas las ópticas en conjunto.
                    </h5>
                </div>
                <br>
                <div class="row clearfix">
                    <div class="col-sm-offset-3 col-sm-6">
                        <select id="optica_modal" class="form-control show-tick">
                            <option value="">Optica</option>
                            @foreach( $admin->opticas() as $optica )
                                <option value="{{ $optica->id }}" >{{ $optica->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <hr>
                <button id="log_in_optica" type="button" class="btn btn-lg bg-green waves-effect pull-right">Elegir Óptica</button>
            </div>
        </div>
    </div>
</div>