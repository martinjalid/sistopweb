<div class="modal fade" id="terminos" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">             
            <div class="modal-header" style="height: 70px">
                <h4 class="modal-title">Terminos y Condiciones de SistOpWeb</h4>  
                <hr>
            </div>          
            <div class="modal-body">
            	<br>
            	<div class="row">
            		<div class="terms" style="overflow-y: scroll; height: 300px; width: 100%; border: 1px solid #DDD; padding: 10px">
                        <p>SisOpWeb brinda un servicio de suscripción que permite a nuestros miembros acceder a una plataforma que administra y archiva pacientes oftalmologicos y ópticos.</p>

                        <p>Membresía: Su membresía de SistOpWeb continuará mes a mes hasta que la cancele. Para usar el servicio de SistOpWeb, debe tener acceso a Internet y un dispositivo listo para SistOpWeb, y proporcionar una forma de pago actual, válida y aceptada, que se puede actualizar cuando resulte necesario.</p>
                        
                        <b>Pruebas gratis</b>
                        
                        <p>2.1. Su membresía de SistOpWeb iniciara con una prueba gratis. El período de prueba gratis de su membresía durará un mes a partir de la creación de la cuenta.</p>

                        <p>2.2. SistOpWeb determina la elegibilidad para la prueba gratis a su sola discreción y podemos limitar la elegibilidad para evitar el abuso de la prueba gratis. Nos reservamos el derecho de revocar la prueba gratuita y suspender su cuenta si determinamos que usted no es elegible. Los miembros en hogares con una membresía de SistOpWeb existente o reciente no resultan elegibles.Podemos utilizar información como el Id. del dispositivo, la forma de pago o la dirección de email de la cuenta utilizada con una membresía de SistOpWeb existente o reciente para determinar la elegibilidad. Se pueden aplicar restricciones a las combinaciones con otras ofertas.</p>

                        <p>2.3. Una vez terminado el período de prueba debera suscribirse a SistOpWeb para continuar el uso de la plataforma. Todos los cobros se realizan a través de Mercadopago, SistOpWeb no guarda ningun dato de los medios de pago ingresados al sistema.</p>

                        <b>Facturación</b>

                        <p>3.1. Formas de pago. Puede cambiar su medio de pago desde la plataforma en la seccion "Suscripción". Si el pago no se pudiera hacer satisfactoriamente, debido a la fecha de vencimiento, falta de fondos o medio de pago inválido, el ingreso a la plataforma sera restringido a solo el administrador de la o las ópticas. Al actualizar su medio de pago, usted nos autoriza a continuar cobrando a través de Mercadopago al medio de pago ingresado. La fecha de cobro es el mismo dia del mes que se inició la suscripción y asi todos los meses hasta la cancelación de la suscripción.</p>
                        
                        <p>3.2. Cancelación. Puede cancelar la membresía de SistOpWeb en cualquier momento, y continuará teniendo acceso al servicio hasta el final del ciclo de facturación mensual.La suscripción a SistOpWeb puede ser reanudada en cualquien momento igualmente. En la medida permitida por la ley aplicable, los pagos no son reembolsables y no se otorgarán reembolsos ni créditos por los períodos de membresía utilizados parcialmente.</p>

                        <p>3.3. Planes. Los planes son los siguiente:
                            Básico: Este plan cuenta con un máximo de una óptica y 2 usuarios adicionales en la misma. Precio 15,00 U$S.
                            Intermedio: Este plan cuenta con un máximo de tres ópticas y 4 usuarios adicionales por cada óptica. Precio 30,00 U$S.
                            Básico: Este plan cuenta con un máximo de cinco óptica y 9 usuarios adicionales por cada óptica. Precio 50,00 U$S.
                        </p>

                        <p>3.4. Cambios en el precio y Planes de servicio.Los planes tienen un precio fijo en dólares americanos (U$S) convertido y redondeado a pesos argentinos ($), estos precios se actualizaran el primer dia del mes de acuerdo a la cotización del dia de la actualización.</p>
                        
                        <p>
                            Ley vigente. Estos Términos de uso se regirán e interpretarán de conformidad con las leyes Argentinas.
                            Continuación de vigencia. Si alguna de las disposiciones de estos Términos de uso es declarada nula, ilegal o inaplicable, la validez, legalidad y aplicación de las restantes disposiciones continuarán en plena vigencia.
                            Cambios a los Términos de uso y cesión. SistOpWeb puede cambiar estos Términos de uso cuando sea necesario. Le informaremos con al menos 30 días de anticipación si estos nuevos Términos de uso se aplican a usted. Podemos ceder o transferir nuestro acuerdo con usted, incluidos nuestros derechos asociados y nuestras obligaciones en cualquier momento y usted acepta cooperar con nosotros en relación con dicha cesión o transferencia.
                            Comunicaciones electrónicas. Le enviaremos la información relativa a su cuenta (por ejemplo, las autorizaciones de pago, las facturas, los cambios de contraseña o de la forma de pago, los mensajes de confirmación, los avisos) de manera electrónica únicamente, por ejemplo, mediante emails a la dirección de email proporcionada durante el registro.      
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <hr>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>