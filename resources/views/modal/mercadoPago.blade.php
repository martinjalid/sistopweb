<div class="modal fade" id="mercado_pago" data-keyboard="false" tabindex="-1" href="#" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">             
            <div class="modal-header">
                <h4 class="modal-title">Datos del Pago</h4>
                <small>Al cargar una nueva tarjeta, ésta misma sera a la que se realizen los cobros.</small>  
                <hr>
            </div>          
            <div class="modal-body">
                <div class="row clearfix">
					<form action="" method="post" id="pay" name="pay">
                        <fieldset>
                        	<div class="col-sm-12">
	                        	<div class="col-sm-5">
		                            <div class="form-group form-float">
		                                <div class="form-line">
		                                    <p class="form-label" for="email">Email</p>
	    		                            <input id="email" name="email" class="form-control"  value="{{ $admin->mail }}" type="email" disabled/>
	            		                </div>                                  
	                    		    </div>                                  
	                        	</div>
	                        	<div class="col-sm-5">
		                            <div class="form-group form-float">
		                                <div class="form-line">
		                                	<p class="form-label" for="cardExpirationMonth">Nombre del Titular</p>
	                            			<input type="text" id="cardholderName" class="form-control" data-checkout="cardholderName" />
	            		                </div>                                  
	                    		    </div>                                  
	                        	</div>
                        	</div>

                        	<div class="col-sm-12">
	                        	<div class="col-sm-3">
		                            <div class="form-group form-float">
		                                <div class="form-line" id="docTypeCard">
	            		                </div>                                  
	                    		    </div>                                  
	                        	</div>
	                        	<div class="col-sm-4">
		                            <div class="form-group form-float">
		                                <div class="form-line">
		                                	<p class="form-label" for="cardNumber">N° de Documento</p>
	                            			<input class="form-control" type="text" id="docNumber" data-checkout="docNumber" />
	            		                </div>                                  
	                    		    </div>                                  
	                        	</div>
                        	</div>

                        	<div class="col-lg-12">
	                        	<div class="col-sm-6"></div>
	                        	<div class="col-sm-3">
	                        		<p>
	                        			Cód. Seguridad
	                        		</p>
	                        	</div>
	                        	<div class="col-sm-2">Vencimiento</div>
                        	</div>

                        	<div class="col-sm-12">
	                        	<div class="col-sm-5">
		                            <div class="form-group form-float">
		                                <div class="form-line">
		                                	<p class="form-label" for="cardNumber">N° de la Tarjeta</p>
	                            			<input class="form-control" type="text" id="cardNumber" data-checkout="cardNumber" />
	            		                </div>                                  
	                    		    </div>                                  
	                        	</div>
	                        	<div class="col-sm-1" id="img" style="padding: 0px; margin-top: 10px"></div>
	                        	<div class="col-sm-2">
		                            <div class="form-group form-float">
		                                <div class="form-line">
		                                	<!-- <p class="form-label" for="cardNumber">Cód. Seguridad</p> -->
	                            			<input class="form-control" type="text" id="securityCode" data-checkout="securityCode" placeholder="123"/>
	            		                </div>                                  
	                    		    </div>                                  
	                        	</div>
	                        	<div class="col-sm-2">
		                            <div class="form-group form-float">
		                                <div class="form-line">
	                            			<input type="text" id="cardExpirationMonth" class="form-control" placeholder="01" data-checkout="cardExpirationMonth"/>
	            		                </div>                                  
	                    		    </div>                                  
	                        	</div>
	                        	<div class="col-sm-2">
		                            <div class="form-group form-float">
		                                <div class="form-line">
	                            			<input type="text" id="cardExpirationYear" class="form-control" placeholder="2018" data-checkout="cardExpirationYear" />
	            		                </div>                                  
	                    		    </div>                                  
	                        	</div>
                        	</div>
                        	<div class="col-sm-12" >
                            	<input type="submit" value="Añadir" class="btn btn-lg bg-green waves-effect pull-right" />
                        	</div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		Mercadopago.setPublishableKey("{{ config('services.mercadopago.public_key') }}");

		Mercadopago.getIdentificationTypes(function(status, docTypes){
			$("#docTypeCard").append('<select id="docType" data-checkout="docType" style="background: none; height:34px;"><option>Tipo Documento</option></select>')
			if(status == 200){
				for (var i = 0; i < docTypes.length; i++) {
				   $("#docType").append('<option value="'+docTypes[i].id+'">'+docTypes[i].name+'</option>');
				}
			}
       	});

       	function getBin() {
		    var ccNumber = document.querySelector('input[data-checkout="cardNumber"]');
		    return ccNumber.value.replace(/[ .-]/g, '').slice(0, 6);
		};

		function guessingPaymentMethod(event) {
		    var bin = getBin();

		    if (event.type == "keyup") {
		        if (bin.length >= 6 && $.isNumeric(bin) ) {
		            Mercadopago.getPaymentMethod({
		                "bin": bin
		            }, setPaymentMethodInfo);
		        }
		    } else {
		        setTimeout(function() {
		            if (bin.length >= 6 && $.isNumeric(bin)) {
		                Mercadopago.getPaymentMethod({
		                    "bin": bin
		                }, setPaymentMethodInfo);
		            }
		        }, 100);
		    }
		};

		function setPaymentMethodInfo(status, response) {
		    if (status == 200) {
		        // do somethings ex: show logo of the payment method
	        	$('#img').html('');
	            $('#img').append('<img src="'+response[0].secure_thumbnail+'" alt="creditCard" />')
		        var form = document.querySelector('#pay');

		        if (document.querySelector("input[name=paymentMethodId]") == null) {
		            var paymentMethod = document.createElement('input');
		            paymentMethod.setAttribute('name', "paymentMethodId");
		            paymentMethod.setAttribute('type', "hidden");
		            paymentMethod.setAttribute('value', response[0].id);
		            form.appendChild(paymentMethod);
		        } else {
		            document.querySelector("input[name=paymentMethodId]").value = response[0].id;
		        }

		        
		    }
		};


		$('input[data-checkout="cardNumber"]').keyup(function(event) {
			guessingPaymentMethod(event);
		});

		$('input[data-checkout="cardNumber"]').change(function(event) {
			guessingPaymentMethod(event);
		});

		$("#pay").submit(function(event) {
			event.preventDefault();
			var $form = document.querySelector('#pay');
			Mercadopago.createToken($form, sdkResponseHandler);
		});

		function sdkResponseHandler(status, response) {
			if (status != 200 && status != 201) {

				toastr['error']('Parámetros inválidos', 'Error');
				var campos = response.cause;
				for (var i = campos.length - 1; i >= 0; i--) {
					var array = campos[i].description.split(' ');
					campo = array[ array.indexOf('parameter') + 1 ];

					$("#"+campo).parent().addClass('focused');
					$("#"+campo).parent().addClass('error');
				}
			}else{
				//AJAX RESPONSE.ID
				console.log(response);

				$.ajax({
					url: '/suscripcion/asociarTarjeta',
					type: 'POST',
					dataType: 'json',
					data: {token: response.id},
					headers:{
						'X-CSRF-TOKEN' : document.getElementsByName('_token')[0].value,
					},
				})
				.done(function( resp ) {
					toastr.options.onHidden = function() { 
  						location.reload();
  					}
					if (resp.error) 
						toastr['error'](resp.msj, 'Error');
					else
						toastr['success']('La tarjeta se guardo correctamente', 'OK');
					
				})

		} }; 

	});
</script>
