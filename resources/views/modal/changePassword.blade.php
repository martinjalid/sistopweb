<div class="modal fade" id="password" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">             
            <div class="modal-header" style="height: 70px">
                <h4 class="modal-title">Cambiar la contraseña</h4>  
                <hr>
            </div>          
            <div class="modal-body">
            	<br>
            	<div class="row">
            		<div class="col-md-offset-2 col-md-8">
	                	<div class="row">
                            <div class="form-group">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Actual contraseña</label> 
                                        <input class="form-control" id="old_password" >
                                    </div>                                  
                                </div>                                  
                            </div>
	                	</div>
                        <div class="row">
                            <div class="form-group">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Nueva contraseña</label> 
                                        <input class="form-control" id="new_password" >
                                    </div>                                  
                                </div>                                  
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Repita nueva contraseña</label> 
                                        <input class="form-control" id="re_password" >
                                    </div>                                  
                                </div>                                  
                            </div>
                        </div>
            		</div>
            	</div>
            </div>
            <div class="modal-footer">
                <hr>
                <button id="save_new_password" type="button" class="btn btn-success waves-effect pull-right">Guardar</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>