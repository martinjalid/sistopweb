<div class="modal fade" id="usuarios" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">             
	        <div class="modal-header" style="height: 70px">
	            <h4 class="modal-title">Usuarios</h4>  
	            <hr>
	        </div>          
	        <div class="modal-body">
	        	<table class="table">
					<thead>
						<tr>
							<th>Email</th>
							<th>Fecha Creacion</th>
							<th>Activo</th>
						</tr>
					</thead>
					<tbody>
						@if( $usuarios->count() == 0 )
							<tr>
								<td> No Hay Usuarios Registrados</td>
	                            <td></td>
	                            <td></td>
							</tr>
						@endif
						@foreach( $usuarios as $usuario )
							<tr>
								<td> {{ $usuario->mail }} </td>
								<td> {{ $usuario->created_at }}</td>
								<td> {{ $usuario->activo ? 'Activo' : 'No Activo' }}</td>
							</tr>
						@endforeach
							<tr>
								<td>
									<button type="button" href="#new_user" data-toggle="modal" class="btn btn-lg btn-info waves-effect pull-right">
			                            <i class="material-icons">person_add</i>
			                            <span>NUEVO CLIENTE</span>
			                        </button>
								</td>
								<td></td>
								<td></td>
							</tr>
					</tbody>
				</table>
	        </div>
	        <div class="modal-footer">
	            <hr>
	            <button id="save_perfil" type="button" class="btn btn-success waves-effect pull-right">Guardar</button>
	            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
	        </div>
	    </div>
	</div>
</div>