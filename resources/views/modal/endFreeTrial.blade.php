<div class="modal fade" id="end_free_trial" data-backdrop="static" data-keyboard="false" tabindex="-1" href="#" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">             
            <div class="modal-header">
                <h4 class="modal-title">Su mes de prueba expiró</h4>  
                <hr>
            </div>          
            <div class="modal-body">
                <div class="row clearfix">
                    <h5 class="col-sm-offset-1 col-sm-10" style="text-align: -webkit-center;">Ya termino su mes de prueba, si quiere seguir utilizando la plataforma debera suscribirse.
                    <br>
                    Solo el administrador de la optica podra realizar la suscripción.
                    </h5>
                </div>
            </div>
            <div class="modal-footer">
                <hr>
                @if( $admin->perfil() == 'Administrador' )
                    <a href="suscripcion">
                        <button type="button" class="btn btn-lg bg-green waves-effect pull-right">Suscripcion</button>
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>