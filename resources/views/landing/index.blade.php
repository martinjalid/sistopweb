<!DOCTYPE html>
<html>
    <head>
    <title>SistOpWeb</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap Core CSS -->
    <link href="/landing/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/landing/css/landing-page.css" rel="stylesheet">
    <link href="/landing/css/envoyer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/landing/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/jpg" href="/images/icon.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body id="home">

        <nav class="navbar navbar-default navbar-fixed-top topnav">
            <div class="container">
                <div class="navbar-header">
                    <div class="navbar-brand pointer" style="margin-top: -10px; padding-bottom: 45px;">
                        <span style="cursor: pointer;" onclick="location.href = '/'"><b style="color: white">SistOpWeb</b></span>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="home-nav">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a style="cursor: pointer;" class="informacion">Información</a></li>
                        <li><a style="cursor: pointer;" class="preview">Preview</a></li>
                        <!-- <li><a style="cursor: pointer;" class="planes">Planes</a></li>-->
                        <li><a style="cursor: pointer;" class="contacto">Contacto</a></li>
                        @if( Auth::check() )
                            <li><a href="estadisticas">Mi Óptica</a></li>
                        @else
                            <li><a href="login">Iniciar Sesión</a></li>
                            <li><a href="register">Registrarse</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Header -->
        <a name="informacion"></a>
        <div class="intro-header" id="informacion">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="intro-message">
                            <h1>SistOpWeb</h1>
                            <hr class="intro-divider">
                            <h2 style="text-shadow: 0px 3px 12px black;">Administrá tu óptica, recetas y clientes gratis.</h2>
                            <a class="btn btn-ultra btn-ultra-secondary" href="/login">
                                <span class="network-name" style="font-weight: 900; font-size: 20px">Comenzá</span>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container -->

        </div>
        <!-- /.intro-header -->

        <!-- Page Content -->

        <a  name="services"></a>

            <div class="container" id="preview">
                
                <div class="row">
                    <div class="col-sm-10">
                        <hr class="section-heading-spacer">
                        <div class="clearfix"></div>
                        <h2 class="section-heading">Guarda todos tus clientes y sus recetas de forma segura:<br></h2>
                        <p class="lead">Olvidate de que todos tus clientes y recetas dependan de tu PC, con SistOpWeb podes acceder a ellos desde cualquier dispositivo con acceso a internet.</p>
                    </div>
                </div>

                <div class="row">
                    <div id="myCarousel" class="col-sm-12 carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="img-responsive" src="/landing/img/usuariolist.png">
                            </div>

                            <div class="item">
                                <img class="img-responsive" src="/landing/img/recetas.png">
                            </div>
                        </div>

                        <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" style="background: none" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" style="color: #337ab7; margin-left: -60%"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" style="background: none" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" style="color: #337ab7;  margin-right: -60%"></span>
                                <span class="sr-only">Next</span>
                            </a>
                </div>
            </div>
            <!-- /.container -->

        </div>
        <!-- /.content-section-a -->

        <div class="content-section-b">
            <div class="container">

                <div class="row">
                    <div class="col-sm-7">
                        <img class="img-responsive" src="/landing/img/estadisticas.png" alt="">
                    </div>
                    <div class="col-sm-5">
                        <hr class="section-heading-spacer">
                        <div class="clearfix"></div>
                        <h2 class="section-heading">Estadísticas</h2>
                        <p class="lead">Todas las estadísticas que necesitas de tus ópticas, y más funcionalidades.</p>
                    </div>
                </div>

            </div>
            <!-- /.container -->

        </div>
        <!-- /.content-section-b -->

        <!--
        <div class="content-section-a">

            <div class="container" id="planes">

                <div class="row">
                    <div class="col-sm-10">
                        <hr class="section-heading-spacer">
                        <div class="clearfix"></div>
                        <h2 class="section-heading">Planes</h2>
                        <p class="lead">Contamos con diferentes planes para que elijas el que mas se ajuste a tu perfil.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-plan">
                            <div class="panel-heading">
                                <div class="left">Básico</div>
                                <div class="right">$399<span>/mes</span></div>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li><strong>1</strong> Óptica</li>
                                    <li>Hasta <strong>3</strong> usuarios</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-plan primary">
                            <div class="panel-heading">
                                <div class="left">Intermedio</div>
                                <div class="right">$699<span>/mes</span></div>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li>Hasta <strong>3</strong> Ópticas</li>
                                    <li>Hasta <strong>5</strong> usuarios por óptica</li>
                                    <li>*Proximamente</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-plan">
                            <div class="panel-heading">
                                <div class="left">Avanzado</div>
                                <div class="right">$999<span>/mes</span></div>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li>Hasta <strong>10</strong> Ópticas</li>
                                    <li>Hasta <strong>10</strong> usuarios por óptica</li>
                                    <li>*Proximamente</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/register" class="btn btn-ultra btn-ultra-secondary" style="margin-left: 34%">
                    Empezar Mes de Prueba!
                </a>
                <div class="row">
                    <br>
                    <br>
                    <div class="col-md-4">
                        <img src="/landing/img/mercadopago.png" width="80%">
                    </div>
                    <h4>Todos las transacciones en la plataforma son realizadas por mercadopago.</h4>
                </div>

            </div>

            </div>

        </div> -->
        <!-- /.content-section-a -->

        <a name="contacto"></a>
        <div class="banner" id="contacto">

            <div class="container">

                <div class="row">
                    <div class="col-lg-6">
                        <h2>Ante cualquier consulta, envianos un mail:</h2>
                    </div>
                    <div class="col-lg-6">
                        <ul class="list-inline banner-social-buttons">
                            <li>
                                <a class="btn btn-ultra btn-ultra-secondary" href="https://mail.google.com/mail" target="_blank">
                                    <i class="fa fa-envelope-o"></i> 
                                    <span class="network-name">&nbsp; sistopweb@gmail.com</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            <!-- /.container -->
        </div>
        <!-- /.banner -->

        <!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="copyright text-muted small">&copy; SistOpWeb 2018.</p>
                    </div>
                </div>
            </div>
        </footer>

        <!-- jQuery -->
        <script src="/landing/js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="/landing/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function($) {
                $(".contacto").click(function(event) {
                    $('html, body').animate({
                        scrollTop: $("#contacto").offset().top
                    },1000);   
                });

                $(".informacion").click(function(event) {
                    $('html, body').animate({
                        scrollTop: $("#informacion").offset().top
                    },1000);   
                });

                $(".preview").click(function(event) {
                    $('html, body').animate({
                        scrollTop: $("#preview").offset().top
                    },1000);   
                });

                $(".planes").click(function(event) {
                    $('html, body').animate({
                        scrollTop: $("#planes").offset().top
                    },1000);   
                });

            });

        </script>
    </body>

</html>