@extends('layout.login')

@section('content')
<div class="col-lg-3 col-md-5 col-xs-12">
    <div class="card">
        <h4 class="l-login">¿Olvidó la contraseña? <div class="msg">Ingrese su email y le resetearemos la contraseña.</div></h4>
        <form class="col-md-12" id="sign_in" method="POST">
            <div class="form-group form-float">
                <div class="form-line">
                    <input type="email" class="form-control">
                    <label class="form-label">Email</label>
                </div>
            </div>            
            <div class="row">                    
                <div class="col-xs-12">
                    <a href="index.html" class="btn btn-raised waves-effect bg-red" type="submit">RESETEAR MI PASSWORD</a>
                </div>
                <div class="col-xs-12"> 
                    <a href="/login">¡Iniciar sesión!</a> 
                </div>
            </div>
        </form>
    </div>
</div>
<div class="col-lg-3 col-md-5 col-xs-12">
    <div class="card">
        <h4 class="l-login">Forgot Password? <div class="msg">Enter your e-mail address below to reset your password.</div></h4>
        <form class="col-md-12" id="sign_in" method="POST">
            <div class="form-group form-float">
                <div class="form-line">
                    <input type="email" class="form-control">
                    <label class="form-label">Email</label>
                </div>
            </div>            
            <div class="row">                    
                <div class="col-xs-12">
                    <a href="index.html" class="btn btn-raised waves-effect bg-red" type="submit">RESET MY PASSWORD</a>
                </div>
                <div class="col-xs-12"> <a href="sign-in.html">Sign In!</a> </div>
            </div>
        </form>
    </div>
</div>
@endsection
