@extends('layout.layout')

@section('content')
    <div class="crsf">
        {{ csrf_field() }}
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="card">
                <div class="header">
                    <h2>Preguntas Frecuentes</h2> 
                </div>
                <div class="body">
                	<div class="container-fluid">
                    	<i class="fa fa-check" aria-hidden="true"></i> &nbsp; <label style="font-size: 20px; color: #6aa7d2" class="pointer ref" element="crear_cliente" >Como crear un cliente?</label><br>
                    	<i class="fa fa-check" aria-hidden="true"></i> &nbsp; <label style="font-size: 20px; color: #6aa7d2" class="pointer ref" element="crear_receta" >Como crear una receta?</label>
                	</div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="card" id="crear_cliente">
                <div class="header" style="background: #26699e;">
                    <h2 style="color: white">Crear Cliente</h2> 
                </div>
                <div class="body">
                    <div class="container-fluid">
                    	<p>
	                    	1 - Para crear un cliente debemos primero abrir la barra lateral apretando este botón &nbsp;
	                    	<i class="fa fa-chevron-right" aria-hidden="true"></i>
	                    	<img src="/images/faq/sidebar_toggle.png">
	                    	en la esquina izquierda superior de la pantalla.
                    	</p>
                    	<p>
                    		2 - Una vez con la barra lateral abierta, apretar el boton del cliente
	                    	<i class="fa fa-chevron-right" aria-hidden="true"></i>
	                    	<img src="/images/faq/clientes_button.png" class="">
	                    	, este lo llevara al listado de todos los clientes de la óptica.
                    	</p>
                    	<p>
                    		3 - Una vez en la pantalla del listado de clientes, apretar el boton de nuevo cliente
	                    	<i class="fa fa-chevron-right" aria-hidden="true"></i>
	                    	<img src="/images/faq/new_cliente_button.png" class="">
	                    	, y completar el formulario con los datos del nuevo cliente.<br>
	                    	<i>* Los campos nombre, apellido, telefono y direccion del cliente son oblgatorios.</i>
                    	</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="card" id="crear_receta">
                <div class="header" style="background: #26699e;">
                    <h2 style="color: white">Crear Receta</h2> 
                </div>
                <div class="body">
                    <div class="container-fluid">
                    	<p>
	                    	1 - Para crear un cliente debemos primero abrir la barra lateral apretando este botón &nbsp;
	                    	<i class="fa fa-chevron-right" aria-hidden="true"></i>
	                    	<img src="/images/faq/sidebar_toggle.png" class="img-circle">
	                    	en la esquina izquierda superior de la pantalla.
                    	</p>
                    	<p>
                    		2 - Una vez con la barra lateral abierta, apretar el boton del cliente
	                    	<i class="fa fa-chevron-right" aria-hidden="true"></i>
	                    	<img src="/images/faq/clientes_button.png" class="">
	                    	, este lo llevara al listado de todos los clientes de la óptica.
                    	</p>
                    	<p>
                    		3 - Una vez en la pantalla del listado de clientes, buscar el cliente al que quiera crearle la receta y apretar este boton
	                    	<i class="fa fa-chevron-right" aria-hidden="true"></i>
	                    	<img src="/images/faq/new_receta_button.png" class="">
	                    	ubicado en la columna de la derecha del listado, y completar el formulario de la receta.<br>
                    	</p>
                    	<p>
                    		4 - Una vez que guarde la receta, la pagina lo va a dirigir a la pantalla de la receta con todos los datos a completar.
                    	</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function($) {
        $(".ref").click(function(event) {
        	var element = $(this).attr('element');

            $('html, body').animate({
                scrollTop: $("#"+element).offset().top
            },1000);   
        });
    });

</script>
@endsection